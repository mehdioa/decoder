#include "Core/Base/Protocol.h"
#include "Core/Base/SizeExpression.h"
#include "Core/Business/Service.h"
#include "Core/Settings/Settings.h"
#include "Gui/MainWindow/MainWindow.h"
#include "pch.hpp"
//#include "DarkStyle.h"
//#include "framelesswindow.h"
//#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>

int main(int argc, char* argv[])
{
    //    Core::Setting::Settings AppSettings;
    //    AppSettings.read("config.xml");
    //Debug("Welcome");

    QApplication app(argc, argv);

    //        QFile style_file("style.css");
    //        style_file.open(QFile::ReadOnly | QFile::Text);
    //        QTextStream ts(&style_file);
    //        qApp->setStyleSheet(ts.readAll());
    Core::Business::Service service;
    service.config("config.xml");
    service.start();
    Gui::Base::MainWindow window(&service);
    window.show();

    //    QApplication::setStyle(new DarkStyle);
    //    FramelessWindow framelessWindow;
    //    framelessWindow.setWindowState(Qt::WindowMaximized);
    //    framelessWindow.setWindowTitle("test title");
    //    framelessWindow.setWindowIcon(qApp->style()->standardIcon(QStyle::SP_DesktopIcon));
    //    framelessWindow.setContent(new MainWindow);
    //    framelessWindow.show();

    return app.exec();

    return 0;
}
