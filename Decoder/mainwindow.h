#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QMenu;

namespace Core {
namespace Setting {
    class Settings;
}
}

namespace Gui {
namespace Base {
    class FieldView;
    class DataView;
    class SpreadSheet;
}
namespace Setting {
    class Settings;
}
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(Core::Setting::Settings* settings);

private Q_SLOTS:
    bool OnSettings();
    void OnSettingFinished(int result);

private:
    void createActions();
    void createStatusBar();
    void createDockWindows();

    QMenu* viewMenu;
    Gui::Base::FieldView* m_FieldView;
    Gui::Base::DataView* m_DataView;
    Gui::Base::SpreadSheet* m_SpreadSheet;
    Gui::Setting::Settings* m_SettingDialog = nullptr;
};

#endif // MAINWINDOW_H
