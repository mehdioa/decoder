import qbs

Product {
    name: "Decoder"
    type: "application"
//    qbs.architecture: project.arch
    files: [
        "**/main.cpp",
//        "mainwindow.h",
//        "../res/Qt-Frameless-Window-DarkStyle/DarkStyle.cpp",
//        "../res/Qt-Frameless-Window-DarkStyle/DarkStyle.h",
//        "../res/Qt-Frameless-Window-DarkStyle/*.qrc",
//        "../res/Qt-Frameless-Window-DarkStyle/framelesswindow/*.cpp",
//        "../res/Qt-Frameless-Window-DarkStyle/framelesswindow/*.h",
//        "../res/Qt-Frameless-Window-DarkStyle/framelesswindow/*.ui",
        "gui.qrc",
    ]

    cpp.includePaths: {
        var res = project.INC_DIRS;
        res.push(".");
        res.push("..");
        res.push("../res/Qt-Frameless-Window-DarkStyle")
        res.push("../res/Qt-Frameless-Window-DarkStyle/framelesswindow")
        return res;
    }

    cpp.defines: ["BOOST_DYNAMIC_BITSET_DONT_USE_FRIENDS"]

    Depends {name: "cpp"}
    Depends {name: "Qt"; submodules: ["xml", "widgets", "gui"]}

    cpp.libraryPaths: project.LIB_DIRS

    cpp.cxxLanguageVersion: project.cxxLanguageVersion

    cpp.dynamicLibraries:  ["pugixml", "DecoderCore", "DecoderGui"]

    Group
    {
        name:"PCH"
        fileTags:"cpp_pch_src"
        files:[
            "pch.hpp",
        ]
    }

    cpp.useCxxPrecompiledHeader: true

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }

    Group {
        name: "XML Files"
        qbs.install: true
        files: [
            "*.xml",
            "Protocols/*.xml",
        ]
        qbs.installDir: "bin"
    }
    Group {
        name: "Trace Files"
        qbs.install: true
        files: [
            "*.pet",
        ]
        qbs.installDir: "bin"
    }
}
