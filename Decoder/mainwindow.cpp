// clang-format off
#include "pch.hpp"
#include "mainwindow.h"
#include "Core/Settings/Settings.h"
#include "Gui/Settings/Settings.h"
#include "Gui/Base/FieldView.h"
#include "Gui/Base/DataView.h"
#include "Gui/SpreadSheet/SpreadSheet.h"
#include <QDockWidget>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QToolBar>
// clang-format on

MainWindow::MainWindow(Core::Setting::Settings* settings)
    : m_SettingDialog(new Gui::Setting::Settings(settings))
{
    m_FieldView = new Gui::Base::FieldView(this);
    m_DataView = new Gui::Base::DataView(this);
    m_SpreadSheet = new Gui::Base::SpreadSheet(10, 6, this);
    //    setCentralWidget(view);
    createActions();
    createStatusBar();
    createDockWindows();

    //    setWindowFlag(Qt::FramelessWindowHint);

    setWindowTitle(tr("Dock Widgets"));

    setUnifiedTitleAndToolBarOnMac(true);
    connect(m_SettingDialog, &QDialog::finished, this, &MainWindow::OnSettingFinished, Qt::UniqueConnection);
}

bool MainWindow::OnSettings()
{
    m_SettingDialog->readSettings();
    m_SettingDialog->reload();
    m_SettingDialog->open();
    return true;
}

void MainWindow::OnSettingFinished(int result)
{
    Core::Info("Result is {}", result);
}

void MainWindow::createActions()
{
    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));
    QToolBar* fileToolBar = addToolBar(tr("File"));

    const QIcon set_editable_icon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    QAction* set_editable_action = new QAction(set_editable_icon, tr("&Editable..."), this);
    set_editable_action->setShortcuts(QKeySequence::Save);
    set_editable_action->setStatusTip(tr("Setting editable"));

    connect(set_editable_action, &QAction::triggered, m_FieldView, &Gui::Base::FieldView::setEditable);
    fileMenu->addAction(set_editable_action);
    fileToolBar->addAction(set_editable_action);

    fileMenu->addSeparator();

    viewMenu = menuBar()->addMenu(tr("&View"));

    QAction* quitAct = fileMenu->addAction(tr("&Quit"), this, &QWidget::close);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Quit the application"));

    auto action = fileMenu->addAction(tr("&Settings"), this, &MainWindow::OnSettings);
    action->setShortcuts(QKeySequence::Quit);
    action->setStatusTip(tr("Quit the application"));
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::createDockWindows()
{
    auto spreadsheet_dock = new QDockWidget(tr("SpreadSheet"), this);
    spreadsheet_dock->setAllowedAreas(Qt::AllDockWidgetAreas);

    spreadsheet_dock->setWidget(m_SpreadSheet);
    addDockWidget(Qt::LeftDockWidgetArea, spreadsheet_dock);
    viewMenu->addAction(spreadsheet_dock->toggleViewAction());

    auto field_view_dock = new QDockWidget(tr("Field View"), this);
    field_view_dock->setAllowedAreas(Qt::AllDockWidgetAreas);

    field_view_dock->setWidget(m_FieldView);
    addDockWidget(Qt::RightDockWidgetArea, field_view_dock);
    viewMenu->addAction(field_view_dock->toggleViewAction());

    auto data_view_dock = new QDockWidget(tr("Data View"), this);
    data_view_dock->setAllowedAreas(Qt::AllDockWidgetAreas);

    data_view_dock->setWidget(m_DataView);
    addDockWidget(Qt::RightDockWidgetArea, data_view_dock);
    viewMenu->addAction(data_view_dock->toggleViewAction());
}
