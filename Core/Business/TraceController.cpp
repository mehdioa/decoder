// clang-format off
#include "pch.hpp"
#include "TraceController.h"
#include "TraceFile.h"
#include "Service.h"
#include "../Settings/Settings.h"
#include "../Settings/Protocols.h"
#include "../Settings/Protocol.h"
#include "../Settings/ColumnSets.h"
#include "../Settings/ColumnSet.h"
#include "../Settings/Column.h"
#include "Cell.h"
#include "Packet.h"
#include "../Base/DecodedField.h"
#include "../Base/Field.h"
#include "../tree.h"
// clang-format on

namespace Core {
namespace Business {
    struct ViewTrace {
        TraceFile trace;
        Setting::ColumnSet columnset;
    };

    struct TraceController::Impl {
        TraceController* m_Parent;
        Service* m_Service;
        IdType m_LastTraceId;
        std::mutex* m_TraceFilesMutex;
        std::vector<ViewTrace*> m_ViewTraces;

        Impl(TraceController* parent)
            : m_Parent(parent)
            , m_TraceFilesMutex(new std::mutex)
        {
        }

        ~Impl()
        {
            delete m_TraceFilesMutex;
        }

        IdType open(const std::string& file_name)
        {
            auto viewtrace = new ViewTrace;
            if (viewtrace->trace.open(file_name)) {
                auto setting = m_Service->settings();
                auto protocols = setting->protocols();
                auto protocol = protocols->At(viewtrace->trace.protocolId());
                viewtrace->trace.setProtocol(protocol->baseProtocol());
                auto columnsets = protocol->columnSets();
                auto columnset = columnsets->Default();
                viewtrace->columnset = *columnset;
                std::lock_guard<std::mutex>
                    lock(*m_TraceFilesMutex);
                for (auto i = 0; i < m_ViewTraces.size(); ++i) {
                    if (m_ViewTraces.at(i) == nullptr) {
                        m_ViewTraces[i] = viewtrace;
                        return i;
                    }
                }
                m_ViewTraces.push_back(viewtrace);
                return m_ViewTraces.size() - 1;
            }
            return InvalidId;
        }

        void close(IdType id)
        {
            std::lock_guard<std::mutex> lock(*m_TraceFilesMutex);
            if (id >= m_ViewTraces.size() || m_ViewTraces.at(id) == nullptr) {
                Warn("No trace is open with id {}", id);
                return;
            }
            auto viewtrace = m_ViewTraces.at(id);
            viewtrace->trace.close();
            delete viewtrace;
        }

        void traverseTrace(IdType trace_id, ViewType view_type, int start, int repeat, PacketVisitor visitor)
        {
            std::lock_guard<std::mutex> lock(*m_TraceFilesMutex);
            if (trace_id >= m_ViewTraces.size() || m_ViewTraces.at(trace_id) == nullptr)
                return;
            m_ViewTraces.at(trace_id)->trace.traverse(start, repeat, visitor);
        }

        void traverseColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor)
        {
            std::lock_guard<std::mutex> lock(*m_TraceFilesMutex);
            if (trace_id >= m_ViewTraces.size() || m_ViewTraces.at(trace_id) == nullptr)
                return;
            m_ViewTraces.at(trace_id)->columnset.traverse(column_visitor);
        }

        StringList columnNames(IdType trace_id, ViewType view_type) const
        {
            StringList names;
            std::lock_guard<std::mutex> lock(*m_TraceFilesMutex);
            if (trace_id >= m_ViewTraces.size() || m_ViewTraces.at(trace_id) == nullptr)
                return names;

            names = m_ViewTraces.at(trace_id)->columnset.All();
            names.push_front("Timestamp");
            names.push_front("Extra");
            return names;
        }

        RowData rowData(IdType trace_id, ViewType view_type, size_t row) const
        {
            std::lock_guard<std::mutex> lock(*m_TraceFilesMutex);
            if (trace_id >= m_ViewTraces.size() || m_ViewTraces.at(trace_id) == nullptr) {
                Error("No trace of id {}", trace_id);
                return {};
            }
            RowData rd;
            auto& view_trace = m_ViewTraces.at(trace_id);
            rd.m_Packet = view_trace->trace.packet(row);
            if (!rd.m_Packet->m_Buffer) {
            }

            auto cell = std::make_shared<Cell>();

            //extra
            cell->Value = std::to_string(rd.m_Packet->m_Extra);
            cell->Foreground = 0xff000000;
            rd.m_Cells.push_back(cell);

            //timestamp
            cell = std::make_shared<Cell>();
            cell->Background = 0xffffff;
            cell->Foreground = 0x000000;
            cell->Value = std::to_string(rd.m_Packet->m_Timestamp) + " ms";
            rd.m_Cells.push_back(cell);

            auto column_vistitor = [&rd](Setting::Column* col) {
                auto cell = std::make_shared<Cell>();
                cell->Background = col->background();
                cell->Foreground = col->foreground();
                rd.m_Packet->m_DecodedFieldTree->visit([&col, &cell, &rd](Base::DecodedField* df) {
                    if (df->field()->fieldInfoIndex() == col->fieldInfoIndex())
                        cell->Value += df->value(ValueType::Hex, rd.m_Packet->m_Buffer) + " ";
                    return false;
                });
                rd.m_Cells.push_back(cell);
                return false;
            };

            view_trace->columnset.traverse(column_vistitor);
            return rd;
        }
    };

    DEF_CMA(TraceController);

    IdType TraceController::open(const std::string& file_name)
    {
        return d->open(file_name);
    }

    void TraceController::setService(Service* service)
    {
        d->m_Service = service;
    }

    void TraceController::close(IdType id)
    {
        d->close(id);
    }

    StringList TraceController::columnNames(IdType trace_id, ViewType view_type) const
    {
        return d->columnNames(trace_id, view_type);
    }

    RowData TraceController::rowData(IdType trace_id, ViewType view_type, size_t row) const
    {
        return d->rowData(trace_id, view_type, row);
    }

    void TraceController::traverseTrace(IdType trace_id, ViewType view_type, int start, int repeat, PacketVisitor visitor)
    {
        d->traverseTrace(trace_id, view_type, start, repeat, visitor);
    }

    void TraceController::traverseColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor)
    {
        d->traverseColumnSet(trace_id, view_type, column_visitor);
    }

}
}
