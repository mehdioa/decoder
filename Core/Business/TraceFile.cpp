// clang-format off
#include "pch.hpp"
#include "TraceFile.h"
#include "FileStream.h"
#include "IPacket.h"
#include "../Base/Buffer.h"
#include "../Base/IBuffer.h"
#include "../Base/Protocol.h"
#include "Packet.h"
// clang-format on

#define Write(X) m_FileStream->write(reinterpret_cast<const char*>(&X), sizeof(X))
#define Read(X) m_FileStream->read(reinterpret_cast<char*>(&X), sizeof(X))

namespace Core {
namespace Business {
    struct TraceFile::Impl {
        TraceFile* m_Parent = nullptr;
        FileStream* m_FileStream = nullptr;
        uint32_t m_ProtocolId;
        TraceNoOfPacketsType m_NoOfPackets;
        TraceAddressType m_AddressPackets;
        TraceAddressType m_AddressData;
        Base::Protocol* m_Protocol = nullptr;

        std::vector<Packet*> m_Packets;
        //        std::vector<Buffer*> m_PacketsData;
        //        TraceNoOfPacketsType m_StartOfPacketsData;

        Impl(TraceFile* parent)
            : m_Parent(parent)
            , m_FileStream(new FileStream)
        {
        }

        ~Impl()
        {
            if (m_FileStream)
                m_FileStream->close();
            for (auto packet : m_Packets)
                delete packet;
            //            for (auto data : m_PacketsData)
            //                delete data;
        }

        bool open(const std::string& file_name)
        {
            if (m_FileStream && m_FileStream->isOpen()) {
                //                Error("Cannot open file {} while {} is open already", m_FileStream->GetName());
                return false;
            }
            m_FileStream->open(file_name.c_str());
            Read(m_ProtocolId);
            Read(m_NoOfPackets);
            Debug("Trace has {} packets", m_NoOfPackets);
            Read(m_AddressPackets);
            Debug("Address of packets is {}", m_AddressPackets);
            Read(m_AddressData);
            Debug("Address of data is {}", m_AddressData);
            //            m_StartOfPacketsData = 0;

            TimestampType timestamp;
            PacketExtraType extra;
            DataLengthType len;
            auto data_address = m_AddressData;
            for (auto i = 0; i < m_NoOfPackets; ++i) {
                Read(timestamp);
                Read(extra);
                Read(len);
                auto packet = new Packet;
                packet->m_Address = data_address;
                packet->m_Timestamp = timestamp;
                packet->m_Extra = extra;
                packet->m_DataLength = len;
                m_Packets.push_back(packet);
                data_address += len;
            }
            return true;
        }

        void close()
        {
            if (m_FileStream)
                m_FileStream->close();
        }

        void save(const std::string& file_name, TraceNoOfPacketsType from, TraceNoOfPacketsType to)
        {
        }

        Base::IBuffer* buffer(TraceNoOfPacketsType no) const
        {
            if (no >= m_NoOfPackets) {
                //                Error("Trace has {} packets, but packet {} requested", m_NoOfPackets, no);
                return nullptr;
            }
            auto packet = m_Packets.at(no);
            if (packet->m_Buffer)
                return packet->m_Buffer;
            packet->m_Buffer = new Base::Buffer;
            m_FileStream->seekg(packet->m_Address);
            auto len = packet->m_DataLength;
            uint8_t val;
            for (auto i = 0; i < len; ++i) {
                Read(val);
                packet->m_Buffer->append(val);
            }
            return packet->m_Buffer;
        }

        void traverse(int start, int repeat, PacketVisitor visitor)
        {
            for (auto i = start; i < m_NoOfPackets && i < start + repeat; ++i) {
                auto packet = m_Packets.at(i);
                if (!packet->m_Buffer) {
                    buffer(i);
                }
                packet->m_DecodedFieldTree = m_Protocol->decode(packet->m_Buffer);
                visitor(m_Packets.at(i));
            }
        }
    };

    DEF_CMA(TraceFile);

    bool TraceFile::open(const std::string& file_name)
    {
        return d->open(file_name);
    }

    void TraceFile::close()
    {
        d->close();
    }

    void TraceFile::save(const std::string& file_name, TraceNoOfPacketsType from, TraceNoOfPacketsType to)
    {
        d->save(file_name, from, to);
    }

    Base::IBuffer* TraceFile::packetBuffer(TraceNoOfPacketsType no) const
    {
        return d->buffer(no);
    }

    Packet* TraceFile::packet(TraceNoOfPacketsType no)
    {
        return d->m_Packets.at(no);
    }

    TraceNoOfPacketsType TraceFile::packetCount() const
    {
        return d->m_NoOfPackets;
    }

    uint32_t TraceFile::protocolId() const
    {
        return d->m_ProtocolId;
    }

    void TraceFile::setProtocol(Base::Protocol* protocol)
    {
        d->m_Protocol = protocol;
    }

    void TraceFile::traverse(int start, int repeat, PacketVisitor visitor)
    {
        d->traverse(start, repeat, visitor);
    }
}
}
