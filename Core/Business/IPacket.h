#pragma once

#include "../tree.h"

namespace Core {
namespace Business {

    struct DECODERCORE_API IPacket {
        IPacket() {}
        virtual ~IPacket() {}

        virtual void set(TimestampType timestamp, PacketExtraType extra, DataLengthType len) = 0;

        virtual TimestampType timestamp() const = 0;
        virtual void setTimestamp(TimestampType timestamp) = 0;

        virtual PacketExtraType extra() const = 0;
        virtual void setExtra(PacketExtraType extra) = 0;

        virtual DataLengthType dataLength() const = 0;
        virtual void setDataLength(DataLengthType len) = 0;

        virtual Base::IBuffer* buffer() const = 0;
        virtual Base::DecodedFieldTree* decodedFieldTree() const = 0;

        //        virtual void traverse(Base::DecodedFieldTree::visitor visitor) const = 0;
    };
}
}
