#pragma once

namespace Core {
namespace Business {

    class DECODERCORE_API TraceFile {
        DEC_CMA(TraceFile);

        bool open(const std::string& file_name);
        void close();

        void save(const std::string& file_name, TraceNoOfPacketsType from, TraceNoOfPacketsType to);

        Base::IBuffer* packetBuffer(TraceNoOfPacketsType no) const;
        Packet* packet(TraceNoOfPacketsType no);

        TraceNoOfPacketsType packetCount() const;

        uint32_t protocolId() const;

        void setProtocol(Base::Protocol* protocol);

        void traverse(int start, int repeat, PacketVisitor visitor);
    };
}
}
