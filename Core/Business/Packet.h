#pragma once

namespace Core {
namespace Business {

    class DECODERCORE_API Packet {
    public:
        TimestampType m_Timestamp;
        PacketExtraType m_Extra;
        DataLengthType m_DataLength;
        std::streampos m_Address;
        Base::IBuffer* m_Buffer = nullptr;
        Base::DecodedFieldTree* m_DecodedFieldTree = nullptr;
    };
}
}
