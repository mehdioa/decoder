// clang-format off
#include "pch.hpp"
#include "FileStream.h"
// clang-format on

namespace Core {
namespace Business {
    struct FileStream::Impl {
        FileStream* m_Parent;
        std::fstream* m_FStream;
        std::string m_FileName;

        Impl(FileStream* parent)
            : m_Parent(parent)
            , m_FStream(new std::fstream)
        {
        }

        ~Impl()
        {
            close();
            zap(m_FStream);
        }

        void open(const char* filename)
        {
            m_FStream->open(filename, std::ios::in | std::ios::out | std::ios::binary);
            m_FileName = filename;
        }

        void close()
        {
            m_FStream->close();
            m_FileName = "";
        }

        int peek()
        {
            return m_FStream->peek();
        }

        std::streamsize read(char* s, std::streamsize n)
        {
            auto gcount = m_FStream->gcount();
            m_FStream->read(s, n);
            return (gcount < n) ? gcount : n;
        }

        void write(const char* s, std::streamsize n)
        {
            m_FStream->write(s, n);
        }

        void seekg(std::streampos pos)
        {
            m_FStream->seekg(pos);
        }

        void seekg(std::streamoff off, std::ios_base::seekdir way)
        {
            m_FStream->seekg(off, way);
        }

        void seekp(std::streampos pos)
        {
            m_FStream->seekp(pos);
        }

        void seekp(std::streamoff off, std::ios_base::seekdir way)
        {
            m_FStream->seekp(off, way);
        }

        void flush()
        {
            m_FStream->flush();
        }

        std::streampos tellg() const
        {
            return m_FStream->tellg();
        }

        std::streampos tellp() const
        {
            return m_FStream->tellp();
        }
    };

    DEF_CMA(FileStream);

    void FileStream::open(const char* filename)
    {
        return d->open(filename);
    }

    std::string FileStream::GetName() const
    {
        return d->m_FileName;
    }

    void FileStream::close()
    {
        return d->close();
    }

    bool FileStream::isOpen()
    {
        return d->m_FStream->is_open();
    }

    std::streamsize FileStream::read(char* s, std::streamsize n)
    {
        return d->read(s, n);
    }

    void FileStream::write(const char* s, std::streamsize n)
    {
        return d->write(s, n);
    }

    int FileStream::peek()
    {
        return d->peek();
    }

    void FileStream::seekg(std::streampos pos)
    {
        return d->seekg(pos);
    }

    void FileStream::seekg(std::streamoff off, std::ios_base::seekdir way)
    {
        return d->seekg(off, way);
    }

    void FileStream::seekp(std::streampos pos)
    {
        return d->seekp(pos);
    }

    void FileStream::seekp(std::streamoff off, std::ios_base::seekdir way)
    {
        return d->seekp(off, way);
    }

    std::streampos FileStream::tellg() const
    {
        return d->tellg();
    }

    std::streampos FileStream::tellp() const
    {
        return d->tellp();
    }

    void FileStream::flush()
    {
        return d->flush();
    }

}
}
