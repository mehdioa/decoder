#pragma once

namespace Core {
namespace Business {
    class DECODERCORE_API FileStream {
        DEC_CMA(FileStream);

        void open(const char* filename);
        std::string GetName() const;

        /**
         * @brief close Closes the file currently associated with the object, disassociating it from the stream.
         * @return
         */
        void close();

        /**
         * @brief isOpen Returns whether the stream is currently associated to a file.
         * @return true if a file is open and associated with this stream object. False otherwise.
         */
        bool isOpen();

        /**
         * @brief read Extracts n characters from the stream and stores them in the array pointed to by s.
         * @param s Pointer to an array where the extracted characters are stored.
         * @param n Number of characters to extract. streamsize is a signed integral type.
         * @return The number of charachters that have been read and store in s.
         */
        std::streamsize read(char* s, std::streamsize n);

        /**
         * @brief write Inserts the first n characters of the array pointed by s into the stream.
         * @param s Pointer to an array of at least n characters.
         * @param n Number of characters to insert.
         */
        void write(const char* s, std::streamsize n);

        /**
         * @brief peek Returns the next character in the input sequence, without extracting it: The character is left as the next character to be extracted from the stream.
         * @return The next character in the input sequence, as a value of type int.
         */
        int peek();

        /**
         * @brief seekg Sets the position of the next character to be extracted from the input stream.
         * @param pos New absolute position within the stream (relative to the beginning).streampos is an fpos type (it can be converted to/from integral types).
         */
        void seekg(std::streampos pos);

        /**
         * @brief seekg Sets the position of the next character to be extracted from the input stream.
         * @param off Offset value, relative to the way parameter. streamoff is an offset type (generally, a signed integral type).
         * @param way Object of type ios_base::seekdir.
         */
        void seekg(std::streamoff off, std::ios_base::seekdir way);

        /**
         * @brief seekp Sets the position where the next character is to be inserted into the output stream.
         * @param pos New absolute position within the stream (relative to the beginning).
         */
        void seekp(std::streampos pos);

        /**
         * @brief seekp Sets the position where the next character is to be inserted into the output stream.
         * @param off Offset value, relative to the way parameter.
         * @param way Object of type ios_base::seekdir.
         */
        void seekp(std::streamoff off, std::ios_base::seekdir way);

        /**
         * @brief tellg Returns the position of the current character in the input stream.
         * @return The current position in the stream.
         */
        std::streampos tellg() const;

        /**
         * @brief tellp Returns the position of the current character in the output stream.
         * @return The current position in the stream.
         */
        std::streampos tellp() const;

        void flush();
    };
}
}
