#pragma once

namespace Core {
namespace Business {

    class DECODERCORE_API Service {
        DEC_CMA(Service);

        void config(const char* file_name);
        void start();
        void stop();

        Setting::Settings* settings() const;

        IdType open(FileType file_type, const std::string& file_name);
        void close(FileType file_type, IdType id);

        StringList columnNames(IdType trace_id, ViewType view_type) const;

        RowData rowData(IdType trace_id, ViewType view_type, size_t row) const;

        void traverseTrace(IdType trace_id, ViewType view_type, uint32_t start, uint32_t repeat, PacketVisitor visitor);
        void traverseTraceColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor);
        //        Base::Buffer* packetBuffer(IdType traceid, TraceNoOfPacketsType no) const;
        //        const IPacket* packet(IdType trace_id, TraceNoOfPacketsType no) const;
        //        TraceNoOfPacketsType packetCount(IdType trace_id) const;
        //        uint32_t protocolId(IdType trace_id) const;
    };
}
}
