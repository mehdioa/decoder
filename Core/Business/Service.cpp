// clang-format off
#include "pch.hpp"
#include "Service.h"
#include "TraceController.h"
#include "../Settings/Settings.h"
// clang-format on

namespace Core {
namespace Business {

    struct Service::Impl {
        Service* m_Parent;
        Setting::Settings m_Setting;
        boost::asio::io_service m_IOService;
        std::vector<std::unique_ptr<std::thread>> m_Threads;
        std::unique_ptr<boost::asio::signal_set> m_SignalSet;
        std::mutex m_runningMtx;
        std::atomic_bool m_isRunning;
        TraceController* m_TraceController;

        Impl(Service* parent)
            : m_Parent(parent)
            , m_TraceController(new TraceController)
        {
            m_Setting.setService(m_Parent);
            m_TraceController->setService(m_Parent);
        }

        ~Impl()
        {
            stop();
        }

        Impl(const Impl& impl)
        {
        }

        void start(size_t thread_count)
        {
            std::lock_guard<std::mutex> lock(m_runningMtx);
            m_SignalSet.reset(new boost::asio::signal_set(m_IOService, SIGINT));
            //            m_SignalSet->async_wait(std::bind(&Service::Impl::signal_handler, parent,
            //                std::placeholders::_1,
            //                std::placeholders::_2));

            for (size_t i = 0; i < thread_count; ++i) {
                std::unique_ptr<std::thread> thr(new std::thread([this]() { m_IOService.run(); }));
                m_Threads.push_back(std::move(thr));
            }
            Info("Worker started with {} threads", thread_count);
            m_isRunning.store(true);
        }

        void stop()
        {
            std::lock_guard<std::mutex> lock(m_runningMtx);
            if (m_isRunning.load()) {
                m_IOService.stop();
                m_isRunning.store(false);
            }
            Info("IOService sopped");

            for (auto& t : m_Threads) {
                if (t->joinable()) {
                    t->join();
                }
            }
            Info("Threads joined");
        }

        void traverseTrace(IdType trace_id, ViewType view_type, uint32_t start, uint32_t repeat, PacketVisitor visitor)
        {
            m_TraceController->traverseTrace(trace_id, view_type, start, repeat, visitor);
        }

        void traverseTraceColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor)
        {
            m_TraceController->traverseColumnSet(trace_id, view_type, column_visitor);
        }

        StringList columnNames(IdType trace_id, ViewType view_type) const
        {
            return m_TraceController->columnNames(trace_id, view_type);
        }

        RowData rowData(IdType trace_id, ViewType view_type, size_t row) const
        {
            return m_TraceController->rowData(trace_id, view_type, row);
        }
    };

    DEF_CMA(Service);

    void Service::config(const char* file_name)
    {
        d->m_Setting.read(file_name);
    }

    void Service::start()
    {
        d->start(d->m_Setting.threadCount());
    }

    void Service::stop()
    {
        d->stop();
    }

    Setting::Settings* Service::settings() const
    {
        return &d->m_Setting;
    }

    IdType Service::open(FileType file_type, const std::string& file_name)
    {
        switch (file_type) {
        case FileType::Trace:
            return d->m_TraceController->open(file_name);
        }
        return -1;
    }

    void Service::close(FileType file_type, IdType id)
    {
        switch (file_type) {
        case FileType::Trace:
            return d->m_TraceController->close(id);
        }
    }

    StringList Service::columnNames(IdType trace_id, ViewType view_type) const
    {
        return d->columnNames(trace_id, view_type);
    }

    RowData Service::rowData(IdType trace_id, ViewType view_type, size_t row) const
    {
        return d->rowData(trace_id, view_type, row);
    }

    void Service::traverseTrace(IdType trace_id, ViewType view_type, uint32_t start, uint32_t repeat, PacketVisitor visitor)
    {
        d->traverseTrace(trace_id, view_type, start, repeat, visitor);
    }

    void Service::traverseTraceColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor)
    {
        d->traverseTraceColumnSet(trace_id, view_type, column_visitor);
    }
}
}
