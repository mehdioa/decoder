#pragma once

namespace Core {
namespace Business {

    class DECODERCORE_API Cell {
    public:
        std::string Value;
        int Foreground;
        int Background;
    };
}
}
