#pragma once
#include "../Base/Buffer.h"

namespace Core {
namespace Business {

    class DECODERCORE_API TraceController {
        DEC_CMA(TraceController);

        void setService(Service* service);

        IdType open(const std::string& file_name);
        void close(IdType id);

        //        Base::Buffer* packetBuffer(IdType traceid, TraceNoOfPacketsType no) const;
        //        const IPacket* packet(TraceNoOfPacketsType no) const;
        //        TraceNoOfPacketsType packetCount() const;
        //        uint32_t protocolId() const;

        StringList columnNames(IdType trace_id, ViewType view_type) const;
        RowData rowData(IdType trace_id, ViewType view_type, size_t row) const;

        void traverseTrace(IdType trace_id, ViewType view_type, int start, int repeat, PacketVisitor visitor);
        void traverseColumnSet(IdType trace_id, ViewType view_type, Setting::ColumnVisitor column_visitor);
    };
}
}
