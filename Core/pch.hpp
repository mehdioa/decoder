// pch.h: This is a precompiled header file.
// Files listed below are compiled only once, improving build performance for future builds.
// This also affects IntelliSense performance, including code completion and many code browsing features.
// However, files listed here are ALL re-compiled if any one of them is updated between builds.
// Do not add files here that you will be updating frequently as this negates the performance advantage.

#ifndef H_DECODERCORE_PCH
#define H_DECODERCORE_PCH

// clang-format off
#include <pugixml.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <boost/algorithm/string.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/icl/interval.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/asio.hpp>
#include <utility>
#include <vector>
#include <typeindex>
#include <typeinfo>
#include <cctype>
#include <functional>
#include <fstream>
#include <algorithm>
#include <limits>
#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <memory>
#include <sstream>
#include <string>
#include <cstdint>
#include <unordered_map>
#include "tree.h"

#define DEC_CMA(T)               \
    struct Impl;                 \
    std::unique_ptr<Impl> d; \
                                 \
public:                          \
	explicit T();				 \
    T(T&& t);                    \
    T& operator=(const T& rhs);  \
    T& operator=(T&& rhs);       \
    virtual ~T()

#define DEF_CMA(T)								\
    T::T(): d(std::make_unique<T::Impl>(this)){} \
    T::T(T&& t) = default;						\
    T& T::operator=(const T& rhs)				\
    {											\
        d.reset(new Impl(*rhs.d));		\
        return *this;							\
    }											\
    T& T::operator=(T&& rhs) = default;			\
    T::~T() = default


#if defined(_WIN32) || defined(_WIN64)
#define DEFINE_CONSOLEV2_PROPERTIES
//#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0200

// System headers
#include <windows.h>

// Standard library C-style
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#if defined(DECODERCORE_EXPORTS)
#define DECODERCORE_API __declspec(dllexport)
#else
#define DECODERCORE_API __declspec(dllimport)
#endif
#else
#define DECODERCORE_API
#endif

// clang-format on

typedef unsigned int uint;

#define zap(X)            \
    assert(X != nullptr); \
    delete X;             \
    X = nullptr

#define UNUSED(expr)  \
    do {              \
        (void)(expr); \
    } while (0)

namespace Core {
typedef boost::dynamic_bitset<uint8_t> BitSet;
typedef unsigned int IdType;

#define InvalidId -1

typedef std::shared_ptr<spdlog::logger> LogPtr;

DECODERCORE_API extern LogPtr Trace_;
DECODERCORE_API extern LogPtr Debug_;
DECODERCORE_API extern LogPtr Info_;
DECODERCORE_API extern LogPtr Warn_;
DECODERCORE_API extern LogPtr Error_;
DECODERCORE_API extern LogPtr Critical_;

#define __FILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define LOG_STR_H(x) #x
#define LOG_STR(x) LOG_STR_H(x)

#define LOG_TOKEN(fmt) fmt " (" LOG_STR(__FILE__) " #" LOG_STR(__LINE__) ")"

#define Trace(fmt, ...) Trace_->trace(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Debug(fmt, ...) Debug_->debug(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Info(fmt, ...) Info_->info(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Warn(fmt, ...) Warn_->warn(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Error(fmt, ...) Error_->error(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Critial(fmt, ...) Critical_->critical(LOG_TOKEN(fmt), ##__VA_ARGS__)

typedef std::vector<int> Intervals;
typedef std::vector<std::string> StringVec;
typedef std::list<std::string> StringList;

typedef std::map<std::string, uint64_t> RefMap;

enum class ByteOrder {
    Big,
    Little
};

enum class BitOrder {
    MSB,
    LSB,
};

enum class ValueType {
    Bin,
    Hex,
    Dec,
};

std::string BitOrderToSrting(BitOrder bit_order);
BitOrder StringToBitOrder(const std::string bit_order);

std::string ByteOrderToString(ByteOrder byte_order);
ByteOrder StringToByteOrder(const std::string byte_order);

StringVec SplitString(const std::string& str, char delim);

enum class FieldType {
    Integral,
    String,
    Block,
};
std::string FieldTypeToString(FieldType field_type);
FieldType StringToFieldType(const std::string& filed_type);

enum class FileType {
    Trace,
    Project,
    Script,

    Last,
    Begin = Trace
};

std::string FileTypeToString(FieldType field_type);
FileType StringToFileType(const std::string& filed_type);

enum class ViewType {
    Event,
    Transaction,
    Link,

    Last,
    Begin = Event
};

std::string ViewTypeToString(ViewType view_type);
ViewType StringToViewType(const std::string& view_type);

namespace Log {
    enum class Level {
        Trace,
        Debug,
        Info,
        Warn,
        Error,
        Critical,

        Last,
        Begin = Trace
    };

    DECODERCORE_API const char* LevelName(Level level);

    enum class Output {
        Console,
        File,

        Last,
        Begin = Console

    };

    DECODERCORE_API const char* OutputName(Output output);
}

typedef uint32_t TimestampType;
typedef uint16_t DataLengthType;
typedef uint32_t PacketExtraType;
typedef uint64_t TraceAddressType;
typedef uint32_t TraceNoOfPacketsType;

namespace Base {
    class Field;
    class Buffer;
    class IBuffer;
    class Option;
    class Protocol;
    class FieldInfos;
    class DecodedField;

    typedef std::vector<Field*> VecFields;
    typedef std::set<Field*> SetFields;
    typedef std::map<std::string, Field*> MapField;
    typedef std::function<bool(Field*)> FieldVisitor;
    typedef std::function<bool(Option*)> OptionVisitor;

    typedef std::vector<DecodedField*> VecDecodedField;

    typedef tree<Base::DecodedField> DecodedFieldTree;

    typedef boost::icl::interval_map<uint64_t, Option*> OptionIntervalMap;
    typedef boost::icl::discrete_interval<uint64_t> OptionValue;
    typedef std::map<Option*, Option*> OptionMap;

    struct FieldInfo {
        std::string Name;
        std::string Id;
    };

    typedef std::vector<FieldInfo*> VecFieldInfo;

    struct DecodedBuffer {
        IBuffer* m_Buffer;
        DecodedFieldTree* m_DecodedFieldTree;
    };

    typedef std::function<bool(DecodedBuffer*)> DecodedBufferVisitor;
}

namespace Business {
    class Cell;
    class FileStream;
    class IPacket;
    class Packet;
    class Service;
    class TraceController;
    class TraceFile;

    typedef std::function<bool(Packet*)> PacketVisitor;
    typedef std::list<std::shared_ptr<Cell>> Cells;

    struct RowData {
        Packet* m_Packet;
        Cells m_Cells;
    };
}

namespace Setting {
    class Logger;
    class Node;
    class Settings;
    class Layout;
    class Item;
    class Items;
    class Column;
    class ColumnSet;
    class ColumnSets;
    class Protocol;
    class Protocols;

    typedef std::function<bool(Column*)> ColumnVisitor;
    typedef std::function<bool(ColumnSet*)> ColumnSetVisitor;
}

}

#endif
