// pch.cpp: source file corresponding to the pre-compiled header
// clang-format off

#include "pch.hpp"

// clang-format on
// When you are using pre-compiled headers, this source file is necessary for compilation to succeed.

namespace Core {
std::string BitOrderToSrting(BitOrder bit_order)
{
    switch (bit_order) {
    case BitOrder::LSB:
        return "LSB";
    default:
        return "MSB";
    }
}

std::string ByteOrderToString(ByteOrder byte_order)
{
    switch (byte_order) {
    case ByteOrder::Big:
        return "Big-Endian";
    default:
        return "Little-Endian";
    }
}

BitOrder StringToBitOrder(const std::string bit_order)
{
    if (boost::iequals(bit_order, "LSB"))
        return BitOrder::LSB;
    else
        return BitOrder::MSB;
}

ByteOrder StringToByteOrder(const std::string byte_order)
{
    if (boost::iequals(byte_order, "LITTLE-ENDIAN"))
        return ByteOrder::Little;
    return ByteOrder::Big;
}

StringVec SplitString(const std::string& str, char delim)
{
    StringVec strings;
    std::istringstream f(str);
    std::string s;
    while (getline(f, s, delim)) {
        strings.push_back(s);
    }
    return strings;
}

std::string FieldTypeToString(FieldType field_type)
{
    switch (field_type) {
    case FieldType::Integral:
        return "int";
    case FieldType::String:
        return "string";
    default:
        return "block";
    }
}

FieldType StringToFieldType(const std::string& filed_type)
{
    if (boost::iequals(filed_type, "STRING"))
        return FieldType::String;
    else if (boost::iequals(filed_type, "BLOCK"))
        return FieldType::Block;
    return FieldType::Integral;
}

std::string FileTypeToString(FileType field_type)
{
    switch (field_type) {
    case FileType::Trace:
        return "Trace";
    case FileType::Project:
        return "Project";
    case FileType::Script:
        return "Script";
    default:
        return "None";
    }
}

FileType StringToFileType(const std::string& filed_type)
{
    if (boost::iequals(filed_type, "TRACE"))
        return FileType::Trace;
    else if (boost::iequals(filed_type, "PROJECT"))
        return FileType::Project;
    else if (boost::iequals(filed_type, "SCRIPT"))
        return FileType::Script;
    return FileType::Trace;
}

namespace Log {

    const char* LevelName(Level level)
    {
        switch (level) {
        case Level::Trace:
            return "trace";
        case Level::Debug:
            return "debug";
        case Level::Info:
            return "info";
        case Level::Warn:
            return "warn";
        case Level::Error:
            return "error";
        case Level::Critical:
            return "critical";
        default:
            return "Unknown";
        }
    }

    const char* OutputName(Output output)
    {
        switch (output) {
        case Output::File:
            return "file";
        case Output::Console:
            return "console";
        default:
            return "Unknown";
        }
    }

}

std::string ViewTypeToString(ViewType view_type)
{
    switch (view_type) {
    case ViewType::Link:
        return "Link";
    case ViewType::Event:
        return "Event";
    case ViewType::Transaction:
        return "Transaction";
    }
    return "Unknown";
}

ViewType StringToViewType(const std::string& view_type)
{
    if (boost::iequals(view_type, "TRANSACTION"))
        return ViewType::Transaction;
    if (boost::iequals(view_type, "LINK"))
        return ViewType::Link;
    return ViewType::Event;
}

}
