#pragma once
#include <map>
#include <vector>

template <class T>
class tree {
public:
    typedef tree self_type;
    typedef T value_type;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef std::vector<self_type*> list_type;
    typedef std::map<int, self_type*> map_type;
    typedef std::bidirectional_iterator_tag iterator_category;
    typedef std::vector<int> index_type;
    typedef std::function<bool(pointer)> visitor_type;

    tree() {}

    ~tree();

    pointer value() const;

    self_type* at(size_type n) const;

    self_type* at(const index_type& index) const;

    self_type* parent() const;

    self_type* push_back(pointer v);

    bool visit(visitor_type visitor);

    size_type width() const;

    size_type depth() const;

    size_type size() const;

private:
    pointer _value = nullptr;
    list_type _children;
    list_type _children_map;
    self_type* _parent = nullptr;
    size_type _depth = 0;
    size_type _size = 0;

    void update();

    void clear();

    ///////////////////////////////////
    //    class iterator {
    //    public:
    //        iterator() {}

    //        self_type& operator*() const
    //        {
    //            return *node;
    //        }
    //        self_type* operator->() const
    //        {
    //            return node;
    //        }

    //        bool operator==(const iterator& rhs) const
    //        {
    //            return node == rhs.node;
    //        }

    //        bool operator!=(const iterator& rhs) const
    //        {
    //            return node != rhs.node;
    //        }

    //        iterator& operator++()
    //        {
    //            self_type *child = this->node;
    //            self_type *parent = this->node._parent;
    //            while(child && parent) {
    //                if (child.idx)
    //            }

    //            assert(this->node != nullptr);
    //            assert(this->node->_parent != nullptr);

    //            return *this;
    //        }

    //        iterator& operator--();
    //        iterator operator++(int);
    //        iterator operator--(int);
    //        iterator& operator+=(unsigned int);
    //        iterator& operator-=(unsigned int);

    //    protected:
    //        self_type* node = nullptr;
    //        index_type idx = {};
    //    };
};

template <class T>
void tree<T>::update()
{
    _size = 1;
    auto t = _parent;
    auto rev_height = _depth;
    while (t) {
        rev_height++;
        ++t->_size;
        if (t->_depth < rev_height)
            t->_depth = rev_height;
        t = t->_parent;
    }
}

template <class T>
void tree<T>::clear()
{
    for (auto e : _children)
        e->clear();
    _children.clear();
    delete _value;
}

template <class T>
tree<T>::~tree()
{
    clear();
}

template <class T>
typename tree<T>::pointer tree<T>::value() const
{
    return _value;
}

template <class T>
typename tree<T>::self_type* tree<T>::at(tree<T>::size_type n) const
{
    return _children.at(n);
}

template <class T>
typename tree<T>::self_type* tree<T>::at(const tree<T>::index_type& index) const
{
    self_type* ret = this;
    for (auto idx : index)
        ret = ret->_children.at(idx);
    return ret;
}

template <class T>
typename tree<T>::self_type* tree<T>::parent() const
{
    return _parent;
}

template <class T>
typename tree<T>::self_type* tree<T>::push_back(tree<T>::pointer v)
{
    auto t = new tree;
    t->_value = v;
    t->_parent = this;
    _children.push_back(t);
    //        t->_children_map.insert({ id, t });
    t->update();
    return t;
}

template <class T>
bool tree<T>::visit(tree<T>::visitor_type visitor)
{
    if (_value && visitor(_value))
        return true;
    for (auto e : _children)
        if (e->visit(visitor))
            return true;
    return false;
}

template <class T>
typename tree<T>::size_type tree<T>::width() const
{
    return _children.size();
}

template <class T>
typename tree<T>::size_type tree<T>::depth() const
{
    return _depth;
}

template <class T>
typename tree<T>::size_type tree<T>::size() const
{
    return _size;
}
