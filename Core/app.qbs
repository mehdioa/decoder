import qbs

DynamicLibrary {
    name: "DecoderCore"
//    qbs.architecture: project.arch
    files: [
        "**/*.cpp",
        "**/*.h",
    ]


    install: true

    Depends {name: "cpp"}

    installImportLib: true
    cpp.defines: ["BOOST_DYNAMIC_BITSET_DONT_USE_FRIENDS", "DECODERCORE_EXPORTS"]

    cpp.libraryPaths: project.LIB_DIRS

    cpp.cxxLanguageVersion: project.cxxLanguageVersion

    cpp.dynamicLibraries:  [
//        "pugixml"
    ]

    cpp.includePaths: {
        var res = project.INC_DIRS;
        res.push(".");
        res.push("..");
        return res;
    }

    Group
    {
        name:"PCH"
        fileTags:"cpp_pch_src"
        files:["pch.hpp"]
        qbs.install: true
        qbs.installSourceBase: ""
        qbs.installDir: "include/Decoder/Core"

    }

    cpp.useCxxPrecompiledHeader: true

    FileTagger {
        patterns: ["*.h", "*.hpp"]
        fileTags: ["header_files"]
    }
    
    Group
    {
        name:"Headers"
        fileTagsFilter: "header_files"
        qbs.install: true
        qbs.installSourceBase: ""
        qbs.installDir: "include/Decoder/Core"
    }
}

