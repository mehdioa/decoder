#pragma once

#include "Buffer.h"
#include "DecodedField.h"

namespace Core {
namespace Base {

    class DECODERCORE_API Protocol {
        DEC_CMA(Protocol);

        void init(pugi::xml_node* node);
        void dump();

        BitOrder bitOrder() const;
        ByteOrder byteOrder() const;
        DecodedFieldTree* decode(IBuffer* buf);

        static unsigned long calculate_crc(unsigned long* frame, unsigned long length);

        static uint32_t hash(uint32_t upperbits, uint32_t lowerbits);

        void visitFields(FieldVisitor visitor) const;
    };
}
}
