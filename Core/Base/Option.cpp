// clang-format off
#include "pch.hpp"
#include "Option.h"
// clang-format on

namespace Core {
namespace Base {

    struct Option::Impl {
        Option* m_Parent;
        Field* m_FieldParent = nullptr;

        uint64_t m_Min = 0;
        uint64_t m_Max = 0;
        int m_Ref = 0;
        std::string m_Name = "";
        std::string m_RefOption;

        VecFields m_Fields;

        Impl(Option* parent)
            : m_Parent(parent)
        {
        }

        void init(pugi::xml_node* node, FieldInfos* fieldinfos, Field* parent)
        {
            m_FieldParent = parent;
            for (auto attr = node->first_attribute(); attr; attr = attr.next_attribute()) {
                auto name = std::string(attr.name());
                if (name == "min")
                    m_Min = std::stoull(attr.value(), nullptr, 16);
                else if (name == "max")
                    m_Max = std::stoull(attr.value(), nullptr, 16);
                else if (name == "ref")
                    m_Ref = attr.as_int();
                else if (name == "name")
                    m_Name = std::string(attr.value());
                else if (name == "ref_option")
                    m_RefOption = std::string(attr.value());
                else
                    Error("Unknown attribute {}", name);
            }
            for (auto child = node->first_child(); child; child = child.next_sibling()) {
                auto name = std::string(child.name());
                if (name == "field") {
                    auto new_field = new Field;
                    new_field->init(&child, m_FieldParent->bitOrder(), fieldinfos, m_Parent);
                    m_Fields.push_back(new_field);
                } else
                    Error("Unknonwn child {}", name);
            }
        }

        ~Impl()
        {
            for (auto f : m_Fields)
                delete f;
        }

        void dump()
        {
            Debug("Option Name {0} Min 0x{1:X} Max 0x{2:X} Ref {3} of {4}", m_Name, m_Min, m_Max, m_Ref, m_Parent->name());
            for (auto field : m_Fields)
                field->dump();
        }

        std::pair<uint64_t, uint64_t> minMax() const
        {
            return std::make_pair(m_Min, m_Max);
        }

        size_t decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields, DecodedFieldTree* df) const
        {
            size_t ret = 0;
            for (auto f : m_Fields)
                ret += f->decode(buf, start + ret, ref_fields, df);
            return ret;
        }

        std::string refOption() const
        {
            return m_RefOption;
        }

        void visit(FieldVisitor v)
        {
            for (auto f : m_Fields) {
                f->traverse(v);
            }
        }
    };

    DEF_CMA(Option);

    Option::Option(const Option& other)
    {
    }

    void Option::init(pugi::xml_node* node, FieldInfos* fieldinfos, Field* parent)
    {
        d->init(node, fieldinfos, parent);
    }

    std::pair<uint64_t, uint64_t> Option::minMax() const
    {
        return std::make_pair(d->m_Min, d->m_Max);
    }

    std::string Option::name() const
    {
        return d->m_Name;
    }

    void Option::dump()
    {
        return d->dump();
    }

    size_t Option::decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields, DecodedFieldTree* df) const
    {
        return d->decode(buf, start, ref_fields, df);
    }

    std::string Option::refOption() const
    {
        return d->refOption();
    }

    void Option::visit(FieldVisitor v)
    {
        return d->visit(v);
    }
}
}
