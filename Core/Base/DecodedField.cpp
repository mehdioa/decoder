// clang-format off
#include "pch.hpp"
#include "DecodedField.h"
#include "Field.h"
#include "Option.h"
// clang-format on

namespace Core {
	namespace Base {

		struct DecodedField::Impl {
			DecodedField* m_Parent;
			Field* m_Field;
			Option* m_Option;
			size_t m_Start;
			size_t m_Length;

			Impl(DecodedField *parent)
				: m_Parent(parent)
			{}

			void init(size_t start, size_t len, Field* field, Option* option /*= nullptr*/)
			{
				m_Start = start;
				m_Length = len;
				m_Field = field;
				m_Option = option;
			}

			std::string GetValue(ValueType value_type, IBuffer* buf) const
			{
				if (m_Field == nullptr) {
					Error("Cannot get value of null field");
					return "";
				}
				auto value = buf->toString(m_Start, m_Length, value_type, m_Field->bitOrder());
				if (m_Option != nullptr) {
					auto option_name = m_Option->name();
					if (option_name != "")
						value += ": " + option_name;
				}
				return value;
			}
		};

		DEF_CMA(DecodedField);

		size_t DecodedField::start() const
		{
			return d->m_Start;
		}

		size_t DecodedField::length() const
		{
			return d->m_Length;
		}

		void DecodedField::setLength(size_t len)
		{
			d->m_Length = len;
		}

		void DecodedField::setOption(Option* option)
		{
			d->m_Option = option;
		}

		Option* DecodedField::option() const
		{
			return d->m_Option;
		}

		Field* DecodedField::field() const
		{
			return d->m_Field;
		}

		std::string DecodedField::value(ValueType value_type, IBuffer* buf) const
		{
			return d->GetValue(value_type, buf);
		}

		void DecodedField::init(size_t start, size_t len, Field* field, Option* option /*= nullptr*/)
		{
			return d->init(start, len, field, option);
		}

		void DecodedField::dump()
		{

		}

	}
}