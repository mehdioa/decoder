#pragma once

#include "DecodedField.h"

#include "Option.h"

namespace Core {
namespace Base {

    class DECODERCORE_API Field {
        DEC_CMA(Field);

        void init(pugi::xml_node* node, BitOrder bit_order, FieldInfos* fieldinfos, Option* parent = nullptr);
        void dump();

        void traverse(OptionVisitor v);
        void traverse(FieldVisitor v);

        std::string name() const;
        std::string ref() const;
        size_t decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields, DecodedFieldTree* df) const;
        std::string refId() const;
        bool hasId() const;
        IdType fieldId() const;
        void setFieldId(IdType& id);
        std::string id() const;
        FieldType fieldType() const;
        BitOrder bitOrder() const;
        size_t optionsSize() const;
        void initFieldRefs(const MapField& ref_map);

        size_t fieldInfoIndex() const;
    };
}
}
