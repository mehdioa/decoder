// clang-format off
#include "pch.hpp"
#include "EditableBuffer.h"

// clang-format on

namespace Core {
namespace Base {

    struct EditableBuffer::Impl {
        EditableBuffer* m_Parent;
        IBuffer* m_Buffer;
        IBuffer* m_Mask;

        Impl(EditableBuffer* parent)
            : m_Parent(parent)
        {
        }

        size_t size() const noexcept
        {
            if (m_Buffer->size() < m_Mask->size())
                return m_Buffer->size();
            return m_Mask->size();
        }

        void clear() noexcept
        {
            m_Buffer->clear();
            m_Mask->clear();
        }

        uint64_t value(size_t start, size_t len, BitOrder bit_order) const
        {
            return m_Buffer->value(start, len, bit_order);
        }

        BitSet data(size_t start, size_t len, BitOrder bit_order) const
        {
            return m_Buffer->data(start, len, bit_order);
        }

        std::string toString(size_t start, size_t len, ValueType value_type, BitOrder bit_order) const
        {
            std::stringstream ss;
            ss << std::uppercase;
            size_t base = 2;
            switch (value_type) {
            case ValueType::Hex:
                ss << "0x";
                ss << std::hex;
                base = 8;
                break;
            case ValueType::Bin:
                ss << "0b";
                base = 2;
                break;
            default:
                auto d = m_Buffer->data(start, len, bit_order);
                auto m = m_Mask->data(start, len, bit_order);
                for (size_t i = 0; i < d.size(); i++)
                    if (m.test_set(i)) {
                        ss << "X";
                    } else {
                        ss << d[i];
                    }
                return ss.str();
            }

            while (start < size() && len > 0) {
                auto min_len_base = std::min(len, base);
                if (m_Mask->value(start, min_len_base, bit_order) != 0)
                    ss << "X";
                else
                    ss << m_Buffer->value(start, min_len_base, bit_order);
                len -= min_len_base;
                start += min_len_base;
            }
            return ss.str();
        }

        void setData(size_t start, BitSet* data, BitSet* mask)
        {
            m_Buffer->setData(start, data, nullptr);
            m_Mask->setData(start, mask, nullptr);
        }

        void init(IBuffer* buf, IBuffer* msk)
        {
            m_Buffer = buf;
            m_Mask = msk;
        }
    };

    DEF_CMA(EditableBuffer);

    void EditableBuffer::init(IBuffer* buf, IBuffer* msk)
    {
        return d->init(buf, msk);
    }

    void EditableBuffer::append(uint8_t val)
    {
        d->m_Buffer->append(val);
    }

    size_t EditableBuffer::size() const noexcept
    {
        return d->size();
    }

    void EditableBuffer::clear() noexcept
    {
        return d->clear();
    }

    uint64_t EditableBuffer::value(size_t start, size_t len, BitOrder bit_order) const
    {
        return d->value(start, len, bit_order);
    }

    BitSet EditableBuffer::data(size_t start, size_t len, BitOrder bit_order) const
    {
        return d->data(start, len, bit_order);
    }

    std::string EditableBuffer::toString(size_t start, size_t len, ValueType value_type, BitOrder bit_order) const
    {
        return d->toString(start, len, value_type, bit_order);
    }

    void EditableBuffer::setData(size_t start, BitSet* data, BitSet* mask)
    {
        return d->setData(start, data, mask);
    }

    template <typename BlockInputIterator>
    void EditableBuffer::set(BlockInputIterator first, BlockInputIterator last)
    {
    }
}
}
