#pragma once

#include "Buffer.h"
#include "Field.h"

namespace Core {
namespace Base {

    class DECODERCORE_API Option {
        DEC_CMA(Option);

        Option(const Option& other);

        void init(pugi::xml_node* node, FieldInfos* fieldinfos, Field* parent);
        std::pair<uint64_t, uint64_t> minMax() const;

        std::string name() const;

        void dump();

        size_t decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields, DecodedFieldTree* df) const;

        std::string refOption() const;

        void visit(FieldVisitor v);
    };

}
}
