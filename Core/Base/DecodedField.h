#pragma once

namespace Core {
	namespace Base {

		class DECODERCORE_API DecodedField {
		public:
			DEC_CMA(DecodedField);

			void init(size_t start, size_t len, Field* field, Option* option = nullptr);
			void dump();

			size_t start() const;
			size_t length() const;
			void setLength(size_t len);
			void setOption(Option* option);

			Option* option() const;

			Field* field() const;

			std::string value(ValueType value_type, IBuffer* buf) const;
		};

	}
}