#pragma once

namespace Core {
namespace Base {

    class DECODERCORE_API IBuffer
    {
    public:
        virtual ~IBuffer();

        virtual void append(uint8_t val) = 0;
        virtual size_t size() const noexcept = 0;
        virtual void clear() noexcept = 0;
        virtual uint64_t value(size_t start, size_t len, BitOrder bit_order) const = 0;
        virtual BitSet data(size_t start, size_t len, BitOrder bit_order) const = 0;
        virtual void setData(size_t start, BitSet* data, BitSet* mask = nullptr) = 0;
        virtual std::string toString(size_t start, size_t len, ValueType value_type, BitOrder bit_order) const = 0;
    };

}
}
