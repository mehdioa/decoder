#pragma once

template <class T>
class TreeHelper {
public:
    typedef typename tree<T>::self_type tree_type;

    TreeHelper();
    virtual ~TreeHelper();

    virtual int sections(tree_type* tr) const
    {
        return 1;
    }
    virtual std::string data(tree_type* tr) const
    {
        return "";
    }
};
