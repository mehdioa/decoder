#pragma once

// clang-format off
#include "Field.h"

#include <tao/pegtl.hpp>
#include <tao/pegtl/contrib/parse_tree.hpp>
#include <tao/pegtl/string_input.hpp>

using namespace tao::TAO_PEGTL_NAMESPACE; // NOLINT

namespace tao {
	namespace TAO_PEGTL_NAMESPACE {
		namespace parse_tree {
			struct Node
				: parse_tree::basic_node<Node> {

                size_t value;
			};

			template <typename Rule,
				template <typename...> class Selector = internal::store_all,
				template <typename...> class Action = nothing,
				template <typename...> class Control = normal,
				typename Input,
				typename... States>
				std::unique_ptr<Node> Parse(Input&& in, States&& ... st)
			{
				return parse<Rule, Node, Selector, Action, Control>(in, st...);
			}
		}
		namespace Attribute {
			// the grammar

			struct Integer : plus< digit > {};
			struct SizeVariable : seq<one<'$'>, identifier> {};
			struct ValueVariable : seq<one<'#'>, identifier> {};

			struct Plus : pad< one< '+' >, space > {};
			struct Minus : pad< one< '-' >, space > {};
			struct Multiply : pad< one< '*' >, space > {};
			struct Divide : pad< one< '/' >, space > {};

			struct OpenBracket : seq< one< '(' >, star< space > > {};
			struct CloseBracket : seq< star< space >, one< ')' > > {};

			struct Expression;
			struct Bracketed : if_must< OpenBracket, Expression, CloseBracket > {};
			struct Value : sor< Integer, sor<SizeVariable, ValueVariable>, Bracketed > {};
			struct Product : list_must< Value, sor< Multiply, Divide > > {};
			struct Expression : list_must< Product, sor< Plus, Minus > > {};

			struct Grammar : seq< Expression, eof > {};

			// after a node is stored successfully, you can add an optional transformer like this:
			struct Rearrange : std::true_type
			{
				// recursively rearrange nodes. the basic principle is:
				//
				// from:          PROD/EXPR
				//                /   |   \          (LHS... may be one or more children, followed by OP,)
				//             LHS... OP   RHS       (which is one operator, and RHS, which is a single child)
				//
				// to:               OP
				//                  /  \             (OP now has two children, the original PROD/EXPR and RHS)
				//         PROD/EXPR    RHS          (Note that PROD/EXPR has two fewer children now)
				//             |
				//            LHS...
				//
				// if only one child is left for LHS..., replace the PROD/EXPR with the child directly.
				// otherwise, perform the above transformation, then apply it recursively until LHS...
				// becomes a single child, which then replaces the parent node and the recursion ends.
				template< typename... States >
				static void transform(std::unique_ptr< parse_tree::Node >& n, States&& ... st)
				{
					if (n->children.size() == 1) {
						n = std::move(n->children.back());
					}
					else {
						n->remove_content();
						auto& c = n->children;
						auto r = std::move(c.back());
						c.pop_back();
						auto o = std::move(c.back());
						c.pop_back();
						o->children.emplace_back(std::move(n));
						o->children.emplace_back(std::move(r));
						n = std::move(o);
						transform(n->children.front(), st...);
					}
				}
			};

			struct StoreIntegerContent : std::true_type
			{
				template<typename State>
				static void transform(std::unique_ptr< parse_tree::Node >& n, State&& st)
				{
                    UNUSED(st);
					n->value = std::stoull(n->content());
				}
				template<typename... States >
				static void transform(std::unique_ptr< parse_tree::Node >& n, States&& ... st)
				{
				}
			};

			struct StoreIndexedContent : std::true_type
			{
				template<typename State>
				static void transform(std::unique_ptr< parse_tree::Node >& n, State&& st)
				{
					auto content = n->content();
					n->value = st.at(content.substr(1))->fieldId();
				}
				template<typename... States >
				static void transform(std::unique_ptr< parse_tree::Node >& n, States&& ... st)
				{
                    UNUSED(n);
//                    UNUSED(st);
				}
			};

			// select which rules in the grammar will produce parse tree nodes:
			template< typename Rule >
			using selector = parse_tree::selector<
				Rule,
                parse_tree::apply<StoreIntegerContent>::on<
				Integer>,
                parse_tree::apply<StoreIndexedContent>::on<
				SizeVariable, ValueVariable >,
                parse_tree::remove_content::on<
				Plus,
				Minus,
				Multiply,
				Divide >,
                parse_tree::apply< Rearrange >::on<
				Product,
				Expression > >;
		} // namespace Attribute
	}
}
// clang-format on
