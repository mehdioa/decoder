// clang-format off
#include "pch.hpp"
#include "DecodedField.h"
#include "Field.h"
#include "Protocol.h"
#include "../tree.h"
#include "FieldInfos.h"
// clang-format on

namespace Core {
namespace Base {

    struct Protocol::Impl {
        Protocol* m_Parent;
        std::string m_Name;
        BitOrder m_BitOrder;
        ByteOrder m_ByteOrder;

        std::string m_Version;

        VecFields m_Fields;
        MapField m_RefFields;

        FieldInfos* m_FieldInfos = new FieldInfos;

        unsigned int m_RefFieldCount = 0;

        Impl(Protocol* parent)
            : m_Parent(parent)
        {
        }

        void Init(pugi::xml_node* node)
        {
            for (auto attr = node->first_attribute(); attr;
                 attr = attr.next_attribute()) {
                auto name = std::string(attr.name());
                if (name == "name")
                    m_Name = std::string(attr.value());
                else if (name == "version")
                    m_Version = std::string(attr.value());
                else if (name == "byte_order")
                    m_ByteOrder = StringToByteOrder(attr.value());
                else if (name == "bit_order")
                    m_BitOrder = StringToBitOrder(attr.value());
                else
                    Error("Unknown attribute {}", name);
            }
            for (auto child = node->first_child(); child;
                 child = child.next_sibling()) {
                auto name = std::string(child.name());
                if (name == "fields") {
                    for (auto ch = child.first_child(); ch; ch = ch.next_sibling()) {
                        auto new_field = new Field;
                        new_field->init(&ch, m_BitOrder, m_FieldInfos);
                        m_Fields.push_back(new_field);
                    }
                } else if (name == "fieldinfos") {
                    m_FieldInfos->init(&child);
                } else {
                    Error("Unknonwn child {}", name);
                }
            }

            SetFields ref_fields;

            auto collect_ref_fields = [&ref_fields](Field* f) {
                if (f->id() != "")
                    ref_fields.insert(f);
                return false;
            };

            for (auto f : m_Fields) {
                f->traverse(collect_ref_fields);
            }

            RefMap ref_map;

            for (auto field : ref_fields) {
                if (field->hasId()) {
                    Error("Duplicate setting field id {}", field->id());
                } else {
                    field->setFieldId(m_RefFieldCount);
                    m_RefFields.insert({ field->id(), field });
                    ref_map.insert({ field->id(), m_RefFieldCount });
                    m_RefFieldCount++;
                }
            }

            auto init_fields = [this](Field* f) {
                f->initFieldRefs(m_RefFields);
                return false;
            };

            for (auto f : m_Fields) {
                f->traverse(init_fields);
            }

            //        for (auto field : ref_fields) {
            //            for (auto ref : ref_fields) {
            //                field->setRefFieldId(ref);
            //            }
            //        }
        }

        ~Impl() { clear(); }

        void clear()
        {
            for (auto f : m_Fields)
                delete f;
            if (m_FieldInfos)
                m_FieldInfos->clear();
            m_Fields.clear();
            m_RefFields.clear();
            m_Name = "";
            m_Version = "";
        }
    };

    DEF_CMA(Protocol);

    void Protocol::init(pugi::xml_node* node) { d->Init(node); }

    void Protocol::dump()
    {
        Debug("Protocol Name {} ByteOrder {} BitOrder {} Version {}", d->m_Name,
            ByteOrderToString(d->m_ByteOrder),
            BitOrderToSrting(d->m_BitOrder), d->m_Version);
        for (auto field : d->m_Fields)
            field->dump();
    }

    BitOrder Protocol::bitOrder() const
    {
        return d->m_BitOrder;
    }

    ByteOrder Protocol::byteOrder() const
    {
        return d->m_ByteOrder;
    }

    DecodedFieldTree* Protocol::decode(IBuffer* buf)
    {
        auto df = new DecodedFieldTree;
        VecDecodedField ref_df(d->m_RefFieldCount);
        size_t start = 0;
        for (auto f : d->m_Fields) {
            auto consumed = f->decode(buf, start, ref_df, df);
            if (consumed <= 0)
                break;
            start += consumed;
        }
        return df;
    }

    unsigned long Protocol::calculate_crc(unsigned long* frame, unsigned long length)
    {
        long poly = 0x04C11DB7L;
        unsigned long crc_gen, x;
        union {
            unsigned long lword;
            unsigned char byte[4];
        } b_access;
        static unsigned char xpose[] = {
            0x0, 0x8, 0x4, 0xC, 0x2, 0xA, 0x6, 0xE,
            0x1, 0x9, 0x5, 0xD, 0x3, 0xB, 0x7, 0xF
        };
        unsigned int i, j, fb;
        crc_gen = ~0; /* seed generator with all ones */
        for (i = 0; i < length; i++) {
            x = *frame++; /* get word */
            b_access.lword = x; /* transpose bits in byte */
            for (j = 0; j < 4; j++) {
                b_access.byte[j] = xpose[b_access.byte[j] >> 4] | xpose[b_access.byte[j] & 0xF] << 4;
            }
            x = b_access.lword;
            for (j = 0; j < 32; j++) { /* serial shift register implementation */
                fb = ((x & 0x80000000L) > 0) ^ ((crc_gen & 0x80000000L) > 0);
                x <<= 1;
                crc_gen <<= 1;
                if (fb)
                    crc_gen ^= poly;
            }
        }
        b_access.lword = crc_gen; /* transpose bits in CRC */
        for (j = 0; j < 4; j++) {
            b_access.byte[j] = xpose[b_access.byte[j] >> 4] | xpose[b_access.byte[j] & 0xF] << 4;
        }
        crc_gen = b_access.lword;
        return ~crc_gen; /* invert output */
    }

    uint32_t Protocol::hash(uint32_t upperbits, uint32_t lowerbits)
    {
        const unsigned distance_9_poly = 0x01DB2777;
        uint32_t msb = 0x01000000;
        uint32_t moving_one, leading_bit;
        int i;
        unsigned regg;
        regg = 0;
        moving_one = 0x80000000;
        for (i = 31; i >= 0; i--) {
            leading_bit = 0;
            if (moving_one & upperbits)
                leading_bit = msb;
            regg <<= 1;
            regg ^= leading_bit;
            if (regg & msb)
                regg ^= distance_9_poly;
            moving_one >>= 1;
        }
        moving_one = 0x80000000;
        for (i = 31; i >= 0; i--) { // note lower limit of i = 0;
            leading_bit = 0;
            if (moving_one & lowerbits)
                leading_bit = msb;
            regg <<= 1;
            regg ^= leading_bit;
            if (regg & msb)
                regg ^= distance_9_poly;
            moving_one >>= 1;
        }
        return regg & 0x00FFFFFF;
    }

    void Protocol::visitFields(FieldVisitor visitor) const
    {
        for (auto const& field : d->m_Fields) {
            if (visitor(field))
                return;
        }
    }

}
}
