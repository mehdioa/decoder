// clang-format off
#include "pch.hpp"
#include "FieldInfos.h"
// clang-format on

namespace Core {
namespace Base {

    struct FieldInfos::Impl {
        FieldInfos* m_Parent;
        VecFieldInfo m_FieldInfos;
        std::map<std::string, size_t> m_FieldInfoIds;

        Impl(FieldInfos* parent)
            : m_Parent(parent)
        {
        }

        void init(pugi::xml_node* node)
        {
            for (auto child = node->first_child(); child; child = child.next_sibling()) {
                auto name = std::string(child.name());
                if (name == "fieldinfo") {
                    auto field_info = new FieldInfo;
                    for (auto attr = child.first_attribute(); attr; attr = attr.next_attribute()) {
                        auto name = std::string(attr.name());
                        if (name == "name")
                            field_info->Name = std::string(attr.value());
                        else if (name == "id")
                            field_info->Id = std::string(attr.value());
                        else
                            Error("Unknown attribute {}", name);
                    }
                    auto ret = m_FieldInfoIds.insert({ field_info->Id, m_FieldInfos.size() });
                    if (!ret.second) {
                        Error("FieldInfo {} already exists", field_info->Id);
                        delete field_info;
                    } else {
                        m_FieldInfos.push_back(field_info);
                    }
                } else
                    Error("Unknonwn child {}", name);
            }
        }

        ~Impl()
        {
            for (auto f : m_FieldInfos)
                delete f;
        }
    };

    DEF_CMA(FieldInfos);

    void FieldInfos::init(pugi::xml_node* node)
    {
        d->init(node);
    }

    std::pair<size_t, FieldInfo*> FieldInfos::fieldInfo(const std::string& ref) const
    {
        return { d->m_FieldInfoIds.at(ref), d->m_FieldInfos.at(d->m_FieldInfoIds.at(ref)) };
    }

    void FieldInfos::clear()
    {
        d->m_FieldInfos.clear();
        d->m_FieldInfoIds.clear();
    }
}
}
