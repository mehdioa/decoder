#pragma once

template <class T, size_t N>
class DECODERCORE_API RecentList {
public:
    typedef std::list<T> Recents;

    void Add(const T& value)
    {
        auto it = std::find(Values.begin(), Values.end(), value);
        if (it != Values.end())
            Values.erase(it);
        Values.push_front(value);
        while (Values.size() > N)
            Values.pop_back();
    }

    void Insert(const T& value, size_t pos)
    {
        if (0 <= pos && pos < N) {
            auto it = Values.front();
            for (auto i = 0; i < pos; ++i)
                ++it;
            Values.insert(it, value);
        }
    }

    Recents Get() const
    {
        return Values;
    }

    T Head() const
    {
        return Values.front();
    }

private:
    Recents Values;
};
