#pragma once

template <class T>
class CacheVector {
    uint32_t m_Start;
    uint32_t m_Current;
    uint32_t m_Size;
    std::vector<T*> m_Items;

public:
    ~CacheVector()
    {
        for (auto item : m_Items)
            delete item;
    }

    //    void setStart()
};
