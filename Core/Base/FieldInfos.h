#pragma once

namespace Core {
namespace Base {

    class DECODERCORE_API FieldInfos {
        DEC_CMA(FieldInfos);

        void init(pugi::xml_node* node);

        /**
         * @brief fieldInfo return FieldInfo and Index of it
         * @param ref field reference
         * @return
         */
        std::pair<size_t, FieldInfo*> fieldInfo(const std::string& ref) const;

        void clear();
    };

}
}
