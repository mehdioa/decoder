// clang-format off
#include "pch.hpp"
#include "Field.h"
#include "Option.h"
#include "SizeExpression.h"
#include "../tree.h"
#include "FieldInfos.h"
// clang-format on

namespace Core {
namespace Base {
    struct PresentExpression {

        std::string m_Ref = ""; // ref to a field that its value force this field to be presented
        std::string m_PresentRef = ""; // ref to the option of the PresentRef
            // that force this field to be presented
        IdType m_RefFieldId = -1; // id of the field that its option defines

        void init(const std::string& expr)
        {
            auto strings = SplitString(expr, '@');
            try {
                m_Ref = strings.at(0);
                m_PresentRef = strings.at(1);
            } catch (const std::out_of_range&) {
                Warn("Corrupted present expression {}", expr);
            }
        }

        void setId(const MapField& ref_map)
        {
            if (m_Ref != "") {
                try {
                    m_RefFieldId = ref_map.at(m_Ref)->fieldId();
                } catch (const std::out_of_range&) {
                    Warn("No field with id {}", m_Ref);
                }
            }
        }
    };

    struct OptionReference {
        //    std::string m_OptionRef = ""; // reference to field that its options defines this field option
        OptionMap m_OptionMap; // map of ref_field_option-->this_field_option
        // options of this field that internally will be used to construct the m_OptionMap
        std::map<std::string, Option*> m_RefOptions;
        IdType m_OptionRefFieldId = -1;

        std::string m_OptionRef = "";

        void setId(const MapField& ref_map)
        {
            if (m_OptionRef != "") {
                try {
                    auto ref_field = ref_map.at(m_OptionRef);
                    m_OptionRefFieldId = ref_field->fieldId();
                    ref_field->traverse([this](Option* op) {
                        auto name = op->name();
                        try {
                            m_OptionMap.insert({ op, m_RefOptions.at(name) });
                        } catch (...) {
                            Warn("There is no option with the name {}", name);
                        }
                        return false;
                    });
                } catch (...) {
                    Warn("There is no option reference with name {}", m_OptionRef);
                }
            }
        }
    };

    struct Field::Impl {
        std::string m_Ref = "";
        FieldInfo* m_FieldInfo = nullptr;
        FieldType m_FieldType = FieldType::Integral;
        BitOrder m_BitOrder = BitOrder::MSB;
        std::string m_Id = ""; // id of the field
        size_t m_FieldInfoIndex = 0;

        int m_OccuranceCount = 1;
        std::string m_OccurenceCountRef = "";

        OptionReference m_OptionReference;

        SizeExpression m_SizeExpression;
        PresentExpression m_PresentExpression;

        IdType m_FieldId = 0;
        bool m_HasId = false;

        OptionIntervalMap m_Options;
        Option* m_Option = nullptr;

        VecFields m_Fields;

        Field* m_Parent;

        Impl(Field* parent)
            : m_Parent(parent)
        {
        }

        Impl(const Impl& rhs)
        {
        }
        ~Impl()
        {
            for (auto op : m_Options)
                delete op.second;

            for (auto f : m_Fields)
                delete f;
        }

        void setFieldId(IdType& id)
        {
            m_HasId = true;
            m_FieldId = id;
        }

        void init(pugi::xml_node* node, BitOrder bit_order, FieldInfos* fieldinfos, Option* parent)
        {
            m_Option = parent;
            for (auto attr = node->first_attribute(); attr;
                 attr = attr.next_attribute()) {
                auto name = std::string(attr.name());
                if (name == "ref")
                    m_Ref = std::string(attr.value());
                else if (name == "size")
                    m_SizeExpression.setPattern(std::string(attr.value()));
                else if (name == "type")
                    m_FieldType = StringToFieldType(attr.value());
                else if (name == "id")
                    m_Id = std::string(attr.value());
                else if (name == "option_ref")
                    m_OptionReference.m_OptionRef = std::string(attr.value());
                else if (name == "present_ref")
                    m_PresentExpression.init(std::string(attr.value()));
                else if (name == "occurance_count")
                    m_OccuranceCount = attr.as_int();
                else if (name == "occurance_count_ref")
                    m_OccurenceCountRef = std::string(attr.value());
                else if (name == "bit_order")
                    m_BitOrder = StringToBitOrder(attr.value());
                else
                    Error("Unknown attribute {}", name);
            }
            try {
                auto field_info_index = fieldinfos->fieldInfo(m_Ref);
                m_FieldInfo = field_info_index.second;
                m_FieldInfoIndex = field_info_index.first;
            } catch (...) {
                Error("No such field ref {}", m_Ref);
            }

            //field can only have option children
            for (auto child = node->first_child(); child;
                 child = child.next_sibling()) {
                auto name = std::string(child.name());
                if (name == "option") {
                    auto option = new Option;
                    option->init(&child, fieldinfos, m_Parent);
                    if (m_OptionReference.m_OptionRef == "") {
                        auto min_max = option->minMax();
                        m_Options.insert(std::make_pair(OptionValue::closed(min_max.first, min_max.second), option));
                    } else {
                        m_OptionReference.m_RefOptions.insert({ option->refOption(), option });
                    }
                } else if (name == "field") {
                    if (!m_Options.empty()) {
                        Error("A field cannot have both options and subfields {}", node->name());
                        auto field = new Field;
                        field->init(&child, bit_order, fieldinfos);
                        m_Fields.push_back(field);
                    }
                } else
                    Error("Unknonwn child {}", name);
            }
        }

        void Dump()
        {
            Debug("Field Ref '{}' Size {} Type '{}' id '{}' Ref '{}' "
                  "PresentRef '{}'",
                m_Ref, m_SizeExpression.getPattern(), FieldTypeToString(m_FieldType), m_Id, m_OptionReference.m_OptionRef,
                m_PresentExpression.m_Ref);
            for (auto option : m_Options)
                option.second->dump();
        }

        size_t decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields,
            DecodedFieldTree* df)
        {
            auto ret = m_SizeExpression.size(ref_fields);
            auto new_field = new DecodedField();
            new_field->init(start, ret, m_Parent);
            auto dft = df->push_back(new_field);
            if (m_HasId) {
                ref_fields[m_FieldId] = new_field;
            }

            if (!m_Fields.empty()) {
                for (auto f : m_Fields) {
                    ret += f->decode(buf, start + ret, ref_fields, dft);
                }
                new_field->setLength(ret);
            } else if (ret < 64) {
                auto data = buf->data(start, ret, m_BitOrder);
                if (data.size() < ret)
                    return 0;
                auto val = data.to_ulong();
                if (!m_Options.empty()) {
                    auto opt = m_Options(val);
                    if (opt) {
                        new_field->setOption(opt);
                        Debug("Found Field '{0}' size {1} Value {2:X} Option Name '{3}'",
                            m_Ref, ret, val, opt->name());

                        ret += opt->decode(buf, start + ret, ref_fields, df);
                        if (m_FieldType != FieldType::Block)
                            new_field->setLength(ret);
                    } else {
                        Debug("Found Field '{0}' size {1} Value {2:X}", m_Ref, ret, val);
                    }
                } else if (!m_OptionReference.m_OptionMap.empty()) {
                    auto ref_field = ref_fields.at(m_OptionReference.m_OptionRefFieldId);
                    if (ref_field) {
                        auto ref_opt = ref_field->option();
                        if (ref_opt) {
                            auto it = m_OptionReference.m_OptionMap.find(ref_opt);
                            if (it != m_OptionReference.m_OptionMap.end()) {
                                new_field->setOption(it->second);
                                ret += it->second->decode(buf, start + ret, ref_fields, dft);
                                new_field->setLength(ret);
                            }
                        }
                    }
                } else {
                    Debug("Found Field '{0}' size {1} Value {2:X}", m_Ref, ret, val);
                }
            } else {
                auto val = buf->data(start, ret, m_BitOrder);
                Debug("Found Field '{0}' size {1} Value {2}", m_Ref, ret, val);
            }
            return ret;
        }

        void initFieldRefs(const MapField& ref_map)
        {
            m_SizeExpression.init(ref_map);
            m_PresentExpression.setId(ref_map);
            m_OptionReference.setId(ref_map);
        }
    };

    DEF_CMA(Field);

    void Field::init(pugi::xml_node* node, BitOrder bit_order, FieldInfos* fieldinfos, Option* parent /*= nullptr*/)
    {
        d->init(node, bit_order, fieldinfos, parent);
    }

    void Field::traverse(FieldVisitor v)
    {
        v(this);
        for (auto f : d->m_Fields)
            f->traverse(v);
        for (auto op : d->m_Options)
            op.second->visit(v);

        for (auto op : d->m_OptionReference.m_RefOptions)
            op.second->visit(v);
    }

    void Field::traverse(OptionVisitor v)
    {
        for (auto op : d->m_Options)
            v(op.second);
    }

    void Field::dump() { return d->Dump(); }

    std::string Field::name() const { return d->m_FieldInfo->Name; }

    std::string Field::ref() const
    {
        return d->m_Ref;
    }

    size_t Field::decode(IBuffer* buf, size_t start, VecDecodedField& ref_fields,
        DecodedFieldTree* df) const
    {
        return d->decode(buf, start, ref_fields, df);
    }

    std::string Field::refId() const { return d->m_OptionReference.m_OptionRef; }

    bool Field::hasId() const
    {
        return d->m_HasId;
    }

    IdType Field::fieldId() const { return d->m_FieldId; }

    void Field::setFieldId(IdType& id) { return d->setFieldId(id); }

    std::string Field::id() const { return d->m_Id; }

    FieldType Field::fieldType() const { return d->m_FieldType; }

    BitOrder Field::bitOrder() const { return d->m_BitOrder; }

    size_t Field::optionsSize() const
    {
        return d->m_Options.size();
    }

    void Field::initFieldRefs(const MapField& ref_map)
    {
        return d->initFieldRefs(ref_map);
    }

    size_t Field::fieldInfoIndex() const
    {
        return d->m_FieldInfoIndex;
    }
}
}
