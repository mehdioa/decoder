#pragma once

#include "FieldSizeParser.h"

namespace Core {
namespace Base {

    class DECODERCORE_API SizeExpression {
        DEC_CMA(SizeExpression);

        void init(const MapField& refs);

        void setPattern(const std::string& pattern);
        std::string getPattern() const;
        size_t size(VecDecodedField& dfs);

    private:
        size_t compute(const parse_tree::Node& n, const VecDecodedField& vdf);
    };
}
}
