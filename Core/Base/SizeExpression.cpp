// clang-format off
#include "pch.hpp"
#include "SizeExpression.h"
// clang-format on

namespace Core {
namespace Base {

    struct SizeExpression::Impl {
        SizeExpression* m_Parent;
        std::function<size_t(const VecDecodedField&)> InternalSizeFunc;
        std::string Pattern;
        std::unique_ptr<parse_tree::Node> Root = nullptr;
        size_t RawValue;

        Impl(SizeExpression* parent)
            : m_Parent(parent)
        {
        }

        Impl(const Impl& rhs) {}

        size_t init(const MapField& refs)
        {
            memory_input<tracking_mode::IMMEDIATE, eol::lf_crlf, std::string> in(Pattern, "");
            Root = parse_tree::Parse<Attribute::Grammar, Attribute::selector>(in, refs);

            if (Root) {
                //        Attribute::print_node(*root, "xxxx");
                if (Root->children.size() == 1) {
                    const auto& node = Root->children.at(0);
                    if (node->is<Attribute::Integer>()) {
                        RawValue = std::stoul(node->content());
                        InternalSizeFunc = [this](const VecDecodedField&) { return RawValue; };
                    } else {
                        InternalSizeFunc = [&node, this](const VecDecodedField& vdf) { return compute(*node, vdf); };
                    }
                } else {
                    Error("Children size error {}", Pattern);
                }
            } else {
                Error("Root error {}", Pattern);
            }
            return 0;
        }

        size_t compute(const parse_tree::Node& n, const VecDecodedField& vdf)
        {
            // detect the root node:
            if (!n.is_root()) {
                if (n.has_content()) {
                    Debug("{} '{}' at {} to {} value {}", n.name(), n.content(), n.begin(), n.end(), n.value);
                    return n.value;
                } else {
                    if (n.is<Attribute::Plus>()) {
                        return compute(*n.children.front(), vdf) + compute(*n.children.back(), vdf);
                    } else if (n.is<Attribute::Minus>()) {
                        return compute(*n.children.front(), vdf) - compute(*n.children.back(), vdf);
                    } else if (n.is<Attribute::Multiply>()) {
                        return compute(*n.children.front(), vdf) * compute(*n.children.back(), vdf);
                    } else if (n.is<Attribute::Divide>()) {
                        return compute(*n.children.front(), vdf) / compute(*n.children.back(), vdf);
                    }
                    //            Debug("{} at {} {}", n.name(), n.begin(), n.is<Attribute::Plus>());
                }
            }
            return 0;
            //    if (!n.is_root())
            //    {
            //        switch (n.) {

            //        }
            //    }
            // print all child nodes
            //    if (!n.children.empty()) {
            //        for (auto& up : n.children) {
            //            Compute(*up, vdf);
            //        }
            //    }
            //    return result;
        }
    };

    DEF_CMA(SizeExpression);

    void SizeExpression::init(const MapField& refs)
    {
        d->init(refs);
    }

    void SizeExpression::setPattern(const std::string& pattern)
    {
        d->Pattern = pattern;
    }

    std::string SizeExpression::getPattern() const
    {
        return d->Pattern;
    }

    size_t SizeExpression::size(VecDecodedField& dfs)
    {
        return d->InternalSizeFunc(dfs);
    }

    size_t SizeExpression::compute(const parse_tree::Node& n, const VecDecodedField& vdf)
    {
        return d->compute(n, vdf);
    }
}
}
