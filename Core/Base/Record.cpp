// clang-format off
#include "pch.hpp"
#include "Record.h"
// clang-format on

namespace Core {
namespace Base {
    struct Record::Impl {
        Record* m_Parent;
        uint32_t m_Index;

        Impl(Record* parent)
            : m_Parent(parent)
        {
        }

        ~Impl()
        {
        }
    };

    DEF_CMA(Record);

}
}
