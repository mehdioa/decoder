#pragma once

#include "IBuffer.h"

namespace Core {
	namespace Base {

		class DECODERCORE_API EditableBuffer : public IBuffer {
			DEC_CMA(EditableBuffer);

			void init(IBuffer* buf, IBuffer* msk);
			template <typename BlockInputIterator>
			void set(BlockInputIterator first, BlockInputIterator last);

			void append(uint8_t val) override;
			size_t size() const noexcept override;
			void clear() noexcept override;
			uint64_t value(size_t start, size_t len, BitOrder bit_order) const override;
			BitSet data(size_t start, size_t len, BitOrder bit_order) const override;
			void setData(size_t start, BitSet* data, BitSet* mask) override;
			std::string toString(size_t start, size_t len, ValueType value_type, BitOrder bit_order) const override;
		};

	}
}