#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Logger : public Node {
        DEC_CMA(Logger);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;

    public:
        void restoreDefault() override;

        void config();

        Log::Level GetLevel() const;
        void SetLevel(Log::Level level);

        Log::Output GetOutput() const;
        void SetOutput(Log::Output output);

        bool GetColored() const;
        void SetColored(bool colored);

        bool GetInverted() const;
        void SetInverted(bool inverted);

        bool GetForceFlush() const;
        void SetForceFlush(bool force);

        std::string GetPattern() const;
        void SetPattern(std::string pattern);

        uint GetMaxFileSize() const;
        void SetMaxFileSize(uint max_file_size);

        uint GetMaxFiles() const;
        void SetMaxFiles(uint max_files);
    };
}
}
