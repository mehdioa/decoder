#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Layout : public Node {
        DEC_CMA(Layout);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;

    public:
        void restoreDefault() override;

        std::string GetGeometry() const;
        void SetGeometry(std::string geometry);

        std::string GetState() const;
        void SetState(std::string state);
    };
}
}
