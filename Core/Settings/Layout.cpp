// clang-format off
#include "pch.hpp"
#include "Layout.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Layout::Impl {
        Parameter<std::string> m_Geometry;
        Parameter<std::string> m_State;

        Layout* m_Parent;

        Impl(Layout* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
            m_Geometry.SetValue("");
            m_State.SetValue("");
        }
    };

    DEF_CMA(Layout);

    void Layout::restoreDefault()
    {
        d->restoreDefault();
    }

    std::string Layout::GetGeometry() const
    {
        return d->m_Geometry.Value();
    }

    void Layout::SetGeometry(std::string geometry)
    {
        d->m_Geometry.SetValue(geometry);
    }

    std::string Layout::GetState() const
    {
        return d->m_State.Value();
    }

    void Layout::SetState(std::string state)
    {
        d->m_State.SetValue(state);
    }

    Node* Layout::initChild(pugi::xml_node* node)
    {
        UNUSED(node);
        return nullptr;
    }

    void Layout::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Geometry") {
            d->m_Geometry.config(attr);
        } else if (name == "State") {
            d->m_State.config(attr);
        }
    }

}
}
