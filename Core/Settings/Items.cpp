// clang-format off
#include "pch.hpp"
#include "Items.h"
#include "Item.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Items::Impl {
        Items* m_Parent;

        static const size_t MaxRecents = 10;
        Parameter<std::string> m_Name;
        std::list<Item*> m_Items;

        Impl(Items* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
        }

        void Sort()
        {
            size_t id = 0;
            for (auto it = m_Items.begin(); it != m_Items.end(); ++it)
                (*it)->SetId(id++);
        }

        void Add(const std::string& value)
        {
            Item* item = nullptr;
            auto it = m_Items.begin();
            for (; it != m_Items.end() && (*it)->Value() != value; ++it)
                ;
            //            auto it = std::find_if(m_Items.begin(), m_Items.end(), [&value](Item* item) { return item->Value() == value; });
            if (it != m_Items.end()) {
                item = *it;
                m_Items.erase(it);
                m_Items.push_front(item);
            } else if (m_Items.size() == MaxRecents) {
                item = m_Items.back();
                m_Items.pop_back();
                item->SetValue(value);
                m_Items.push_front(item);
            } else {
                auto child = m_Parent->AppendChild("Item");
                child.append_attribute("Id") = 0;
                child.append_attribute("Value") = value.c_str();
                item = new Item;
                item->init(&child);
                m_Items.push_front(item);
            }
            Sort();
        }
    };

    DEF_CMA(Items);

    void Items::restoreDefault()
    {
        d->restoreDefault();
    }

    std::string Items::At(size_t index) const
    {
        if (index < d->m_Items.size()) {
            auto it = d->m_Items.begin();
            std::advance(it, index);
            return (*it)->Value();
        }
        Error("Index out of range");
        return "";
    }

    size_t Items::Size() const
    {
        return d->m_Items.size();
    }

    void Items::Add(const std::string& value)
    {
        d->Add(value);
    }

    StringVec Items::All() const
    {
        StringVec vec;
        for (auto it = d->m_Items.begin(); it != d->m_Items.end(); ++it)
            vec.push_back((*it)->Value());
        return vec;
    }

    std::string Items::Name() const
    {
        return d->m_Name.Value();
    }

    Node* Items::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "Item") {
            auto item = new Item();
            item->init(node);
            d->m_Items.push_front(item);
        }
        return nullptr;
    }

    void Items::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Name") {
            d->m_Name.config(attr);
        }
    }

    void Items::afterInit()
    {
        d->m_Items.sort([](const Item* item1, const Item* item2) {
            return item1->Id() < item2->Id();
        });
    }

}
}
