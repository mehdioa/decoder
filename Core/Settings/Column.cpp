// clang-format off
#include "pch.hpp"
#include "Column.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Column::Impl {
        Column* m_Parent;
        Parameter<std::string> m_Value;
        Parameter<int> m_Id;
        Parameter<int> m_Foreground;
        Parameter<int> m_Background;
        size_t m_FieldInfoIndex;

        Impl(Column* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
        }
    };

    DEF_CMA(Column);

    void Column::restoreDefault()
    {
        d->restoreDefault();
    }

    bool Column::operator<(const Column& rhs)
    {
        return Id() < rhs.Id();
    }

    int Column::Id() const
    {
        return d->m_Id.Value();
    }

    void Column::SetId(int id)
    {
        d->m_Id.SetValue(id);
    }

    std::string Column::Value() const
    {
        return d->m_Value.Value();
    }

    void Column::SetValue(const std::string value)
    {
        d->m_Value.SetValue(value);
    }

    int Column::foreground() const
    {
        return d->m_Foreground.Value();
    }

    void Column::setForeground(int argb)
    {
        d->m_Foreground.SetValue(argb);
    }

    int Column::background() const
    {
        return d->m_Background.Value();
    }

    void Column::setBackground(int argb)
    {
        d->m_Background.SetValue(argb);
    }

    void Column::setFieldInfoIndex(size_t index)
    {
        d->m_FieldInfoIndex = index;
    }

    size_t Column::fieldInfoIndex() const
    {
        return d->m_FieldInfoIndex;
    }

    Node* Column::initChild(pugi::xml_node* node)
    {
        UNUSED(node);
        return nullptr;
    }

    void Column::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Value") {
            d->m_Value.config(attr);
        } else if (name == "Id") {
            d->m_Id.config(attr);
        } else if (name == "Foreground") {
            d->m_Foreground.config(attr);
        } else if (name == "Background") {
            d->m_Background.config(attr);
        }
    }

}
}
