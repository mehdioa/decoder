// clang-format off
#include "pch.hpp"
#include "Node.h"

// clang-format on

using namespace Core::Setting;

struct Node::Impl {
    Node* m_Parent;
    pugi::xml_node m_Node;
    Nodes m_Children;
    std::string m_Name;

    Impl(Node* parent)
        : m_Parent(parent)
    {
    }
};

DEF_CMA(Node);

void Node::init(pugi::xml_node* node)
{
    d->m_Node = *node;
    d->m_Name = node->name();

    for (auto attr = d->m_Node.first_attribute(); attr;
         attr = attr.next_attribute()) {
        initAttr(&attr);
    }
    for (auto child = d->m_Node.first_child(); child; child = child.next_sibling()) {
        auto child_node = initChild(&child);
        if (child_node)
            d->m_Children.push_back(child_node);
    }
    afterInit();
}

std::string Node::name() const
{
    return d->m_Name;
}

Nodes Node::children() const
{
    return d->m_Children;
}

pugi::xml_node Node::AppendChild(const std::string& name)
{
    return d->m_Node.append_child(name.c_str());
}

void Node::afterInit()
{
}
