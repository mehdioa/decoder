#pragma once

namespace Core {
namespace Setting {

    template <typename ValueType, typename Dummy = ValueType>
    class DECODERCORE_API Parameter {
    public:
        Parameter()
            : m_Attribute(nullptr)
        {
        }

    protected:
        pugi::xml_attribute m_Attribute;
        ValueType m_Value;

    public:
        virtual ~Parameter() = default;
        ValueType Value() const { return m_Value; }
        std::string StringValue() const { return m_Attribute.value(); }

        virtual void SetValue(ValueType value) = 0;
        virtual void config(pugi::xml_attribute* attr) = 0;
    };

    template <>
    class DECODERCORE_API Parameter<std::string> : public Parameter<std::string, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_string();
        }

        void SetValue(std::string value) override
        {
            m_Value = value;
            m_Attribute.set_value(value.c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<bool> : public Parameter<bool, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_bool();
        }

        void SetValue(bool value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<int> : public Parameter<int, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_int();
        }

        void SetValue(int value) override
        {
            m_Value = value;
            m_Attribute.set_value(value);
        }
    };

    template <>
    class DECODERCORE_API Parameter<uint> : public Parameter<uint, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_uint();
        }

        void SetValue(uint value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<double> : public Parameter<double, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_double();
        }

        void SetValue(double value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<float> : public Parameter<float, void> {
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_float();
        }

        void SetValue(float value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<long long int> : public Parameter<long long int, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_llong();
        }

        void SetValue(long long int value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<unsigned long long int> : public Parameter<unsigned long long int, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = attr->as_ullong();
        }

        void SetValue(unsigned long long int value) override
        {
            m_Value = value;
            std::stringstream ss;
            ss << value;
            m_Attribute.set_value(ss.str().c_str());
        }
    };

    template <>
    class DECODERCORE_API Parameter<Log::Level> : public Parameter<Log::Level, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = static_cast<Log::Level>(attr->as_uint());
        }

        void SetValue(Log::Level value) override
        {
            m_Value = value;
            m_Attribute.set_value(LevelName(m_Value));
        }
    };

    template <>
    class DECODERCORE_API Parameter<Log::Output> : public Parameter<Log::Output, void> {
    public:
        void config(pugi::xml_attribute* attr) override
        {
            m_Attribute = *attr;
            m_Value = static_cast<Log::Output>(attr->as_uint());
        }

        void SetValue(Log::Output value) override
        {
            m_Value = value;
            switch (value) {
            case Log::Output::File:
                m_Attribute.set_value("file");
                break;
            default:
                m_Attribute.set_value("console");
                break;
            }
        }
    };
}
}
