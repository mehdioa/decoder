// clang-format off
#include "pch.hpp"
#include "Protocol.h"
#include "Parameter.h"
#include "ColumnSets.h"
#include "ColumnSet.h"
#include "Column.h"
#include "../Base/Field.h"
#include "../Base/Protocol.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Protocol::Impl {
        Protocol* m_Parent;
        Parameter<std::string> m_Name;
        Parameter<std::string> m_Version;
        Parameter<std::string> m_Revision;
        Parameter<uint32_t> m_Id;
        ColumnSets m_ColumnSets;
        Base::Protocol m_BaseProcotol;

        pugi::xml_document m_Document;

        Impl(Protocol* parent)
            : m_Parent(parent)
        {
        }

        Impl(const Impl&) {}

        ~Impl()
        {
        }

        void restoreDefault()
        {
        }

        void read()
        {
            auto file_name = m_Name.Value() + "-V" + m_Version.Value() + "-R" + m_Revision.Value() + ".xml";
            auto result = m_Document.load_file(file_name.c_str());
            if (result.status != pugi::status_ok) {
                spdlog::critical("Error reading protocol file: {} status: {}", file_name, result.status);
                exit(result.status);
            }
            auto child = m_Document.child("protocol");
            m_BaseProcotol.init(&child);
        }

        void write()
        {
            auto file_name = m_Name.Value() + "_V" + m_Version.Value() + "_R" + m_Revision.Value();
            m_Document.save_file(file_name.c_str());
        }

        void setColumnsFieldIds()
        {
            auto column_vistor = [this](Column* c) {
                auto field_visitor = [&c](Base::Field* field) {
                    if (field->ref() == c->Value()) {
                        c->setFieldInfoIndex(field->fieldInfoIndex());
                        return true;
                    }
                    return false;
                };
                m_BaseProcotol.visitFields(field_visitor);
                return false;
            };

            auto columnset_visitor = [column_vistor](ColumnSet* cs) {
                cs->traverse(column_vistor);
                return false;
            };

            m_ColumnSets.traverse(columnset_visitor);
        }
    };

    DEF_CMA(Protocol);

    void Protocol::restoreDefault()
    {
        d->restoreDefault();
    }

    uint32_t Protocol::Id() const
    {
        return d->m_Id.Value();
    }

    std::string Protocol::Name() const
    {
        return d->m_Name.Value();
    }

    void Protocol::SetName(const std::string value)
    {
        d->m_Name.SetValue(value);
    }

    std::string Protocol::version() const
    {
        return d->m_Version.Value();
    }

    void Protocol::setVersion(const std::string value)
    {
        d->m_Version.SetValue(value);
    }

    std::string Protocol::revision() const
    {
        return d->m_Revision.Value();
    }

    void Protocol::setRevision(const std::string value)
    {
        d->m_Revision.SetValue(value);
    }

    Node* Protocol::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "ColumnSets") {
            d->m_ColumnSets.init(node);
            return &d->m_ColumnSets;
        }
        return nullptr;
    }

    void Protocol::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Name") {
            d->m_Name.config(attr);
        } else if (name == "Version") {
            d->m_Version.config(attr);
        } else if (name == "Revision") {
            d->m_Revision.config(attr);
        } else if (name == "Id") {
            d->m_Id.config(attr);
        }
    }

    void Protocol::afterInit()
    {
        d->read();
        d->setColumnsFieldIds();
    }

    ColumnSets* Protocol::columnSets() const
    {
        return &d->m_ColumnSets;
    }

    Base::Protocol* Protocol::baseProtocol() const
    {
        return &d->m_BaseProcotol;
    }

    bool Protocol::operator<(const Protocol& rhs)
    {
        return Id() < rhs.Id();
    }

}
}
