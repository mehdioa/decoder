// clang-format off
#include "pch.hpp"
#include "Settings.h"
#include "Logger.h"
#include "Layout.h"
#include "Recents.h"
#include "Protocols.h"
#include "Parameter.h"
#include "../Business/Service.h"
// clang-format on

using namespace Core::Setting;

struct Settings::Impl {

    Settings* m_Parent;
    Core::Business::Service* m_Service;
    pugi::xml_document m_Document;
    Logger m_Logger;
    Layout m_Layout;
    Recents m_Recents;
    std::string m_FileName;
    Protocols m_Procotols;
    Parameter<int> m_ThreadCount;
    Parameter<int> m_Version;

    Impl(Settings* p)
        : m_Parent(p)
    {
    }

    Impl(const Impl&) {}

    ~Impl()
    {
        write(m_FileName.c_str());
    }

    void read(const char* file_name)
    {
        m_FileName = file_name;
        auto result = m_Document.load_file(file_name);
        if (result.status != pugi::status_ok) {
            spdlog::critical("Error reading setting {}: status: {}", file_name, result.status);
            exit(result.status);
        }
        auto child = m_Document.child("Settings");
        m_Parent->init(&child);
    }

    void write(const char* file_name)
    {
        m_Document.save_file(file_name);
    }
};

DEF_CMA(Settings);

void Settings::setService(Core::Business::Service* service)
{
    d->m_Service = service;
}

void Settings::read(const char* file_name)
{
    d->read(file_name);
}

void Settings::write(const char* file_name)
{
    d->write(file_name);
}

void Settings::restoreDefault()
{
    d->m_Logger.restoreDefault();
    d->m_Layout.restoreDefault();
}

Core::StringVec Settings::Recent(Core::FileType file_type) const
{
    return d->m_Recents.Recent(file_type);
}

void Settings::AddRecent(Core::FileType file_type, const std::string value)
{
    return d->m_Recents.Add(file_type, value);
}

std::string Settings::RecentAt(Core::FileType file_type, size_t index) const
{
    return d->m_Recents.At(file_type, index);
}

Protocols* Settings::protocols() const
{
    return &d->m_Procotols;
}

int Settings::threadCount() const
{
    return d->m_ThreadCount.Value();
}

std::string Settings::GetGeometry() const
{
    return d->m_Layout.GetGeometry();
}

void Settings::SetGeometry(std::string geometry)
{
    return d->m_Layout.SetGeometry(geometry);
}

std::string Settings::GetState() const
{
    return d->m_Layout.GetState();
}

void Settings::SetState(std::string state)
{
    return d->m_Layout.SetState(state);
}

Node* Settings::initChild(pugi::xml_node* node)
{
    std::string name(node->name());
    if (name == "Logger") {
        d->m_Logger.init(node);
        d->m_Logger.config();
        return &(d->m_Logger);
    } else if (name == "Layout") {
        d->m_Layout.init(node);
        return &(d->m_Layout);
    } else if (name == "Protocols") {
        d->m_Procotols.init(node);
        return &(d->m_Procotols);
    } else if (name == "Recents") {
        d->m_Recents.init(node);
        return &(d->m_Recents);
    }
    return nullptr;
}

void Settings::initAttr(pugi::xml_attribute* attr)
{
    std::string name(attr->name());
    if (name == "version") {
        d->m_Version.config(attr);
    } else if (name == "threads") {
        d->m_ThreadCount.config(attr);
    }
}
