// clang-format off
#include "pch.hpp"
#include "Item.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Item::Impl {
        Item* m_Parent;
        Parameter<std::string> m_Value;
        Parameter<int> m_Id;

        Impl(Item* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
            m_Value.SetValue("");
        }
    };

    DEF_CMA(Item);

    void Item::restoreDefault()
    {
        d->restoreDefault();
    }

    bool Item::operator<(const Item& rhs)
    {
        return Id() < rhs.Id();
    }

    int Item::Id() const
    {
        return d->m_Id.Value();
    }

    void Item::SetId(int id)
    {
        d->m_Id.SetValue(id);
    }

    std::string Item::Value() const
    {
        return d->m_Value.Value();
    }

    void Item::SetValue(const std::string value)
    {
        d->m_Value.SetValue(value);
    }

    Node* Item::initChild(pugi::xml_node* node)
    {
        UNUSED(node);
        return nullptr;
    }

    void Item::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Value") {
            d->m_Value.config(attr);
        } else if (name == "Id") {
            d->m_Id.config(attr);
        }
    }

}
}
