// clang-format off
#include "pch.hpp"
#include "Protocols.h"
#include "Protocol.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Protocols::Impl {
        Protocols* m_Parent;
        std::map<uint32_t, Protocol*> m_Protocols;

        Impl(Protocols* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
        }
    };

    DEF_CMA(Protocols);

    void Protocols::restoreDefault()
    {
        d->restoreDefault();
    }

    //    ColumnSet* ColumnSets::At(size_t index) const
    //    {
    //        if (index < d->m_ColumnSets.size()) {
    //            auto it = d->m_ColumnSets.begin();
    //            std::advance(it, index);
    //            return (*it)->Value();
    //        }
    //        Error("Index out of range");
    //        return "";
    //    }

    size_t Protocols::Size() const
    {
        return d->m_Protocols.size();
    }

    Protocol* Protocols::At(uint32_t id) const
    {
        auto it = d->m_Protocols.find(id);
        if (it != d->m_Protocols.end())
            return it->second;
        Error("No Protocol of id {}", id);
        return nullptr;
    }

    Node* Protocols::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "Protocol") {
            auto pr = new Protocol();
            pr->init(node);
            d->m_Protocols.insert({ pr->Id(), pr });
        }
        return nullptr;
    }

    void Protocols::initAttr(pugi::xml_attribute* attr)
    {
        UNUSED(attr);
        //        std::string name(attr->name());
        //        if (name == "Default") {
        //            d->m_Default.config(attr);
        //        }
    }

    void Protocols::afterInit()
    {
        //        if (d->m_ColumnSets.find(d->m_Default.Value()) == d->m_ColumnSets.end()) {
        //            Warn("ColumnSet has no default. Setting the first one as default");
        //            d->m_Default.SetValue(d->m_ColumnSets.begin()->second->Name());
        //        }
    }

}
}
