// clang-format off
#include "pch.hpp"
#include "ColumnSets.h"
#include "ColumnSet.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct ColumnSets::Impl {
        ColumnSets* m_Parent;
        Parameter<uint32_t> m_Default;
        std::map<uint32_t, ColumnSet*> m_ColumnSets;

        Impl(ColumnSets* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
        }
    };

    DEF_CMA(ColumnSets);

    void ColumnSets::restoreDefault()
    {
        d->restoreDefault();
    }

    ColumnSet* ColumnSets::At(size_t index) const
    {
        if (index < d->m_ColumnSets.size()) {
            return d->m_ColumnSets.at(index);
        }
        Error("Index out of range");
        return nullptr;
    }

    size_t ColumnSets::Size() const
    {
        return d->m_ColumnSets.size();
    }

    ColumnSet* ColumnSets::Default() const
    {
        return d->m_ColumnSets.at(d->m_Default.Value());
    }

    void ColumnSets::traverse(ColumnSetVisitor visitor)
    {
        for (auto cs : d->m_ColumnSets)
            if (visitor(cs.second))
                return;
    }

    Node* ColumnSets::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "ColumnSet") {
            auto cs = new ColumnSet();
            cs->init(node);
            d->m_ColumnSets.insert({ cs->id(), cs });
        }
        return nullptr;
    }

    void ColumnSets::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Default") {
            d->m_Default.config(attr);
        }
    }

    void ColumnSets::afterInit()
    {
        if (d->m_ColumnSets.find(d->m_Default.Value()) == d->m_ColumnSets.end()) {
            Warn("ColumnSet has no default. Setting the first one as default");
            d->m_Default.SetValue(d->m_ColumnSets.begin()->second->id());
        }
    }

}
}
