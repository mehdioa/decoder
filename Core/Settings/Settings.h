#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Settings : public Node {
        DEC_CMA(Settings);

        void setService(Business::Service* service);

        void read(const char* file_name);
        void write(const char* file_name);

        void restoreDefault() override;

        StringVec Recent(FileType file_type) const;
        void AddRecent(FileType file_type, const std::string value);
        std::string RecentAt(FileType file_type, size_t index) const;

        Protocols* protocols() const;
        int threadCount() const;

        std::string GetGeometry() const;
        void SetGeometry(std::string geometry);

        std::string GetState() const;
        void SetState(std::string state);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
    };

}
}
