#pragma once

#include "Node.h"

namespace Core {
namespace Setting {
    class DECODERCORE_API Protocols : public Node {
        DEC_CMA(Protocols);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
        void afterInit() override;

    public:
        void restoreDefault() override;

        size_t Size() const;
        Protocol* At(uint32_t id) const;
    };
}
}
