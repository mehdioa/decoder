#pragma once

namespace Core {
namespace Setting {
    class Node;
    typedef std::vector<Node*> Nodes;

    class DECODERCORE_API Node {
        DEC_CMA(Node);
        //public:
        //    Node();
        //Node(const Node& node);
        //Node(Node&& t);
        //Node& operator=(const Node& rhs);
        //Node& operator=(Node&& rhs);
        //virtual ~Node();

        virtual void restoreDefault() = 0;

        void init(pugi::xml_node* node);

        virtual std::string name() const;

        Nodes children() const;

        pugi::xml_node AppendChild(const std::string& name);

    private:
        virtual Node* initChild(pugi::xml_node* node) = 0;
        virtual void initAttr(pugi::xml_attribute* attr) = 0;
        virtual void afterInit();
    };
}
}
