#pragma once

#include "Node.h"

namespace Core {
namespace Setting {
    class DECODERCORE_API ColumnSets : public Node {
        DEC_CMA(ColumnSets);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
        void afterInit() override;

    public:
        void restoreDefault() override;

        ColumnSet* At(size_t index) const;
        size_t Size() const;
        ColumnSet* Default() const;

        void traverse(ColumnSetVisitor visitor);
    };
}
}
