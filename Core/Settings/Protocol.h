#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Protocol : public Node {
        DEC_CMA(Protocol);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
        void afterInit() override;

    public:
        void restoreDefault() override;
        uint32_t Id() const;

        std::string Name() const;
        void SetName(const std::string value);
        std::string version() const;
        void setVersion(const std::string value);
        std::string revision() const;
        void setRevision(const std::string value);

        ColumnSets* columnSets() const;
        Core::Base::Protocol* baseProtocol() const;

        bool operator<(const Protocol& rhs);
    };
}
}
