#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class Item;
    class DECODERCORE_API Recents : public Node {
        DEC_CMA(Recents);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;

    public:
        void restoreDefault() override;

        StringVec Recent(FileType file_type) const;
        void Add(FileType file_type, const std::string value);
        std::string At(FileType file_type, size_t index) const;
    };
}
}
