#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Column : public Node {
        DEC_CMA(Column);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;

    public:
        void restoreDefault() override;

        bool operator<(const Column& rhs);

        int Id() const;
        void SetId(int id);
        std::string Value() const;
        void SetValue(const std::string value);

        int foreground() const;
        void setForeground(int argb);

        int background() const;
        void setBackground(int argb);

        void setFieldInfoIndex(size_t index);
        size_t fieldInfoIndex() const;
    };
}
}
