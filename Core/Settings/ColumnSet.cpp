// clang-format off
#include "pch.hpp"
#include "ColumnSet.h"
#include "Column.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct ColumnSet::Impl {
        ColumnSet* m_Parent;

        static const size_t MaxRecents = 10;
        Parameter<std::string> m_Name;
        Parameter<uint32_t> m_Id;
        std::list<Column*> m_Columns;

        Impl(ColumnSet* parent)
            : m_Parent(parent)
        {
        }

        void restoreDefault()
        {
        }

        void Sort()
        {
            size_t id = 0;
            for (auto it = m_Columns.begin(); it != m_Columns.end(); ++it)
                (*it)->SetId(id++);
        }

        void Add(const std::string& value)
        {
            Column* column = nullptr;
            auto it = m_Columns.begin();
            for (; it != m_Columns.end() && (*it)->Value() != value; ++it)
                ;
            //            auto it = std::find_if(m_Cells.begin(), m_Cells.end(), [&value](Cell* Cell) { return Cell->Value() == value; });
            if (it != m_Columns.end()) {
                column = *it;
                m_Columns.erase(it);
                m_Columns.push_front(column);
            } else if (m_Columns.size() == MaxRecents) {
                column = m_Columns.back();
                m_Columns.pop_back();
                column->SetValue(value);
                m_Columns.push_front(column);
            } else {
                auto child = m_Parent->AppendChild("Column");
                child.append_attribute("Id") = 0;
                child.append_attribute("Value") = value.c_str();
                column = new Column;
                column->init(&child);
                m_Columns.push_front(column);
            }
            Sort();
        }
    };

    DEF_CMA(ColumnSet);

    void ColumnSet::restoreDefault()
    {
        d->restoreDefault();
    }

    std::string ColumnSet::At(size_t index) const
    {
        if (index < d->m_Columns.size()) {
            auto it = d->m_Columns.begin();
            std::advance(it, index);
            return (*it)->Value();
        }
        Error("Index out of range");
        return "";
    }

    size_t ColumnSet::Size() const
    {
        return d->m_Columns.size();
    }

    void ColumnSet::Add(const std::string& value)
    {
        d->Add(value);
    }

    StringList ColumnSet::All() const
    {
        StringList vec;
        for (auto it = d->m_Columns.begin(); it != d->m_Columns.end(); ++it)
            vec.push_back((*it)->Value());
        return vec;
    }

    std::string ColumnSet::Name() const
    {
        return d->m_Name.Value();
    }

    uint32_t ColumnSet::id() const
    {
        return d->m_Id.Value();
    }

    void ColumnSet::traverse(ColumnVisitor visitor)
    {
        for (auto column : d->m_Columns)
            if (visitor(column))
                return;
    }

    Node* ColumnSet::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "Column") {
            auto column = new Column();
            column->init(node);
            d->m_Columns.push_front(column);
        }
        return nullptr;
    }

    void ColumnSet::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == "Name") {
            d->m_Name.config(attr);
        } else if (name == "Id") {
            d->m_Id.config(attr);
        }
    }

    void ColumnSet::afterInit()
    {
        d->m_Columns.sort([](const Column* c1, const Column* c2) {
            return c1->Id() < c2->Id();
        });
    }

}
}
