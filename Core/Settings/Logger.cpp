// clang-format off
#include "pch.hpp"
#include "Logger.h"
#include "Parameter.h"
// clang-format on

#define RESET "\x1b[0m"
#define CLEAR "\x1b[2J"
#define BLACK "\x1b[30m"
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define WHITE "\x1b[37m"
#define BOLDBLACK "\x1b[1m\x1b[30m"
#define BOLDRED "\x1b[1m\x1b[31m"
#define BOLDGREEN "\x1b[1m\x1b[32m"
#define BOLDYELLOW "\x1b[1m\x1b[33m"
#define BOLDBLUE "\x1b[1m\x1b[34m"
#define BOLDMAGENTA "\x1b[1m\x1b[35m"
#define BOLDCYAN "\x1b[1m\x1b[36m"
#define BOLDWHITE "\x1b[1m\x1b[37m"
#define BLINKRED "\x1b[31m\x1b[5m"

#define ATTR_LEVEL_NAME "Level"
#define ATTR_OUTPUT_NAME "Output"
#define ATTR_COLORED_NAME "Colored"
#define ATTR_INVERTED_NAME "Inverted"
#define ATTR_PATTERN_NAME "Pattern"
#define ATTR_MAXFILESIZE_NAME "MaxFileSize"
#define ATTR_MAXFILES_NAME "MaxFiles"
#define ATTR_FORCEFLUSH_NAME "ForceFlush"
#define ATTR_RELATIVE_PATH_NAME "logs/log"

namespace Core {
LogPtr Trace_ = spdlog::get(LevelName(Log::Level::Trace));
LogPtr Debug_ = spdlog::get(LevelName(Log::Level::Debug));
LogPtr Info_ = spdlog::get(LevelName(Log::Level::Info));
LogPtr Warn_ = spdlog::get(LevelName(Log::Level::Warn));
LogPtr Error_ = spdlog::get(LevelName(Log::Level::Error));
LogPtr Critical_ = spdlog::get(LevelName(Log::Level::Critical));
namespace Setting {

    struct Logger::Impl {
        Parameter<Log::Level> m_Level;
        Parameter<Log::Output> m_Output;
        Parameter<bool> m_Colored;
        Parameter<bool> m_Inverted;
        Parameter<std::string> m_Pattern;
        Parameter<uint> m_MaxFileSize;
        Parameter<uint> m_MaxFiles;
        Parameter<bool> m_ForceFlush;
        Parameter<std::string> m_Path;

        Logger* m_Parent;

        Impl(Logger* parent)
            : m_Parent(parent)
        {
        }

        void config()
        {
            using namespace spdlog;

            level::level_enum level = level::from_str(m_Level.StringValue());

            std::string format;

            if (m_Colored.Value()) {
                auto inv = (m_Inverted.Value() ? "\x1b[7m" : "");
                format = m_Pattern.Value() + std::string(inv) + std::string("%v\x1b[27m");
            } else {
                format = m_Pattern.Value() + std::string("%v");
            }

            switch (m_Output.Value()) {

            default:
                Trace_ = stdout_color_mt(LevelName(Log::Level::Trace));
                Debug_ = stdout_color_mt(LevelName(Log::Level::Debug));
                Info_ = stdout_color_mt(LevelName(Log::Level::Info));
                Warn_ = stdout_color_mt(LevelName(Log::Level::Warn));
                Error_ = stdout_color_mt(LevelName(Log::Level::Error));
                Critical_ = stdout_color_mt(LevelName(Log::Level::Critical));
                break;
            case (Log::Output::File):
                Trace_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Trace), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                Debug_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Debug), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                Info_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Info), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                Warn_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Warn), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                Error_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Error), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                Critical_ = spdlog::rotating_logger_mt(LevelName(Log::Level::Critical), m_Path.Value(), m_MaxFileSize.Value(), m_MaxFiles.Value());
                break;
            }

            if (m_Colored.Value()) {
                Trace_->set_pattern(std::string(GREEN) + format);
                Debug_->set_pattern(std::string(RESET) + format);
                Info_->set_pattern(std::string(BLUE) + format);
                Warn_->set_pattern(std::string(YELLOW) + format);
                Error_->set_pattern(std::string(RED) + format);
                Critical_->set_pattern(std::string(BLINKRED) + format);
            } else {
                Trace_->set_pattern(format);
                Debug_->set_pattern(format);
                Info_->set_pattern(format);
                Warn_->set_pattern(format);
                Error_->set_pattern(format);
                Critical_->set_pattern(format);
            }

            Trace_->set_level(level);
            Debug_->set_level(level);
            Info_->set_level(level);
            Warn_->set_level(level);
            Error_->set_level(level);
            Critical_->set_level(level);
        }

        void restoreDefault()
        {
            m_Level.SetValue(Log::Level::Warn);
            m_Output.SetValue(Log::Output::File);
            m_Colored.SetValue(true);
            m_Inverted.SetValue(false);
            m_Pattern.SetValue("%L ");
            m_MaxFiles.SetValue(3);
            m_MaxFileSize.SetValue(5242880);
            m_ForceFlush.SetValue(false);
        }
    };

    DEF_CMA(Logger);

    void Logger::restoreDefault()
    {
        d->restoreDefault();
    }

    void Logger::config()
    {
        d->config();
    }

    Log::Level Logger::GetLevel() const
    {
        return d->m_Level.Value();
    }

    void Logger::SetLevel(Log::Level level)
    {
        d->m_Level.SetValue(level);
    }

    Log::Output Logger::GetOutput() const
    {
        return d->m_Output.Value();
    }

    void Logger::SetOutput(Log::Output output)
    {
        d->m_Output.SetValue(output);
    }

    bool Logger::GetColored() const
    {
        return d->m_Colored.Value();
    }

    void Logger::SetColored(bool colored)
    {
        d->m_Colored.SetValue(colored);
    }

    bool Logger::GetInverted() const
    {
        return d->m_Inverted.Value();
    }

    void Logger::SetInverted(bool inverted)
    {
        d->m_Inverted.SetValue(inverted);
    }

    bool Logger::GetForceFlush() const
    {
        return d->m_ForceFlush.Value();
    }

    void Logger::SetForceFlush(bool force)
    {
        d->m_ForceFlush.SetValue(force);
    }

    std::string Logger::GetPattern() const
    {
        return d->m_Pattern.Value();
    }

    void Logger::SetPattern(std::string pattern)
    {
        d->m_Pattern.SetValue(pattern);
    }

    uint Logger::GetMaxFileSize() const
    {
        return d->m_MaxFileSize.Value();
    }

    void Logger::SetMaxFileSize(uint max_file_size)
    {
        d->m_MaxFileSize.SetValue(max_file_size);
    }

    uint Logger::GetMaxFiles() const
    {
        return d->m_MaxFiles.Value();
    }

    void Logger::SetMaxFiles(uint max_files)
    {
        d->m_MaxFiles.SetValue(max_files);
    }

    Node* Logger::initChild(pugi::xml_node* node)
    {
        UNUSED(node);
        return nullptr;
    }

    void Logger::initAttr(pugi::xml_attribute* attr)
    {
        std::string name(attr->name());
        if (name == ATTR_LEVEL_NAME) {
            d->m_Level.config(attr);
        } else if (name == ATTR_OUTPUT_NAME) {
            d->m_Output.config(attr);
        } else if (name == ATTR_COLORED_NAME) {
            d->m_Colored.config(attr);
        } else if (name == ATTR_PATTERN_NAME) {
            d->m_Pattern.config(attr);
        } else if (name == ATTR_INVERTED_NAME) {
            d->m_Inverted.config(attr);
        } else if (name == ATTR_MAXFILES_NAME) {
            d->m_MaxFiles.config(attr);
        } else if (name == ATTR_FORCEFLUSH_NAME) {
            d->m_ForceFlush.config(attr);
        } else if (name == ATTR_MAXFILESIZE_NAME) {
            d->m_MaxFileSize.config(attr);
        } else if (name == ATTR_RELATIVE_PATH_NAME) {
            d->m_Path.config(attr);
        }
    }

}
}
