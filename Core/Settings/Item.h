#pragma once

#include "Node.h"

namespace Core {
namespace Setting {

    class DECODERCORE_API Item : public Node {
        DEC_CMA(Item);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;

    public:
        void restoreDefault() override;

        bool operator<(const Item& rhs);

        int Id() const;
        void SetId(int id);
        std::string Value() const;
        void SetValue(const std::string value);
    };
}
}
