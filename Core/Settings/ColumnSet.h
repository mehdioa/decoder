#pragma once

#include "Node.h"

namespace Core {
namespace Setting {
    class DECODERCORE_API ColumnSet : public Node {
        DEC_CMA(ColumnSet);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
        void afterInit() override;

    public:
        void restoreDefault() override;

        std::string At(size_t index) const;
        size_t Size() const;
        void Add(const std::string& value);
        StringList All() const;

        std::string Name() const;
        uint32_t id() const;

        void traverse(ColumnVisitor visitor);
    };
}
}
