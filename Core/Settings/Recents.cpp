// clang-format off
#include "pch.hpp"
#include "Recents.h"
#include "Items.h"
#include "Item.h"
#include "Parameter.h"
// clang-format on

namespace Core {
namespace Setting {
    struct Recents::Impl {
        static const size_t MaxRecents = 5;
        Recents* m_Parent;
        std::vector<Items*> m_Items;

        Impl(Recents* parent)
            : m_Parent(parent)
        {
            for (auto i = static_cast<size_t>(FileType::Begin); i < static_cast<size_t>(FileType::Last); ++i) {
                m_Items.push_back(nullptr);
            }
        }

        void restoreDefault()
        {
        }

        void Add(FileType file_type, const std::string& value)
        {
            m_Items.at(static_cast<size_t>(file_type))->Add(value);
        }
    };

    DEF_CMA(Recents);

    void Recents::restoreDefault()
    {
        d->restoreDefault();
    }

    StringVec Recents::Recent(FileType file_type) const
    {
        if (static_cast<int>(file_type) < static_cast<int>(FileType::Last))
            return d->m_Items.at(static_cast<size_t>(file_type))->All();
        return {};
    }

    void Recents::Add(FileType file_type, const std::string value)
    {
        d->Add(file_type, value);
    }

    std::string Recents::At(FileType file_type, size_t index) const
    {
        return d->m_Items.at(static_cast<size_t>(file_type))->At(index);
    }

    Node* Recents::initChild(pugi::xml_node* node)
    {
        std::string name(node->name());
        if (name == "Items") {
            auto items = new Items();
            items->init(node);
            auto file_type = StringToFileType(items->Name());
            d->m_Items[static_cast<size_t>(file_type)] = items;
        }
        return nullptr;
    }

    void Recents::initAttr(pugi::xml_attribute* attr)
    {
        //        std::string name(attr->name());
        //        if (name == "Geometry") {
        //            d->m_Geometry.config(attr);
        //        } else if (name == "State") {
        //            d->m_State.config(attr);
        //        }
    }

}
}
