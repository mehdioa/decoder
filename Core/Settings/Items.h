#pragma once

#include "Node.h"

namespace Core {
namespace Setting {
    class DECODERCORE_API Items : public Node {
        DEC_CMA(Items);

    private:
        Node* initChild(pugi::xml_node* node) override;
        void initAttr(pugi::xml_attribute* attr) override;
        void afterInit() override;

    public:
        void restoreDefault() override;

        std::string At(size_t index) const;
        size_t Size() const;
        void Add(const std::string& value);
        StringVec All() const;

        std::string Name() const;
    };
}
}
