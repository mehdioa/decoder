import qbs
import qbs.Environment

Project {
    name: "Decoder"
    property int major: 0
    property int minor: 1
    property int patch: 0
    property string version: major+"."+minor+"."+patch
    property string cxxLanguageVersion: "c++17"
    property string warningLevel: "all"
//    property string arch: "x86_64"

    property bool IS_UNIX: qbs.targetOS.contains("unix")
    property bool IS_WIN: qbs.targetOS.contains("windows")
    property bool IS_OSX: qbs.targetOS.contains("osx")
    property bool IS_LINUX: IS_UNIX && !IS_OSX
    property string USER_LOCAL_PATH: IS_WIN ? "C:/local/": "/usr/local/"
    property bool BUILD_TEST: true
    property bool BUILD_EXAMPLES: true
    property string INC_DIR:  USER_LOCAL_PATH+"/include/"
    property bool BUILD_STATIC: true
    property string BOOST_DIR: "boost_1_71_0"
    property string LIB_TYPE:
    {
        var res = "dynamiclibrary";
        if (BUILD_STATIC) {
            res = "staticlibrary";
        }
        return res;
    }

    property pathList LIB_DIRS:  {
        var ret= [USER_LOCAL_PATH+"/lib"]
        var loc = "/install-root/"
        if (IS_WIN)
        {
            ret.push(USER_LOCAL_PATH+BOOST_DIR+"/lib64-msvc-14.2")
        }
        else
        {
            loc = loc+"usr/local/"
        }
        ret.push(buildDirectory + loc + "lib")
        return ret
    }


    property pathList INC_DIRS: {
        var ret = [INC_DIR]

        if (IS_WIN){
            ret.push(INC_DIR+"pugixml")
            ret.push(USER_LOCAL_PATH+BOOST_DIR)
        }
        console.error(ret)
        return ret
    }

    references: {
        ref = [
                    "Core/app.qbs",
//                    "Net/app.qbs",
                    "Gui/app.qbs",
                    "Decoder/app.qbs",
                    "Utils/TraceCreator/app.qbs",
//                    "playground/app.qbs"
                ];
//        if (BUILD_TEST) {
//            ref.push("test/app.qbs")
//        }
//        if (BUILD_EXAMPLES) {
//            ref.push("example/app.qbs")
//        }
        return ref
    }
}
