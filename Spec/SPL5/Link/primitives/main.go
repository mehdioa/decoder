package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func make_primitives() {
	file, err := os.Open("primitives.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	output, err := os.Create("primitives.xml")
	if err != nil {
		log.Fatal(err)
	}
	defer output.Close()

	output.WriteString("<?Xml Version=\"1.0\" Encoding=\"Utf-8\"?>\n")

	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	var value [3]string
	index := 0
	inside_node := false
	for scanner.Scan() {
		text := scanner.Text()
		if len(text) == 0 {
			continue
		}
		if len(text) >= 6 && text[:6] == "Table " {
			if inside_node {
				output.WriteString("</primitives>\n")
			}
			output.WriteString(fmt.Sprintf("<primitives  name=\"%s\">\n", text[14:]))
			inside_node = true
			continue
		}
		value[index] = text
		index = index + 1
		if index == 3 {
			index = 0
			output.WriteString(fmt.Sprintf("  <primitive name=\"%s\" func=\"%s\" value=\"%s\">\n", value[0], value[1], value[2]))
		}
	}

	output.WriteString("</primitives>\n")

}

func make_binary_primitives() {
	file, err := os.Open("binary_primitives.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	output, err := os.Create("binary_primitives.xml")
	if err != nil {
		log.Fatal(err)
	}
	defer output.Close()

	output.WriteString("<?Xml Version=\"1.0\" Encoding=\"Utf-8\"?>\n")

	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	var value [3]string
	index := 0
	inside_node := false
	for scanner.Scan() {
		text := scanner.Text()
		if len(text) == 0 {
			continue
		}
		if len(text) >= 6 && text[:6] == "Table " {
			if inside_node {
				output.WriteString("</primitives>\n")
			}
			output.WriteString(fmt.Sprintf("<primitives  name=\"%s\">\n", text[14:]))
			inside_node = true
			continue
		}
		value[index] = text
		index = index + 1
		if index == 2 {
			index = 0
			output.WriteString(fmt.Sprintf("  <primitive name=\"%s\" value=\"%s\">\n", value[0], value[1]))
		}
	}

	output.WriteString("</primitives>\n")
}

func main() {
	make_primitives()
	make_binary_primitives()
}
