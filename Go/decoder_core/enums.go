package decoder_core

type EAPIErrorCode int

const (
	API_OK                                          EAPIErrorCode = 0
	API_EXCEPTION                                   EAPIErrorCode = 1
	API_ERROR_INVALID_OBJECT                        EAPIErrorCode = 2
	API_ERROR_INSUFFICIENT_MEMORY                   EAPIErrorCode = 3
	API_ERROR_INVALID_CHAIN_INDEX                   EAPIErrorCode = 4
	API_ERROR_INVALID_DEVICE_INDEX                  EAPIErrorCode = 5
	API_ERROR_INVALID_PAIR_PORT_INDEX               EAPIErrorCode = 6
	API_ERROR_INVALID_TRIGGER_NAME                  EAPIErrorCode = 7
	API_ERROR_INVALID_TRIGGER_POSITION              EAPIErrorCode = 8
	API_ERROR_INVALID_SEGMENT_SIZE                  EAPIErrorCode = 9
	API_ERROR_INVALID_NUMBER_OF_SEGMENT             EAPIErrorCode = 10
	API_ERROR_INVALID_TRIG_MODE                     EAPIErrorCode = 11
	API_ERROR_CAN_NOT_STOP_JAMMER                   EAPIErrorCode = 12
	API_ERROR_CAN_NOT_START_JAMMER                  EAPIErrorCode = 13
	API_ERROR_INVALID_TRACE                         EAPIErrorCode = 14
	API_ERROR_INVALID_INDEX_OF_PACKET               EAPIErrorCode = 15
	API_ERROR_RECORDING                             EAPIErrorCode = 16
	API_ERROR_UPLOADING                             EAPIErrorCode = 17
	API_ERROR_JAMMER                                EAPIErrorCode = 18
	API_ERROR_CAN_NOT_OPEN_TRACE                    EAPIErrorCode = 19
	API_ERROR_CAN_NOT_UPDATE_TRACE                  EAPIErrorCode = 20
	API_ERROR_CAN_NOT_SAVE_TRACE                    EAPIErrorCode = 21
	API_ERROR_CAN_NOT_CLOSE_TRACE                   EAPIErrorCode = 22
	API_ERROR_CAN_NOT_CLOSE_PROJECT                 EAPIErrorCode = 23
	API_ERROR_CAN_NOT_SAVE_PROJECT                  EAPIErrorCode = 24
	API_ERROR_CAN_NOT_OPEN_PROJECT                  EAPIErrorCode = 25
	API_ERROR_CAN_NOT_DISCONNECT_FROM_DEVICE        EAPIErrorCode = 26
	API_ERROR_CAN_NOT_CONNECT_TO_DEVICE             EAPIErrorCode = 27
	API_ERROR_INVALID_DEVICE_OBJECT                 EAPIErrorCode = 28
	API_ERROR_SELECTED_DEVICE_IS_NOT_UPDATED        EAPIErrorCode = 29
	API_ERROR_INVALID_SCENARIO_NAME                 EAPIErrorCode = 30
	API_ERROR_CAN_NOT_STOP_RECORDING                EAPIErrorCode = 31
	API_ERROR_CAN_NOT_START_RECORDING               EAPIErrorCode = 32
	API_ERROR_CAN_NOT_ASSIGN_TO_DEVICE              EAPIErrorCode = 33
	API_ERROR_TLNETAPI_IS_NOT_INITIALIZED           EAPIErrorCode = 34
	API_ERROR_TLAPI_IS_NOT_INITIALIZED              EAPIErrorCode = 34
	API_ERROR_INVALID_ARGUMENT                      EAPIErrorCode = 35
	API_ERROR_PROJECT_IS_NOT_OPEN                   EAPIErrorCode = 36
	API_ERROR_PROJECT_ALREADY_OPEN                  EAPIErrorCode = 37
	API_ERROR_CAN_NOT_STOP_RTS                      EAPIErrorCode = 38
	API_ERROR_CAN_NOT_START_RTS                     EAPIErrorCode = 39
	API_ERROR_CAN_NOT_OPEN_LOG_RTS                  EAPIErrorCode = 40
	API_ERROR_NO_STARTED_RTS_TO_STOP                EAPIErrorCode = 41
	API_ERROR_INVALID_SCRIPT                        EAPIErrorCode = 42
	API_ERROR_FAILED_TO_SET_DEVICE_ALIASNAME        EAPIErrorCode = 43
	API_ERROR_FAILED_TO_GET_DEVICE_IP               EAPIErrorCode = 44
	API_ERROR_FAILED_TO_SET_DEVICE_IP               EAPIErrorCode = 45
	API_ERROR_FAILED_TO_UPDATE_DEVICE               EAPIErrorCode = 46
	API_ERROR_FAILED_TO_UPDATE_LICENSE              EAPIErrorCode = 47
	API_UPDATED_SUCCESSFULLY_BUT_POWER_CYCLE_NEEDED EAPIErrorCode = 48
	API_ERROR_FAILED_TO_GET_REGISTER_VALUE          EAPIErrorCode = 49
	API_ERROR_FAILED_TO_SET_REGISTER_VALUE          EAPIErrorCode = 50
	API_ERROR_REPEATED_SUBNET_ADDRESS               EAPIErrorCode = 51
	API_ERROR_SUBNET_DOES_NOT_EXIST                 EAPIErrorCode = 52
	API_ERROR_FAILED_TO_ADD_SUBNET                  EAPIErrorCode = 53
	API_ERROR_ADMIN_MODE_LOGIN_INVALID_PASSWORD     EAPIErrorCode = 54
	API_ERROR_ADMIN_MODE_UNABLE_LOGIN               EAPIErrorCode = 55
	API_ERROR_ADMIN_MODE_DISCONNECT_FAILED          EAPIErrorCode = 56
	API_ERROR_ADMIN_MODE_RESET_DEVICE_FAILED        EAPIErrorCode = 57
	API_ERROR_FAIL_TO_ADD_DEVICE_IN_PROJECT         EAPIErrorCode = 58
	API_ERROR_FAIL_TO_ADD_JAMMER_OBJECT             EAPIErrorCode = 59
	API_ERROR_NO_JAMMER_FEATURE_AVAILABLE           EAPIErrorCode = 60
	API_ERROR_NO_ANALYZER_FEATURE_AVAILABLE         EAPIErrorCode = 61
	API_ERROR_FAIL_TO_ADD_TRIGGER_SETTING           EAPIErrorCode = 62
	API_ERROR_FAIL_TO_ADD_FILTER_SETTING            EAPIErrorCode = 63
	API_ERROR_ADMIN_ACCESS_NOT_SUPPORTED            EAPIErrorCode = 64
	API_ERROR_CAN_NOT_START_EXERCISER               EAPIErrorCode = 65
	API_ERROR_CAN_NOT_STOP_EXERCISER                EAPIErrorCode = 66
	API_ERROR_INVALID_SCRIPT_NAME                   EAPIErrorCode = 67
	API_ERROR_EXERCISER                             EAPIErrorCode = 68
	API_ERROR_INVALID_EXPORT_FILE_NAME              EAPIErrorCode = 69
	API_ERROR_INDEX_OUT_OF_RANGE                    EAPIErrorCode = 70
	API_ERROR_INVALID_SESSION                       EAPIErrorCode = 71
)

type EIPMode int

const (
	STATIC_IP_MODE EIPMode = 1
	DHCP_IP_MODE   EIPMode = 2
)

type ELinkConfiguration int

const (
	LINK_CONFIG__INVALID                    ELinkConfiguration = -1
	LINK_CONFIG__FIRST_ITEM                 ELinkConfiguration = 1000
	LINK_CONFIG__FIRST_ITEM_SAS_PRODUCT     ELinkConfiguration = 2000
	LINK_CONFIG__A_SAS__A_SAS__A_SAS__A_SAS ELinkConfiguration = 2002
	LINK_CONFIG__A_SAS__A_SAS__0__0         ELinkConfiguration = 2003
	LINK_CONFIG__0__0__A_SAS__A_SAS         ELinkConfiguration = 2004
	LINK_CONFIG__A_SAS__0__0__0             ELinkConfiguration = 2005
	LINK_CONFIG__0__A_SAS__0__0             ELinkConfiguration = 2006
	LINK_CONFIG__0__0__A_SAS__0             ELinkConfiguration = 2007
	LINK_CONFIG__0__0__0__A_SAS             ELinkConfiguration = 2008
	LINK_CONFIG__A_SAS__0__A_SAS__0         ELinkConfiguration = 2009
	LINK_CONFIG__0__A_SAS__0__A_SAS         ELinkConfiguration = 2010
	LINK_CONFIG__AJA_SAS__0__0__0           ELinkConfiguration = 2012
	LINK_CONFIG__0__AJA_SAS__0__0           ELinkConfiguration = 2013
	LINK_CONFIG__AJA_SAS__0__AJA_SAS__0     ELinkConfiguration = 2014
	LINK_CONFIG__0__AJA_SAS__0__AJA_SAS     ELinkConfiguration = 2015
	LINK_CONFIG__AJ_SAS__0__0__0            ELinkConfiguration = 2016
	LINK_CONFIG__0__AJ_SAS__0__0            ELinkConfiguration = 2017
	LINK_CONFIG__AJ_SAS__0__AJ_SAS__0       ELinkConfiguration = 2018
	LINK_CONFIG__0__AJ_SAS__0__AJ_SAS       ELinkConfiguration = 2019
	LINK_CONFIG__JA_SAS__0__0__0            ELinkConfiguration = 2020
	LINK_CONFIG__0__JA_SAS__0__0            ELinkConfiguration = 2021
	LINK_CONFIG__JA_SAS__0__JA_SAS__0       ELinkConfiguration = 2022
	LINK_CONFIG__0__JA_SAS__0__JA_SAS       ELinkConfiguration = 2023
	LINK_CONFIG__J_SAS__0__0__0             ELinkConfiguration = 2024
	LINK_CONFIG__0__J_SAS__0__0             ELinkConfiguration = 2025
	LINK_CONFIG__J_SAS__0__J_SAS__0         ELinkConfiguration = 2026
	LINK_CONFIG__0__J_SAS__0__J_SAS         ELinkConfiguration = 2027
	LINK_CONFIG__J_SAS__J_SAS__0__0         ELinkConfiguration = 2028
	LINK_CONFIG__J_SAS__J_SAS__J_SAS__J_SAS ELinkConfiguration = 2029
	LINK_CONFIG__E_SAS__0__E_SAS__0         ELinkConfiguration = 2030
	LINK_CONFIG__0__E_SAS__0__E_SAS         ELinkConfiguration = 2031
	LINK_CONFIG__E_SAS__E_SAS__E_SAS__E_SAS ELinkConfiguration = 2032
	LINK_CONFIG__E_SAS__0__0__0             ELinkConfiguration = 2033
	LINK_CONFIG__0__E_SAS__0__0             ELinkConfiguration = 2034
	LINK_CONFIG__0__0__E_SAS__0             ELinkConfiguration = 2035
	LINK_CONFIG__0__0__0__E_SAS             ELinkConfiguration = 2036
	LINK_CONFIG__AE_SAS__0__AE_SAS__0       ELinkConfiguration = 2037
	LINK_CONFIG__0__AE_SAS__0__AE_SAS       ELinkConfiguration = 2038
	LINK_CONFIG__AE_SAS__0__0__0            ELinkConfiguration = 2039
	LINK_CONFIG__0__AE_SAS__0__0            ELinkConfiguration = 2040
	LINK_CONFIG__0__0__AE_SAS__0            ELinkConfiguration = 2041
	LINK_CONFIG__0__0__0__AE_SAS            ELinkConfiguration = 2042
	LINK_CONFIG__A_UP__0__0__0              ELinkConfiguration = 2043
)

type EConnectionType int

const (
	CONNECTION_TYPE_NONE EConnectionType = 0
	CONNECTION_TYPE_USB  EConnectionType = 1
	CONNECTION_TYPE_TCP  EConnectionType = 2
)

type ERecordingStatus int

const (
	RECORDING_STATUS_NONE                ERecordingStatus = 0
	RECORDING_STATUS_IDLE                ERecordingStatus = 1
	RECORDING_STATUS_PROGRAMMING         ERecordingStatus = 2
	RECORDING_STATUS_UPLOADING           ERecordingStatus = 3
	RECORDING_STATUS_WAITING_TRIGGER     ERecordingStatus = 4
	RECORDING_STATUS_RECORDING_TRIGGERED ERecordingStatus = 5
	RECORDING_STATUS_CASCADING_PENDING   ERecordingStatus = 6
)

type EJammerSessionStatus int

const (
	JAMMER_SESSION_STATUS_NONE        EJammerSessionStatus = 0
	JAMMER_SESSION_STATUS_IDLE        EJammerSessionStatus = 1
	JAMMER_SESSION_STATUS_RUNNING     EJammerSessionStatus = 2
	JAMMER_SESSION_STATUS_PROGRAMMING EJammerSessionStatus = 3
)

type EExerciserSessionStatus int

const (
	EXERCISER_SESSION_STATUS_NONE        EExerciserSessionStatus = 0
	EXERCISER_SESSION_STATUS_IDLE        EExerciserSessionStatus = 1
	EXERCISER_SESSION_STATUS_RUNNING     EExerciserSessionStatus = 2
	EXERCISER_SESSION_STATUS_PROGRAMMING EExerciserSessionStatus = 3
	EXERCISER_SESSION_STATUS_PAUSE       EExerciserSessionStatus = 4
)

//type ELinkConfiguration int

//class ERTSSessionStatus(Enum):
//    RTS_SESSION_STATUS_NONE = 0
//    RTS_SESSION_STATUS_IDLE = 1
//    RTS_SESSION_STATUS_RUNNING = 2
//    RTS_SESSION_STATUS_PROGRAMMING = 3

//type ELinkConfiguration int

//class EEyeScanSessionStatus(Enum):
//    EYE_SCANNER_SESSION_STATUS_NONE = 0
//    EYE_SCANNER_SESSION_STATUS_IDLE = 1
//    EYE_SCANNER_SESSION_STATUS_RUNNING = 2
//    EYE_SCANNER_SESSION_STATUS_PROGRAMMING = 3

//type ELinkConfiguration int

//class ETriggerMode(Enum):
//    TRIGGER_MODE_NONE = -1
//    TRIGGER_MODE_SNAPSHOT = 0
//    TRIGGER_MODE_PATTERN = 1

//type ELinkConfiguration int

type EAnalyzerSpeed int

const (
	ANALYZER_SPEED_INVALID EAnalyzerSpeed = -1
	ANALYZER_SPEED_UNKNOWN EAnalyzerSpeed = 16
	ANALYZER_SPEED_1_5     EAnalyzerSpeed = 0
	ANALYZER_SPEED_3       EAnalyzerSpeed = 1
	ANALYZER_SPEED_6       EAnalyzerSpeed = 2
	ANALYZER_SPEED_12      EAnalyzerSpeed = 3
	ANALYZER_SPEED_24      EAnalyzerSpeed = 4
	ANALYZER_SPEED_AUTO_12 EAnalyzerSpeed = 5
	ANALYZER_SPEED_AUTO_24 EAnalyzerSpeed = 6
	ANALYZER_SPEED_AUTO_6  EAnalyzerSpeed = 7
)

//type ELinkConfiguration int

//class EICActionType(Enum):
//    IC_ACTION_NONE = 0
//    IC_ACTION_BEEP = 24
//    IC_ACTION_MONITOR_COUNT = 25
//    IC_ACTION_RECONNECT = 26
//    IC_ACTION_DISCONNECT = 27
//    IC_ACTION_STOP = 28
//    IC_ACTION_RESTART_ALL = 29
//    IC_ACTION_RESTART_CURRENT_SEQ = 30
//    IC_ACTION_BRANCH = 31
//    IC_ACTION_ANALYZER_TRIGGER = 32
//    IC_ACTION_TRIGGER_OUT = 33
//    IC_ACTION_CAPTURE_DATA_DWORD = 53
//    IC_ACTION_REPLACE_DWORD_GENERAL = 54
//    IC_ACTION_NEW_FIRST_SAS = 10000
//    IC_ACTION_SAS_SUBSTITUTE_DATA = 10001
//    IC_ACTION_SAS_INJECT_ERROR_TYPE_CRC = 10002
//    IC_ACTION_SAS_INJECT_ERROR_TYPE_RD = 10003
//    IC_ACTION_SAS_INJECT_ERROR_TYPE_10b_CODE = 10004
//    IC_ACTION_SAS_INJECT_ERROR_TYPE_FEC_CORRECTABLE = 10005
//    IC_ACTION_SAS_INJECT_ERROR_TYPE_FEC_UNCORRECTABLE = 10006
//    IC_ACTION_SAS_INSERT_DWORD = 10007
//    IC_ACTION_SAS_INSERT_PRIMITIVE = 10008
//    IC_ACTION_SAS_INSERT_FRAME = 10009
//    IC_ACTION_SAS_SUBSTITUTE_PRIMITIVE = 10010
//    IC_ACTION_SAS_DELETE_PRIMITIVE = 10011
//    IC_ACTION_SAS_TRUNCATE_FRAME = 10012
//    IC_ACTION_SAS_DELETE_FRAME = 10013
//    IC_ACTION_SAS_SNW_RETIME = 10014
//    IC_ACTION_SAS_SNW_VIOLATION = 10015
//    IC_ACTION_SAS_MODIFY_FRAME = 10016
//    IC_ACTION_SAS_SUBSTITUTE_SPL_PACKET = 10017
//    IC_ACTION_SAS_INSERT_SPL_PACKET = 10018
//    IC_ACTION_SAS_DELETE_SPL_PACKET = 10019
//    IC_ACTION_ST_OTHER_FPGA_TRIGGER = 10020
//    IC_ACTION_ST_ANALYZER_MARKER_TRIGGER = 10021
//    IC_ACTION_NEW_TYPE_LAST = 10022

//type ELinkConfiguration int

type EDataType int

const (
	UnknownType       EDataType = 0
	Primitive         EDataType = 1
	OpenAddrFrame     EDataType = 2
	IdentifyAddrFrame EDataType = 3
	SSPFrame          EDataType = 4
	SMPFrame          EDataType = 5
	STPFrame          EDataType = 6
	IdleType          EDataType = 7
	Training          EDataType = 8
	OOBSignal         EDataType = 9
	DevSlp            EDataType = 10
	DCMRequest        EDataType = 11
	EDataType_Count   EDataType = 12
)

//type ELinkConfiguration int

//class ECaptureStrategy(Enum):
//    CAPTURE_EVERYTHING = 0
//    CAPTURE_PATTERN_INCLUDE = 1
//    CAPTURE_PATTERN_EXCLUDE = 2

//type ELinkConfiguration int

//class EJammerSequencer(Enum):
//    JAMMER_GLOBAL_SEQUENCER = 0
//    JAMMER_SEQUENCER_0 = 1
//    JAMMER_SEQUENCER_1 = 2

//type ELinkConfiguration int

type EDeviceType int

const (
	T244 EDeviceType = 16898
	M244 EDeviceType = 16914
)

type VSEStatus int

const (
	Idle     VSEStatus = 0
	Enqueued VSEStatus = 1
	Running  VSEStatus = 2
	Success  VSEStatus = 3
	Failed   VSEStatus = 4
	Error    VSEStatus = 5
)

type PortStatus int

const (
	Enable        PortStatus = 0
	Disable       PortStatus = 1
	NotApplicable PortStatus = 2
)
