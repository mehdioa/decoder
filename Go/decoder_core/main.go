// main
package decoder_core

/*
#cgo CPPFLAGS: -IC:/Users/Omid/Documents/Decoder/Core -IC:/local/include/pugixml
#cgo LDFLAGS: C:/Users/Omid/Documents/build/decoder/MVSVC_142/x64/Debug/bin/decoder_core.dll
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#define DECODERCORE_API __declspec(dllimport)
#include <Utils.h>
*/
import "C"

//var enable_export bool
//var enable_vse_log bool
//var enable_vse_progress bool
//var enalbe_vse_status bool
//var enalbe_update_progress bool
//var vse_status int32

//func init() {
//	enable_export = true
//	enable_vse_log = true
//	enable_vse_progress = true
//	enalbe_vse_status = true
//	enalbe_update_progress = true
//	C.APIInitialize(true)
//}

//func Unload() {
//	C.APIUninitialize()
//}

////export on_update_trace
//func on_update_trace(progress int) int {
//	go fmt.Printf("on_update_trace_go %d\n", progress)
//	return 1
//}

////export on_export
//func on_export(progress int) int {
//	go fmt.Printf("on_export %d\n", progress)
//	return 1
//}

////export on_log_appended
//func on_log_appended(log *C.char) {
//	go fmt.Printf("on_log_appended %v\n", C.GoString(log))
//}

////export on_vse_progress
//func on_vse_progress(p int) {
//	go fmt.Printf("on_vse_progress %d\n", p)
//}

////export on_vse_status
//func on_vse_status(s int) {
//	atomic.StoreInt32(&vse_status, int32(s))
//}

//func make_error(err C.EAPIErrorCode) error {
//	if err != C.API_OK {
//		return errors.New(fmt.Sprintf("Error is %d", err))
//	}
//	return nil
//}

//func EnableVSELog(enable bool) {
//	enable_vse_log = enable
//}

//func EnableVSEProgress(enable bool) {
//	enable_vse_progress = enable
//}
//func EnableVSEStatus(enable bool) {
//	enalbe_vse_status = enable
//}
//func EnableUpdateProgress(enable bool) {
//	enalbe_update_progress = enable
//}

//func EnableExport(enable bool) {
//	enable_export = enable
//}

////////////////////////////Trace/////////////////////////////

//type Trace struct {
//	object *C.APITrace
//}

//func NewTrace() Trace {
//	var trace Trace
//	make_error(C.APITrace_create(&trace.object))
//	return trace
//}

//func (t Trace) Free() error {
//	return make_error(C.APITrace_destroy(&t.object))
//}

//func (t Trace) Open(file_name string) error {
//	cstr := C.CString(file_name)
//	defer C.free(unsafe.Pointer(cstr))
//	callback := (*[0]byte)(C.on_update_trace)
//	if !enalbe_update_progress {
//		callback = nil
//	}
//	return make_error(C.APITrace_Open(t.object, cstr, callback))
//}

//func (t Trace) Close() error {
//	return make_error(C.APITrace_Close(t.object))
//}

//func (t Trace) StartTime() (int64, error) {
//	var ans C.long
//	err := make_error(C.APITrace_GetStartDateTime(t.object, &ans))
//	return int64(ans), err
//}

//func (t Trace) EndTime() (int64, error) {
//	var ans C.long
//	err := make_error(C.APITrace_GetEndDateTime(t.object, &ans))
//	return int64(ans), err
//}

//func (t Trace) TriggerTime() (int64, error) {
//	var ans C.long
//	err := make_error(C.APITrace_GetTriggerDateTime(t.object, &ans))
//	return int64(ans), err
//}

//func (t Trace) ExportToCSV(file_name string) error {
//	cstr := C.CString(file_name)
//	defer C.free(unsafe.Pointer(cstr))
//	callback := (*[0]byte)(C.on_export)
//	if !enable_export {
//		callback = nil
//	}

//	return make_error(C.APITrace_ExportToCSV(t.object, cstr, callback))
//}

//func (t Trace) RunVScript(file_name string) (VSEStatus, error) {
//	cstr := C.CString(file_name)
//	defer C.free(unsafe.Pointer(cstr))
//	on_vse_status(0)

//	callback_status := (*[0]byte)(C.on_vse_status)
//	if !enalbe_vse_status {
//		callback_status = nil
//	}

//	callback_progress := (*[0]byte)(C.on_vse_progress)
//	if !enable_vse_progress {
//		callback_progress = nil
//	}

//	callback_log := (*[0]byte)(C.on_log_appended)
//	if !enable_vse_log {
//		callback_log = nil
//	}

//	err := make_error(C.APITrace_RunVScript(t.object, cstr, callback_status, callback_progress, callback_log))
//	return VSEStatus(atomic.LoadInt32(&vse_status)), err
//}

////////////////////////////Project/////////////////////////////

//type Project struct {
//	object *C.APIProject
//}

//func NewProject() Project {
//	var project Project
//	make_error(C.APIProject_create(&project.object))
//	return project
//}

//func (p Project) Free() error {
//	return make_error(C.APIProject_destroy(&p.object))
//}

//func (p Project) Open(file_name string) error {
//	cstr := C.CString(file_name)
//	defer C.free(unsafe.Pointer(cstr))
//	return make_error(C.APIProject_Open(p.object, cstr))
//}

//func (p Project) Close() error {
//	return make_error(C.APIProject_Close(p.object))
//}

//func (p Project) Save() error {
//	return make_error(C.APIProject_Save(p.object))
//}

//func (p Project) ChainCount() (int, error) {
//	var ans C.int
//	err := make_error(C.APIProject_GetChainCount(p.object, &ans))
//	return int(ans), err
//}

//func (p Project) DeviceCount(chain_index int) (int, error) {
//	var ans C.int
//	err := make_error(C.APIProject_GetDeviceCount(p.object, C.int(chain_index), &ans))
//	return int(ans), err
//}

//func (p Project) PairPortCount(chain_index int, device_index int) (int, error) {
//	var ans C.int
//	err := make_error(C.APIProject_GetPairPortCount(p.object, C.int(chain_index), C.int(device_index), &ans))
//	return int(ans), err
//}

//func (p Project) PortConfiguration(chain_index int, device_index int) (ELinkConfiguration, error) {
//	var ans C.ELinkConfiguration
//	err := make_error(C.APIProject_GetPortConfiguration(p.object, C.int(chain_index), C.int(device_index), &ans))
//	return ELinkConfiguration(ans), err
//}

////////////////////////////DEVICE/////////////////////////////
//type Device struct {
//	object *C.APIDevice
//}

//func NewDevice() Device {
//	var device Device
//	make_error(C.APIDevice_create(&device.object))
//	return device
//}

//func (p Device) Free() error {
//	return make_error(C.APIDevice_destroy(&p.object))
//}

//func (p Device) AliasName() (string, error) {
//	var name *C.char
//	//	cstr := C.CString(file_name)
//	//	defer C.free(unsafe.Pointer(cstr))
//	err := make_error(C.APIDevice_GetAliasName(p.object, &name))
//	return C.GoString(name), err
//}

////////////////////////////AnalyzerSettings/////////////////////////////
//type AnalyzerSettings struct {
//	object *C.APIAnalyzerSettings
//}

//func NewAnalyzerSettings() AnalyzerSettings {
//	var x AnalyzerSettings
//	make_error(C.APIAnalyzerSettings_create(&x.object))
//	return x
//}

//func (p AnalyzerSettings) Free() error {
//	return make_error(C.APIAnalyzerSettings_destroy(&p.object))
//}

////////////////////////////AdapterInfo/////////////////////////////
//type AdapterInfo struct {
//	object *C.APIAdapterInfo
//}

//func NewAdapterInfo() AdapterInfo {
//	var x AdapterInfo
//	make_error(C.APIAdapterInfo_create(&x.object))
//	return x
//}

//func (p AdapterInfo) Free() error {
//	return make_error(C.APIAdapterInfo_destroy(&p.object))
//}

//func (p AdapterInfo) Count() (int, error) {
//	var ans C.int
//	err := make_error(C.APIAdapterInfo_GetCount(p.object, &ans))
//	return int(ans), err
//}

//func (p AdapterInfo) Name(index int) (string, error) {
//	var ans *C.char
//	err := make_error(C.APIAdapterInfo_GetName(p.object, C.int(index), &ans))
//	return C.GoString(ans), err
//}

//func (p AdapterInfo) Ip(index int) (string, error) {
//	var ans *C.char
//	err := make_error(C.APIAdapterInfo_GetIp(p.object, C.int(index), &ans))
//	return C.GoString(ans), err
//}

//func (p AdapterInfo) SubnetMask(index int) (string, error) {
//	var ans *C.char
//	err := make_error(C.APIAdapterInfo_GetSubnetMask(p.object, C.int(index), &ans))
//	return C.GoString(ans), err
//}

//func (p AdapterInfo) DefaultGateway(index int) (string, error) {
//	var ans *C.char
//	err := make_error(C.APIAdapterInfo_GetDefaultGateway(p.object, C.int(index), &ans))
//	return C.GoString(ans), err
//}

////////////////////////////Status/////////////////////////////
//type Status struct {
//	object *C.APIStatus
//}

//func NewStatus() Status {
//	var x Status
//	make_error(C.APIStatus_create(&x.object))
//	return x
//}

//func (p Status) Free() error {
//	return make_error(C.APIStatus_destroy(&p.object))
//}

//func (p Status) Trigger(port_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetTrigger(p.object, C.int(port_index), &ans))
//	return PortStatus(ans), err
//}

//func (p Status) OOB(port_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetOOB(p.object, C.int(port_index), &ans))
//	return PortStatus(ans), err
//}
//func (p Status) Link(port_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetLink(p.object, C.int(port_index), &ans))
//	return PortStatus(ans), err
//}
//func (p Status) Error(port_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetError(p.object, C.int(port_index), &ans))
//	return PortStatus(ans), err
//}
//func (p Status) Frame(port_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetFrame(p.object, C.int(port_index), &ans))
//	return PortStatus(ans), err
//}
//func (p Status) AnalyzerStarted(link_index int) (PortStatus, error) {
//	var ans C.PortStatusEnum
//	err := make_error(C.APIStatus_GetAnalyzerStarted(p.object, C.int(link_index), &ans))
//	return PortStatus(ans), err
//}
//func (p Status) Speed(link_index int) (EAnalyzerSpeed, error) {
//	var ans C.EAnalyzerSpeed
//	err := make_error(C.APIStatus_GetSpeed(p.object, C.int(link_index), &ans))
//	return EAnalyzerSpeed(ans), err
//}

////////////////////////////TraceIterator/////////////////////////////
//type TraceIterator struct {
//	object *C.APITraceIterator
//}

//func NewTraceIterator(trace *Trace) TraceIterator {
//	var x TraceIterator
//	make_error(C.APITraceIterator_create(trace.object, &x.object))
//	return x
//}

//func (p TraceIterator) Free() error {
//	return make_error(C.APITraceIterator_destroy(&p.object))
//}

//func (p TraceIterator) HasNext() (bool, error) {
//	var ans C.int
//	err := make_error(C.APITraceIterator_HasNext(p.object, &ans))
//	return int(ans) != 0, err
//}

//func (p TraceIterator) HasPrevious() (bool, error) {
//	var ans C.int
//	err := make_error(C.APITraceIterator_HasPrevious(p.object, &ans))
//	return int(ans) != 0, err
//}

//func (p TraceIterator) Next(packet *Packet) error {
//	return make_error(C.APITraceIterator_GetNext(p.object, packet.object))
//}

//func (p TraceIterator) PeekNext(packet *Packet) error {
//	return make_error(C.APITraceIterator_PeekNext(p.object, packet.object))
//}

//func (p TraceIterator) PeekPrevious(packet *Packet) error {
//	return make_error(C.APITraceIterator_PeekPrevious(p.object, packet.object))
//}

//func (p TraceIterator) Previous(packet *Packet) error {
//	return make_error(C.APITraceIterator_GetPrevious(p.object, packet.object))
//}

//func (p TraceIterator) ToBack() error {
//	return make_error(C.APITraceIterator_ToBack(p.object))
//}

//func (p TraceIterator) ToFront() error {
//	return make_error(C.APITraceIterator_ToFront(p.object))
//}

//func (p TraceIterator) Count() (int, error) {
//	var ans C.int
//	err := make_error(C.APITraceIterator_GetCount(p.object, &ans))
//	return int(ans), err
//}

//func (p TraceIterator) At(index int, packet *Packet) error {
//	return make_error(C.APITraceIterator_GetAt(p.object, C.int(index), packet.object))
//}

////////////////////////////Packet/////////////////////////////
//type Packet struct {
//	object *C.APITracePacket
//}

//func NewPacket() Packet {
//	var x Packet
//	make_error(C.APITracePacket_create(&x.object))
//	return x
//}

//func (p Packet) Free() error {
//	return make_error(C.APITracePacket_destroy(&p.object))
//}

//func (p Packet) Timestamp() (int64, error) {
//	var ans C.longlong
//	err := make_error(C.APITracePacket_GetTimeStamp(p.object, &ans))
//	return int64(ans), err
//}

//func (p Packet) Channel() (int, error) {
//	var ans C.int
//	err := make_error(C.APITracePacket_GetChannel(p.object, &ans))
//	return int(ans), err
//}

//func (p Packet) Speed() (EAnalyzerSpeed, error) {
//	var ans C.EAnalyzerSpeed
//	err := make_error(C.APITracePacket_GetSpeed(p.object, &ans))
//	return EAnalyzerSpeed(ans), err
//}

//func (p Packet) Type() (EDataType, error) {
//	var ans C.EDataType
//	err := make_error(C.APITracePacket_GetType(p.object, &ans))
//	return EDataType(ans), err
//}

//func (p Packet) Data() ([]byte, error) {
//	var ans *C.uchar
//	var len C.int
//	err := make_error(C.APITracePacket_GetData(p.object, &ans, &len))
//	return C.GoBytes(unsafe.Pointer(ans), len), err
//}

//func (p Packet) Bookmark() (string, error) {
//	var ans *C.char
//	err := make_error(C.APITracePacket_GetBookmark(p.object, &ans))
//	return C.GoString(ans), err
//}

//func (p Packet) SetBookmark(str string) error {
//	cstr := C.CString(str)
//	defer C.free(unsafe.Pointer(cstr))
//	return make_error(C.APITracePacket_SetBookmark(p.object, cstr))
//}

////////////////////////////DeviceManager/////////////////////////////
//type DeviceManager struct {
//	object *C.APIDeviceManager
//}

//func NewDeviceManager() DeviceManager {
//	var x DeviceManager
//	make_error(C.APIDeviceManager_create(&x.object))
//	return x
//}

//func (p DeviceManager) Free() error {
//	return make_error(C.APIDeviceManager_destroy(&p.object))
//}

////func (p DeviceManager) ConnectByIp(ip string, dev *Device) error {
////	cstr := C.CString(ip)
////	defer C.free(unsafe.Pointer(cstr))

////	var x *C.char
////	return make_error(C.APIDeviceManager_ConnectByIP(&p.object, x, dev.object))
////}

////////////////////////////DeviceInfo/////////////////////////////
//type DeviceInfo struct {
//	object *C.APIDeviceInfo
//}

//func NewDeviceInfo() DeviceInfo {
//	var x DeviceInfo
//	make_error(C.APIDeviceInfo_create(&x.object))
//	return x
//}

//func (p DeviceInfo) Free() error {
//	return make_error(C.APIDeviceInfo_destroy(&p.object))
//}

//func (p DeviceInfo) Count() (int, error) {
//	var ans C.int
//	err := make_error(C.APIDeviceInfo_GetCount(p.object, &ans))
//	return int(ans), err
//}
