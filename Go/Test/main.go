// main
package main

import (
	"Lecroy/TLSAS"
	"fmt"
	"time"
)

func check_error(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}

func main() {
	fmt.Println("Hello World!")
	trace := TLSAS.NewTrace()
	defer trace.Free()
	err := trace.Open("D:/LaptopWin/depot/Software/Puma/_Deliverables/SASProtocolSuite/PublicDocs/Examples/Traces/SCSI and ATA via Expander.sxt")
	check_error(err)
	start := time.Now()
	res, err := trace.RunVScript("D:/LaptopWin/depot/Software/Puma/_Deliverables/SASProtocolSuite/PublicDocs/Examples/VSE/PrimitiveTypes.py")
	fmt.Printf("Result %v Took %v\n", res, time.Since(start))
	check_error(err)
	TLSAS.Unload()
}
