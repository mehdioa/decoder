import qbs

DynamicLibrary {
    name: "DecoderGui"
//    qbs.architecture: project.arch
    files: [
        "**/*.cpp",
        "**/*.h",
    ]

    install: true
    installImportLib: true
//    cpp.allowUnresolvedSymbols: true

    Depends {name: "Qt"; submodules: ["xml", "widgets", "gui"]}

    cpp.defines: ["BOOST_DYNAMIC_BITSET_DONT_USE_FRIENDS", "DECODERGUI_EXPORTS"]
    cpp.cxxLanguageVersion: project.cxxLanguageVersion
    cpp.dynamicLibraries:  [
                                                "DecoderCore",
//                                                "pugixml",
//                                                "KF5Activities", "KF5ConfigWidgets", "KF5IdleTime", "KF5KIONTLM", "KF5PeopleWidgets", "KF5Style",
//                                                "KF5ActivitiesStats", "KF5Contacts", "KF5ItemModels", "KF5KIOWidgets", "KF5PimTextEdit", "KF5Su",
//                                                "KF5Archive", "KF5CoreAddons", "KF5ItemViews", "KF5Kipi", "KF5PlasmaQuick", "KF5SyntaxHighlighting",
//                                                "KF5Attica", "KF5Crash", "KF5JobWidgets", "KF5Kirigami2", "KF5Plasma", "KF5TextEditor",
//                                                "KF5AuthCore", "KF5DBusAddons", "KF5JSApi", "KF5Libkleo", "KF5Prison", "KF5TextWidgets",
//                                                "KF5Auth", "KF5Declarative", "KF5JsEmbed", "KF5ModemManagerQt", "KF5Pty", "KF5ThreadWeaver",
//                                                "KF5Baloo", "KF5DNSSD", "KF5JS", "KF5NetworkManagerQt", "KF5Purpose", "KF5UnitConversion",
//                                                "KF5BalooWidgets", "KF5Emoticons", "KF5KCMUtils", "KF5NewStuffCore", "KF5PurposeWidgets", "KF5Wallet",
//                                                "KF5BluezQt", "KF5FileMetaData", "KF5KDcraw", "KF5NewStuff", "KF5QuickAddons", "KF5WaylandClient",
//                                                "KF5Bookmarks", "KF5GlobalAccel", "KF5KDELibs4Support", "KF5Notifications", "KF5Runner", "KF5WaylandServer",
//                                                "KF5CalendarEvents", "KF5GuiAddons", "KF5KExiv2", "KF5NotifyConfig", "KF5Screen", "KF5WidgetsAddons",
//                                                "KF5Codecs", "KF5Holidays", "KF5KHtml", "KF5Package", "KF5Service", "KF5WindowSystem",
//                                                "KF5Completion", "KF5I18n", "KF5KIOCore", "KF5Parts", "KF5Solid", "KF5XmlGui",
//                                                "KF5ConfigCore", "KF5IconThemes", "KF5KIOFileWidgets", "KF5PeopleBackend", "KF5SonnetCore", "KF5XmlRpcClient",
//                                                "KF5ConfigGui", "KF5IdentityManagement", "KF5KIOGui", "KF5People", "KF5SonnetUi"
                                            ]

    cpp.libraryPaths: project.LIB_DIRS

    cpp.includePaths: {
        var res = project.INC_DIRS;
        res.push(".");
        res.push("..");
        return res;
    }

    Group
    {
        name:"PCH"
        fileTags:"cpp_pch_src"
        files:["pch.hpp"]
        qbs.install: true
        qbs.installSourceBase: ""
        qbs.installDir: "include/Decoder/Gui"

    }

    cpp.useCxxPrecompiledHeader: true

    FileTagger {
        patterns: ["*.h", "*.hpp"]
        fileTags: ["header_files"]
    }

    Group
    {
        name:"Headers"
        fileTagsFilter: "header_files"
        qbs.install: true
        qbs.installSourceBase: ""
        qbs.installDir: "include/Decoder/Gui"
    }

    Group {
        name: "Style Files"
        qbs.install: true
        files: [
            "**/*.css",
        ]
    }
}
