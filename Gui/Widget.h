#pragma once

#define COMPANY_NAME "MyCompany"
#define APP_NAME "MyApp"

namespace Gui {
template <typename Q_Widget>
class Widget : public Q_Widget {
public:
    Widget(const QString& name, QWidget* parent = nullptr)
        : Q_Widget(parent)
        , m_Name(name)
    {
    }

    void readSettings()
    {
        QSettings settings(COMPANY_NAME, APP_NAME);
        Q_Widget::restoreGeometry(settings.value(m_Name).toByteArray());
        //    restoreState(settings.value("myWidget/windowState").toByteArray());
    }

protected:
    void closeEvent(QCloseEvent* event)
    {
        QSettings settings(COMPANY_NAME, APP_NAME);
        settings.setValue(m_Name, Q_Widget::saveGeometry());
        //    settings.setValue("windowState", saveState());
        QWidget::closeEvent(event);
    }

private:
    QString m_Name;
};
}
