#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API SpreadSheet : public QTableWidget {
        Q_OBJECT
    public:
        SpreadSheet(int trace_id, Core::ViewType view_type, QWidget* parent = nullptr);
        ~SpreadSheet();

        void fillRow(size_t row, const Core::Business::RowData& row_data);

    Q_SIGNALS:
        void currentItemChangedSignal(Core::Business::Packet* packet);

    public Q_SLOTS:
        void onCurrentItemChanged(QTableWidgetItem* current, QTableWidgetItem* previous);

    protected:
        void setupContextMenu();
        void createActions();

    private:
        QLabel* cellLabel;
        int m_TraceId;
        Core::ViewType m_ViewType;
    };
}
}
