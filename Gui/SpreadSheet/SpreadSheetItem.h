#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API SpreadSheetItem : public QTableWidgetItem {
    public:
        using QTableWidgetItem::QTableWidgetItem;

        SpreadSheetItem(const char* field_name, Core::Business::Packet* df);

        //        QVariant data(int role) const override;
        //        void setData(int role, const QVariant& value) override;
        //        QVariant display() const;

        //        inline QString formula() const
        //        {
        //            return QTableWidgetItem::data(Qt::DisplayRole).toString();
        //        }

        //        static QVariant computeFormula(const QString& formula,
        //            const QTableWidget* widget,
        //            const QTableWidgetItem* self = nullptr);

        void setValue();

        Core::Business::Packet* packet() const;

    private:
        mutable bool isResolving = false;
        Core::Business::Packet* m_Packet;
        std::string m_FieldName;
    };

}
}
