// clang-format off
#include "pch.hpp"
#include "SpreadSheet.h"
//#include "SpreadSheetDelegate.h"
#include "SpreadSheetItem.h"
//#include "Core/Business/TraceFile.h"
#include "Core/Business/Packet.h"
#include "Core/Settings/ColumnSet.h"
#include "Core/Settings/ColumnSets.h"
#include "Core/Settings/Protocol.h"
#include "Core/Base/Protocol.h"
#include "../Base/FieldView.h"
#include "../Base/DataView.h"
#include "Core/Business/Packet.h"
#include "Core/Business/Cell.h"
// clang-format on

namespace Gui {
namespace Base {

    SpreadSheet::SpreadSheet(int trace_id, Core::ViewType view_type, QWidget* parent)
        : QTableWidget(parent)
        , m_TraceId(trace_id)
        , m_ViewType(view_type)
    {
        //        int cols = m_ColumnSet->Size();
        //        int rows = m_TraceFile->packetCount();
        //        setColumnCount(cols);
        //        setRowCount(rows);
        //        for (auto row = 0; row < rows; ++row) {
        //            auto db = new Core::Base::DecodedBuffer;
        //            db->m_Buffer = m_TraceFile->packetBuffer(row);
        //            db->m_DecodedFieldTree = m_Protocol->baseProtocol()->decode(db->m_Buffer);

        //            auto item = new QTableWidgetItem;
        //            setItem(row, 0, item);
        //            item = new QTableWidgetItem;
        //            auto packet = m_TraceFile->packet(row);
        //            item->setText(QString::number(packet->timestamp()));
        //            setItem(row, 1, item);

        //            for (auto col = 0; col < cols; ++col) {
        //                auto column = m_ColumnSet->At(col + 2);
        //                auto item = new SpreadSheetItem(column, db);
        //                item->setValue();
        //            }
        //            m_DecodedBuffers.push_back(db);
        //        }
        setSelectionBehavior(QAbstractItemView::SelectRows);
        setSizeAdjustPolicy(QTableWidget::AdjustToContents);

        //        auto all_cols = m_ColumnSet->All();
        //        QStringList headers;
        //        for (auto col : all_cols)
        //            headers.append(QString::fromStdString(col));
        //        setHorizontalHeaderLabels(headers);

        //        setItemPrototype(item(rows - 1, cols - 1));
        //        setItemDelegate(new SpreadSheetDelegate());

        createActions();
        setupContextMenu();

        setWindowTitle(tr("Spreadsheet"));

        connect(this, &QTableWidget::currentItemChanged, this, &SpreadSheet::onCurrentItemChanged);
    }

    SpreadSheet::~SpreadSheet()
    {
    }

    void SpreadSheet::fillRow(size_t row, const Core::Business::RowData& row_data)
    {
        size_t col = 0;
        for (const auto& cell : row_data.m_Cells) {
            auto item = new SpreadSheetItem("", row_data.m_Packet);
            item->setText(cell->Value.c_str());
            QBrush brush;
            brush.setColor(QColor::fromRgb(cell->Foreground));
            item->setForeground(brush);
            brush.setColor(QColor::fromRgb(cell->Background));
            item->setBackground(brush);
            setItem(row, col++, item);
        }
    }

    void SpreadSheet::onCurrentItemChanged(QTableWidgetItem* current, QTableWidgetItem* previous)
    {
        if (!current)
            return;
        auto item = static_cast<SpreadSheetItem*>(current);
        if (!item)
            return;
        //        auto packet = static_cast<Core::Business::Packet*>(item->data(ItemDataRole::EPacket).value<void*>());
        //        if (packet) {
        emit currentItemChangedSignal(item->packet());
        //        }
    }

    void SpreadSheet::createActions()
    {
    }

    void SpreadSheet::setupContextMenu()
    {
        //        addAction(cell_addAction);
        //        addAction(cell_subAction);
        //        addAction(cell_mulAction);
        //        addAction(cell_divAction);
        //        addAction(cell_sumAction);
        //        addAction(firstSeparator);
        //        addAction(colorAction);
        //        addAction(fontAction);
        //        addAction(secondSeparator);
        //        addAction(clearAction);
        setContextMenuPolicy(Qt::ActionsContextMenu);
    }

}
}
