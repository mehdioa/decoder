// clang-format off
#include "pch.hpp"
#include "DataView.h"
#include "Core/Base/Buffer.h"
#include "QHexEdit.h"
// clang-format on

using namespace Gui::Base;

struct DataView::DataViewPrivate {
    DataView* m_Parent;
    QHexEdit dataView;

    DataViewPrivate(DataView* parent)
        : m_Parent(parent)
    {
    }
};

DataView::DataView(QWidget* parent)
    : QWidget(parent)
    , Impl(std::make_unique<DataViewPrivate>(this))
{
    auto lyMain = new QBoxLayout(QBoxLayout::TopToBottom);
    {
        lyMain->addWidget(&Impl->dataView);
    }

    setLayout(lyMain);
}

DataView::~DataView() = default;

void DataView::setEditable(bool b)
{
    //    Impl->fieldDelegate.setEnabled(b);
}

void DataView::setPacket(Core::Business::Packet* packet)
{
    Impl->dataView.setPacket(packet);
}
