#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API TreeView : public QWidget {
        Q_OBJECT

    public:
        TreeView();
        ~TreeView();

        void setModel(TreeModel* model);

    Q_SIGNALS:
        void currentChanged(const QModelIndex& current, const QModelIndex& previous);

        //    public Q_SLOTS:
        //        void setEditable(bool b);

    private Q_SLOTS:
        void OnFilter(const QString& text);

    private:
        struct TreeViewPrivate;
        std::unique_ptr<TreeViewPrivate> Impl;
    };
}
}
