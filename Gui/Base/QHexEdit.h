#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API QHexEdit : public QAbstractScrollArea {
        Q_OBJECT

    public:
        QHexEdit(QWidget* parent = nullptr);
        Core::BitSet dataAt(qint64 pos, qint64 count = -1);
        void insert(qint64 pos, char ch);
        void remove(qint64 pos, qint64 len = 1);
        void replace(qint64 pos, char ch);
        void insert(qint64 pos, const QByteArray& ba);
        void replace(qint64 pos, qint64 len, const QByteArray& ba);
        qint64 cursorPosition(QPoint point);
        void ensureVisible();
        qint64 indexOf(const QByteArray& ba, qint64 from);
        bool isModified();
        qint64 lastIndexOf(const QByteArray& ba, qint64 from);
        QString selectionToReadableString();
        void setFont(const QFont& font);
        QString toReadableString();

    public Q_SLOTS:
        void redo();
        void undo();

    Q_SIGNALS:
        void currentAddressChanged(qint64 address);
        void currentSizeChanged(qint64 size);
        void dataChanged();
        void overwriteModeChanged(bool state);

    public:
        ~QHexEdit();

        // Properties
        bool addressArea();
        void setAddressArea(bool addressArea);

        QColor addressAreaColor();
        void setAddressAreaColor(const QColor& color);

        qint64 addressOffset();
        void setAddressOffset(qint64 addressArea);

        int addressWidth();
        void setAddressWidth(int addressWidth);

        bool asciiArea();
        void setAsciiArea(bool asciiArea);

        int bytesPerLine();
        void setBytesPerLine(int count);

        qint64 cursorPosition();
        void setCursorPosition(qint64 position);

        Core::BitSet data();
        void setPacket(Core::Business::Packet* packet);

        void setHexCaps(const bool isCaps);
        bool hexCaps();

        void setDynamicBytesPerLine(const bool isDynamic);
        bool dynamicBytesPerLine();

        bool highlighting();
        void setHighlighting(bool mode);

        QColor highlightingColor();
        void setHighlightingColor();

        bool overwriteMode();
        void setOverwriteMode(bool overwriteMode);

        bool isReadOnly();
        void setReadOnly(bool readOnly);

        QColor selectionColor();
        void setSelectionColor(const QColor& color);

    protected:
        // Handle events
        void keyPressEvent(QKeyEvent* event);
        void mouseMoveEvent(QMouseEvent* event);
        void mousePressEvent(QMouseEvent* event);
        void paintEvent(QPaintEvent* event);
        void resizeEvent(QResizeEvent*);
        virtual bool focusNextPrevChild(bool next);

    private:
        // Handle selections
        void resetSelection(qint64 pos); // set selectionStart and selectionEnd to pos
        void resetSelection(); // set selectionEnd to selectionStart
        void setSelection(qint64 pos); // set min (if below init) or max (if greater init)
        int getSelectionBegin();
        int getSelectionEnd();

        // Private utility functions
        void init();
        void readBuffers();
        QString toReadable(int start, int stop);

        qint64 bufferSize() const;

    private Q_SLOTS:
        void adjust(); // recalc pixel positions
        void dataChangedPrivate(int idx = 0); // emit dataChanged() signal
        void refresh(); // ensureVisible() and readBuffers()
        void updateCursor(); // update blinking cursor

    private:
        // Name convention: pixel positions start with _px
        int _pxCharWidth, _pxCharHeight; // char dimensions (dependend on font)
        int _pxPosHexX; // X-Pos of HeaxArea
        int _pxPosAdrX; // X-Pos of Address Area
        int _pxPosAsciiX; // X-Pos of Ascii Area
        int _pxGapAdr; // gap left from AddressArea
        int _pxGapAdrHex; // gap between AddressArea and HexAerea
        int _pxGapHexAscii; // gap between HexArea and AsciiArea
        int _pxCursorWidth; // cursor width
        int _pxSelectionSub; // offset selection rect
        int _pxCursorX; // current cursor pos
        int _pxCursorY; // current cursor pos

        // Name convention: absolute byte positions in chunks start with _b
        qint64 _bSelectionBegin; // first position of Selection
        qint64 _bSelectionEnd; // end of Selection
        qint64 _bSelectionInit; // memory position of Selection
        qint64 _bPosFirst; // position of first byte shown
        qint64 _bPosLast; // position of last byte shown
        qint64 _bPosCurrent; // current position

        // variables to store the property values
        bool _addressArea; // left area of QHexEdit
        QColor _addressAreaColor;
        int _addressWidth;
        bool _asciiArea;
        qint64 _addressOffset;
        int _bytesPerLine;
        int _hexCharsInLine;
        bool _highlighting;
        bool _overwriteMode;
        QBrush _brushSelection;
        QPen _penSelection;
        QBrush _brushHighlighted;
        QPen _penHighlighted;
        bool _readOnly;
        bool _hexCaps;
        bool _dynamicBytesPerLine;

        // other variables
        bool _editAreaIsAscii; // flag about the ascii mode edited
        int _addrDigits; // real no of addressdigits, may be > addressWidth
        bool _blink; // help get cursor blinking
        Core::Base::IBuffer* m_Buffer = nullptr;
        QTimer _cursorTimer; // for blinking cursor
        qint64 _cursorPosition; // absolute position of cursor, 1 Byte == 2 tics
        QRect _cursorRect; // physical dimensions of cursor
        QByteArray _dataShown; // data in the current View
        QByteArray _hexDataShown; // data in view, transformed to hex
        qint64 _lastEventSize; // size, which was emitted last time
        QByteArray _markedShown; // marked data in view
        bool _modified; // Is any data in editor modified?
        int _rowsShown; // lines of text shown
    };
}
}
