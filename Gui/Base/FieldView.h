#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API FieldView : public QWidget {
        Q_OBJECT
    public:
        FieldView(bool is_editable = false, QWidget* parent = nullptr);
        ~FieldView();

    public Q_SLOTS:
        void setPacket(Core::Business::Packet* packet);

    private:
        struct FieldViewPrivate;
        std::unique_ptr<FieldViewPrivate> Impl;
    };
}
}
