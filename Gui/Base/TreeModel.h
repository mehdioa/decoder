#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API TreeModel : public QAbstractItemModel {
    public:
        explicit TreeModel();
        void init(Setting::NodeTree* d);
        ~TreeModel(); // Destructor

        QVariant data(const QModelIndex& index, int role) const override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;
        QVariant headerData(int section, Qt::Orientation orientation,
            int role = Qt::DisplayRole) const override;
        QModelIndex index(int row, int column,
            const QModelIndex& parent = QModelIndex()) const override;
        QModelIndex parent(const QModelIndex& index) const override;
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    private:
        struct TreeModelPrivate;
        std::unique_ptr<TreeModelPrivate> Impl;
    };
}
}
