// clang-format off
#include "pch.hpp"
#include "FieldTreeModel.h"
#include "Core/Base/Buffer.h"
#include "Core/Base/Field.h"
#include "Core/Base/DecodedField.h"
#include "Core/Business/Packet.h"
#include "Core/tree.h"
// clang-format on

using namespace Gui::Base;

struct FieldTreeModel::FieldTreeModelPrivate {
    FieldTreeModel* m_FieldTreeModel;
    Core::Business::Packet* m_Packet;
    bool m_IsEditable = false;

    FieldTreeModelPrivate(bool is_editable, FieldTreeModel* p)
        : m_FieldTreeModel(p)
        , m_IsEditable(is_editable)
    {
    }
};

FieldTreeModel::FieldTreeModel(QObject* parent)
    : QAbstractItemModel(parent)
    , Impl(std::make_unique<FieldTreeModel::FieldTreeModelPrivate>(false, this))
{
}

FieldTreeModel::~FieldTreeModel() = default;

int FieldTreeModel::columnCount(const QModelIndex& parent) const
{
    return 2;
}

void FieldTreeModel::init(Core::Business::Packet* packet)
{
    Impl->m_Packet = packet;
}

QVariant FieldTreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    auto parent = static_cast<Core::Base::DecodedFieldTree*>(index.internalPointer());
    if (!parent)
        return QVariant();

    switch (index.column()) {
    case 0:
        return QString::fromStdString(parent->value()->field()->name());
    case 1:
        return QString::fromStdString(parent->value()->value(Core::ValueType::Hex, Impl->m_Packet->m_Buffer));
    }
    return QVariant();
}

Qt::ItemFlags FieldTreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant FieldTreeModel::headerData(int section, Qt::Orientation orientation,
    int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        switch (section) {
        case 0:
            return "Name";
        case 1:
            return "Value";
        }
    }
    return QVariant();
}

QModelIndex FieldTreeModel::index(int row, int column, const QModelIndex& parent)
    const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    Core::Base::DecodedFieldTree* item = nullptr;

    if (!parent.isValid())
        item = Impl->m_Packet->m_DecodedFieldTree;
    else
        item = static_cast<Core::Base::DecodedFieldTree*>(parent.internalPointer());

    if (item) {
        auto child = item->at(row);

        if (child)
            return createIndex(row, column, child);
    }
    return QModelIndex();
}

QModelIndex FieldTreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    auto child = static_cast<Core::Base::DecodedFieldTree*>(index.internalPointer());
    auto parent = child->parent();

    if (!parent || parent == Impl->m_Packet->m_DecodedFieldTree)
        return QModelIndex();

    return createIndex(parent->width(), 0, parent);
}

int FieldTreeModel::rowCount(const QModelIndex& parent) const
{
    if (parent.column() > 0)
        return 0;

    if (parent.isValid()) {
        auto item = static_cast<Core::Base::DecodedFieldTree*>(parent.internalPointer());
        if (item)
            return item->width();
    }
    return Impl->m_Packet->m_DecodedFieldTree->width();
}

bool Gui::Base::FieldTreeModel::setData(const QModelIndex& index, const QVariant& value, int role /*= Qt::EditRole*/)
{
    return true;
}
