// clang-format off
#include "pch.hpp"
#include "TreeView.h"
#include "Core/Base/Protocol.h"
#include "TreeModel.h"

// clang-format on

using namespace Gui::Base;

struct TreeView::TreeViewPrivate {
    TreeView* m_Parent;
    TreeModel* m_TreeModel = nullptr;
    QTreeView m_TreeView;
    QLineEdit m_FilterLineEdit;
    //    TreeItemDelegate m_TreeItemDelegate;
    QSortFilterProxyModel m_ProxyModel;

    TreeViewPrivate(TreeView* parent)
        : m_Parent(parent)
    {
    }

    void setModel(TreeModel* model)
    {
        auto selection_model = m_TreeView.selectionModel();
        if (selection_model) {
            disconnect(m_TreeView.selectionModel(), &QItemSelectionModel::currentChanged, m_Parent, &TreeView::currentChanged);
        }
        m_TreeModel = model;
        m_ProxyModel.setSourceModel(m_TreeModel);
        m_TreeView.setModel(&m_ProxyModel);
        m_TreeView.setSortingEnabled(true);
        selection_model = m_TreeView.selectionModel();
        connect(selection_model, &QItemSelectionModel::currentChanged, m_Parent, &TreeView::currentChanged);
    }
};

TreeView::TreeView()
    : Impl(std::make_unique<TreeViewPrivate>(this))
{
    auto lyMain = new QBoxLayout(QBoxLayout::TopToBottom);
    {
        lyMain->addWidget(&Impl->m_FilterLineEdit);
        lyMain->addWidget(&Impl->m_TreeView);
    }

    setLayout(lyMain);

    Impl->m_TreeView.setSelectionBehavior(QAbstractItemView::SelectItems);

    connect(&Impl->m_FilterLineEdit, &QLineEdit::textChanged, this, &TreeView::OnFilter);
}

TreeView::~TreeView() = default;

void TreeView::setModel(TreeModel* model)
{
    Impl->setModel(model);
}

//void TreeView::setEditable(bool b)
//{
//    auto e = Impl->m_FilterLineEdit.isEnabled();
//    Impl->m_FilterLineEdit.setEnabled(!e);
//    if (!e) {
//        Impl->m_TreeView.setItemDelegate(&Impl->m_TreeItemDelegate);
//    } else {
//    }
//}

void TreeView::OnFilter(const QString& text)
{
    Impl->m_ProxyModel.setFilterRegExp(QRegExp(text, Qt::CaseInsensitive,
        QRegExp::FixedString));
    Impl->m_ProxyModel.setFilterKeyColumn(0);
}
