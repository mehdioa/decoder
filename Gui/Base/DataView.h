#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API DataView : public QWidget {
        Q_OBJECT
    public:
        DataView(QWidget* parent = nullptr);
        ~DataView();

    public Q_SLOTS:
        void setEditable(bool b);
        void setPacket(Core::Business::Packet* packet);

        //    private Q_SLOTS:

    private:
        struct DataViewPrivate;
        std::unique_ptr<DataViewPrivate> Impl;
    };
}
}
