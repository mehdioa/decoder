// clang-format off
#include "pch.hpp"
#include "TreeModel.h"
#include "Core/tree.h"
#include "../Settings/Node.h"
// clang-format on

using namespace Gui::Base;

struct TreeModel::TreeModelPrivate {
    TreeModel* m_TreeModel;
    Setting::NodeTree* m_NodeTree;
    bool m_IsEditable = false;

    TreeModelPrivate(TreeModel* p)
        : m_TreeModel(p)
    {
    }
};

TreeModel::TreeModel()
    : Impl(std::make_unique<TreeModel::TreeModelPrivate>(this))
{
}

TreeModel::~TreeModel() = default;

int TreeModel::columnCount(const QModelIndex& parent) const
{
    return 1;
}

void TreeModel::init(Gui::Setting::NodeTree* node_tree)
{
    Impl->m_NodeTree = node_tree;
}

QVariant TreeModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    auto parentItem = static_cast<Setting::NodeTree*>(index.internalPointer());
    if (!parentItem)
        return QVariant();

    auto t = parentItem->value();
    if (t) {
        switch (index.column()) {
        case 0:
            return t->name();
        }
    }
    return QVariant();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
    int role) const
{
    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex& parent)
    const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    Setting::NodeTree* parentItem;

    if (!parent.isValid())
        parentItem = Impl->m_NodeTree;
    else
        parentItem = static_cast<Setting::NodeTree*>(parent.internalPointer());

    if (parentItem)
        return createIndex(row, column, parentItem->at(row));
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex& index) const
{
    if (!index.isValid())
        return QModelIndex();

    auto* child = static_cast<Setting::NodeTree*>(index.internalPointer());
    auto parent = child->parent();

    if (!parent || parent == Impl->m_NodeTree)
        return QModelIndex();

    if (parent == Impl->m_NodeTree)
        return QModelIndex();

    return createIndex(parent->width(), 0, parent);
}

int TreeModel::rowCount(const QModelIndex& parent) const
{
    if (parent.column() > 0)
        return 0;

    if (parent.isValid()) {
        auto item = static_cast<Setting::NodeTree*>(parent.internalPointer());
        if (item)
            return item->width();
    }
    return Impl->m_NodeTree->width();
}
