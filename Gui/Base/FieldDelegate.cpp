// clang-format off
#include "pch.hpp"
#include "Core/tree.h"
#include "FieldDelegate.h"
#include "Core/Base/Field.h"
#include "Core/Base/DecodedField.h"
//#include "FieldTreeModelData.h"
// clang-format on

using namespace Gui::Base;

FieldDelegate::FieldDelegate()
    : QStyledItemDelegate()
{
}

QWidget* FieldDelegate::createEditor(QWidget* parent,
    const QStyleOptionViewItem& /* option */,
    const QModelIndex& index) const
{
    //    if (m_Enabled) {
    //        if (index.column() == 1) {
    //            auto item = static_cast<Core::Base::DecodedFieldTree*>(index.internalPointer());
    //            //            auto model_data = static_cast<FieldTreeModelData*>(item->modelData());
    //            //            auto decoded_buffer = item->dec model_data->decodedBuffer();
    //            auto root = item->m_root;
    //            if (root == nullptr) {
    //                //Error("createEditor on null root");
    //                return nullptr;
    //            }
    //            auto field = root->field();
    //            auto option = root->option();

    //            if (field->optionsSize() == 0) {
    //                auto editor = new QLineEdit(parent);
    //                editor->setText(model_data->data(1).toString());
    //                return editor;
    //            }

    //            auto editor = new QComboBox(parent);
    //            editor->setFrame(false);
    //            int idx = -1;
    //            field->traverse([&idx, &editor, option](Core::Base::Option* op) {
    //                auto min_max = op->minMax();
    //                std::stringstream ss;
    //                for (auto i = min_max.first; i <= min_max.second; ++i) {
    //                    idx++;
    //                    ss << i << ": " << op->name();
    //                    editor->addItem(QString::fromStdString(ss.str()));
    //                }
    //                if (option == op)
    //                    editor->setCurrentIndex(idx);
    //                return false;
    //            });
    //            return editor;
    //        }
    //    } else {
    //        auto editor = new QLineEdit(parent);
    //        auto item = static_cast<FieldTreeItem*>(index.internalPointer());
    //        auto model_data = static_cast<FieldTreeModelData*>(item->modelData());
    //        editor->setText(model_data->data(index.column()).toString());
    //        return editor;
    //    }
    return nullptr;
}

void FieldDelegate::setEditorData(QWidget* editor,
    const QModelIndex& index) const
{
    if (m_Enabled) {
        int value = index.model()->data(index, Qt::EditRole).toInt();

        auto combo_box = static_cast<QComboBox*>(editor);

        //    UNUSED(value);
        //    UNUSED(combo_box);
    } else {
        int value = index.model()->data(index, Qt::EditRole).toInt();

        auto line_edit = static_cast<QLineEdit*>(editor);
    }
}

void FieldDelegate::setModelData(QWidget* editor, QAbstractItemModel* model,
    const QModelIndex& index) const
{
    if (m_Enabled) {
        auto combo_box = static_cast<QComboBox*>(editor);

        //    UNUSED(combo_box);
        //    combo_box->interpretText();
        int value = combo_box->currentIndex();

        model->setData(index, value, Qt::EditRole);
    } else {
        auto line_edit = static_cast<QLineEdit*>(editor);

        //    UNUSED(combo_box);
        //    combo_box->interpretText();
        auto value = line_edit->text();

        model->setData(index, value, Qt::EditRole);
    }
}

void FieldDelegate::updateEditorGeometry(QWidget* editor,
    const QStyleOptionViewItem& option, const QModelIndex& /* index */) const
{
    editor->setGeometry(option.rect);
}

void FieldDelegate::setEnabled(bool Enabled)
{
    m_Enabled = Enabled;
}
