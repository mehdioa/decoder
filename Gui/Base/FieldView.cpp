﻿// clang-format off
#include "pch.hpp"
#include "FieldView.h"
#include "Core/Base/Protocol.h"
#include "FieldDelegate.h"
#include "FieldTreeModel.h"
//#include "FieldTreeModelData.h"
#include "Core/Business/Packet.h"
// clang-format on

using namespace Gui::Base;

struct FieldView::FieldViewPrivate {
    QWidget* m_Parent;
    FieldTreeModel m_FieldTreeModel;
    QTreeView m_FieldTreeView;
    FieldDelegate m_FieldDelegate;
    Core::Business::Packet* m_Packet;
    bool m_IsEditable;

    FieldViewPrivate(bool is_editable, FieldView* parent)
        : m_Parent(parent)
        , m_IsEditable(is_editable)
    {
    }

    void setPacket(Core::Business::Packet* packet)
    {
        if (m_Packet == packet)
            return;
        m_Packet = packet;
        m_FieldTreeModel.init(m_Packet);

        m_FieldTreeView.setModel(&m_FieldTreeModel);

        if (m_IsEditable) {
            m_FieldTreeView.setItemDelegate(&m_FieldDelegate);
        }

        m_FieldTreeView.expandAll();
        for (int column = 0; column < m_FieldTreeModel.columnCount(); ++column)
            m_FieldTreeView.resizeColumnToContents(column);
    }
};

FieldView::FieldView(bool is_editable, QWidget* parent)
    : QWidget(parent)
    , Impl(std::make_unique<FieldViewPrivate>(is_editable, this))
{
    Impl->m_FieldTreeView.setSelectionBehavior(QAbstractItemView::SelectItems);
    Impl->m_FieldTreeView.setItemDelegate(&Impl->m_FieldDelegate);
    Impl->m_FieldDelegate.setEnabled(false);

    auto lyMain = new QBoxLayout(QBoxLayout::TopToBottom);
    {
        lyMain->addWidget(&Impl->m_FieldTreeView);
    }

    setLayout(lyMain);
}

FieldView::~FieldView() = default;

void FieldView::setPacket(Core::Business::Packet* packet)
{
    Impl->setPacket(packet);
}
