#pragma once

namespace Gui {
	namespace Base {

		class DECODERGUI_API FieldDelegate : public QStyledItemDelegate {
		public:
			FieldDelegate();

			QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option,
				const QModelIndex& index) const override;

			void setEditorData(QWidget* editor, const QModelIndex& index) const override;
			void setModelData(QWidget* editor, QAbstractItemModel* model,
				const QModelIndex& index) const override;

			void updateEditorGeometry(QWidget* editor,
				const QStyleOptionViewItem& option, const QModelIndex& index) const override;

			void setEnabled(bool Enabled);

		private:
			bool m_Enabled;
		};
	}
}