#pragma once

#pragma once

namespace Gui {
namespace Base {
    template <class T>
    class DECODERGUI_API TreeItemModel : public QAbstractItemModel {
    public:
        typedef tree<T> tree_type;

        void init(tree_type* d, std::list<std::string>) { _tree = d; }

        QVariant data(const QModelIndex& index, int role) const override
        {
            if (!index.isValid())
                return QVariant();

            if (role != Qt::DisplayRole && role != Qt::EditRole)
                return QVariant();

            auto parentItem = static_cast<tree_type*>(index.internalPointer());
            if (!parentItem)
                return QVariant();

            auto t = parentItem->value();
            if (t) {
                switch (index.column()) {
                case 0:
                    return t->name();
                }
            }
            return QVariant();
        }

        Qt::ItemFlags flags(const QModelIndex& index) const override
        {
            if (!index.isValid())
                return Qt::NoItemFlags;

            return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
        }

        QVariant headerData(int section, Qt::Orientation orientation,
            int role = Qt::DisplayRole) const override
        {
            return QVariant();
        }

        QModelIndex index(int row, int column,
            const QModelIndex& parent = QModelIndex()) const override
        {
            if (!hasIndex(row, column, parent))
                return QModelIndex();

            tree_type* parent_item;

            if (!parent.isValid())
                parent_item = _tree;
            else
                parent_item = static_cast<tree_type*>(parent.internalPointer());

            if (parent_item)
                return createIndex(row, column, parent_item->at(row));
            else
                return QModelIndex();
        }

        QModelIndex parent(const QModelIndex& index) const override
        {
            if (!index.isValid())
                return QModelIndex();

            auto* child = static_cast<tree_type*>(index.internalPointer());
            auto parent = child->parent();

            if (!parent || parent == _tree)
                return QModelIndex();

            if (parent == _tree)
                return QModelIndex();

            return createIndex(parent->width(), 0, parent);
        }

        int rowCount(const QModelIndex& parent = QModelIndex()) const override
        {
            //            if (parent.column() > 0)
            //                return 0;

            if (parent.isValid()) {
                auto item = static_cast<tree_type*>(parent.internalPointer());
                if (item)
                    return item->width();
            }
            return _tree->width();
        }

        int columnCount(const QModelIndex& parent = QModelIndex()) const override
        {
            return 1;
        }

    private:
        tree_type* _tree;
    };
}
}
