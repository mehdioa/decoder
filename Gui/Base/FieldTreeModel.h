#pragma once

namespace Gui {
namespace Base {

    class DECODERGUI_API FieldTreeModel : public QAbstractItemModel {
    public:
        explicit FieldTreeModel(QObject* parent = nullptr);
        void init(Core::Business::Packet* packet);
        ~FieldTreeModel(); // Destructor

        QVariant data(const QModelIndex& index, int role) const override;
        Qt::ItemFlags flags(const QModelIndex& index) const override;
        QVariant headerData(int section, Qt::Orientation orientation,
            int role = Qt::DisplayRole) const override;
        QModelIndex index(int row, int column,
            const QModelIndex& parent = QModelIndex()) const override;
        QModelIndex parent(const QModelIndex& index) const override;
        int rowCount(const QModelIndex& parent = QModelIndex()) const override;
        int columnCount(const QModelIndex& parent = QModelIndex()) const override;
        bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    private:
        struct FieldTreeModelPrivate;
        std::unique_ptr<FieldTreeModelPrivate> Impl;
    };

}
}
