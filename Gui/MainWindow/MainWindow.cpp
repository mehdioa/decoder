// clang-format off
#include "pch.hpp"
#include "MainWindow.h"
#include "Core/Settings/Settings.h"
#include "Gui/Settings/Settings.h"
#include "Gui/Base/DataView.h"
#include "Gui/Base/FieldView.h"
#include "Core/Base/Buffer.h"
#include "Gui/SpreadSheet/SpreadSheet.h"
#include "Core/Settings/ColumnSets.h"
#include "Core/Settings/ColumnSet.h"
#include "Core/Settings/Column.h"
#include "Core/Settings/Protocols.h"
#include "Core/Settings/Protocol.h"
#include "Core/Business/Service.h"
#include "Core/Business/Packet.h"
#include "Core/tree.h"
#include <QTextCodec>

// clang-format on

using namespace Gui::Base;

struct MainWindow::MainWindowPrivate {
    MainWindow* m_Parent;

    Gui::Setting::Settings* m_Settings = nullptr;
    Core::Business::Service* m_Service;

    QTabWidget m_TraceView;
    QStackedWidget* m_FieldView;
    QStackedWidget* m_DataView;
    SpreadSheet* m_SpreadSheet;

    QMenu* m_MenuFile;
    QMenu* m_MenuEdit;
    QMenu* m_MenuTools;
    QMenu* m_MenuView;
    QMenu* m_MenuHelp;
    QMenu* m_MenuRecentTrces;
    QMenu* m_MenuRecentProjects;

    QAction* m_ActionNewProject;
    QAction* m_ActionOption;
    QAction* m_ActionSaveTrace;
    QAction* m_ActionSaveTraceAs;
    QAction* m_ActionSaveAllTraces;
    QAction* m_ActionSaveProject;
    QAction* m_ActionSaveProjectAs;
    QAction* m_ActionCloseProject;
    QAction* m_ActionRecentTraces;
    QAction* m_ActionRecentProjects;
    QAction* m_ActionQuit;
    QAction* m_ActionPreferences;
    QAction* m_ActionResetLayout;
    QAction* m_ActionSaveLayout;

    //    QToolBar* fileToolBar;
    QToolBar* m_ProjectToolBar;

    std::list<Core::Business::TraceFile*> m_TraceFiles;

    MainWindowPrivate(Core::Business::Service* service, MainWindow* parent)
        : m_Parent(parent)
        , m_Settings(new Gui::Setting::Settings(service->settings()))
        , m_Service(service)
        , m_TraceView(QTabWidget())
        , m_FieldView(new QStackedWidget(m_Parent))
        , m_DataView(new QStackedWidget(m_Parent))
    {
        m_Parent->setWindowTitle(tr("SAS Analyzer"));
        m_Parent->setUnifiedTitleAndToolBarOnMac(true);

        m_Parent->restoreGeometry(QString::fromStdString(m_Settings->CoreSettings()->GetGeometry()).toLocal8Bit());
        m_Parent->restoreState(QString::fromStdString(m_Settings->CoreSettings()->GetState()).toLocal8Bit());

        connect(m_Settings, &QDialog::finished, m_Parent, &MainWindow::OnPreferencesFinished, Qt::UniqueConnection);

        m_Parent->setCentralWidget(&m_TraceView);
        m_TraceView.setTabsClosable(true);
        connect(&m_TraceView, &QTabWidget::tabCloseRequested, m_Parent, &MainWindow::OnCloseTrace);
        connect(&m_TraceView, &QTabWidget::currentChanged, m_Parent, &MainWindow::OnCurrentTabChanged);
        createActions();
        createDockWindows();
        createStatusBar();

        connect(m_MenuRecentTrces, &QMenu::aboutToShow, m_Parent, &MainWindow::MenuRecentTracesAboutToShow);
    }

    void createActions()
    {
        m_ProjectToolBar = m_Parent->addToolBar(tr("Project"));

        m_MenuFile = m_Parent->menuBar()->addMenu(tr("&File"));

        const QIcon new_icon = QIcon::fromTheme("document-new", QIcon(":/images/new.png"));
        m_ActionNewProject = new QAction(new_icon, tr("&New Project"), m_Parent);
        m_ActionNewProject->setShortcuts(QKeySequence::New);
        m_ActionNewProject->setStatusTip(tr("New Project"));
        connect(m_ActionNewProject, &QAction::triggered, m_Parent, &MainWindow::OnNewProject);
        m_MenuFile->addAction(m_ActionNewProject);
        //        fileToolBar->addAction(m_ActionNewProject);

        const QIcon open_icon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
        m_ActionOption = new QAction(open_icon, tr("&Open"), m_Parent);
        m_ActionOption->setShortcuts(QKeySequence::Open);
        m_ActionOption->setStatusTip(tr("Open"));
        connect(m_ActionOption, &QAction::triggered, m_Parent, &MainWindow::OnOpen);
        m_MenuFile->addAction(m_ActionOption);
        //        fileToolBar->addAction(m_ActionOption);

        m_MenuFile->addSeparator();

        const QIcon save_icon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
        m_ActionSaveTrace = new QAction(save_icon, tr("&Save Trace"), m_Parent);
        m_ActionSaveTrace->setShortcuts(QKeySequence::Save);
        m_ActionSaveTrace->setStatusTip(tr("Save Trace"));
        connect(m_ActionSaveTrace, &QAction::triggered, m_Parent, &MainWindow::OnSaveTrace);
        m_MenuFile->addAction(m_ActionSaveTrace);
        //        fileToolBar->addAction(m_ActionSaveTrace);

        const QIcon save_as_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionSaveTraceAs = new QAction(save_as_icon, tr("&Save Trace As"), m_Parent);
        m_ActionSaveTraceAs->setShortcuts(QKeySequence::SelectAll);
        m_ActionSaveTraceAs->setStatusTip(tr("Save Trace As..."));
        connect(m_ActionSaveTraceAs, &QAction::triggered, m_Parent, &MainWindow::OnSaveTraceAs);
        m_MenuFile->addAction(m_ActionSaveTraceAs);

        const QIcon save_all_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionSaveAllTraces = new QAction(save_all_icon, tr("&Save All Traces"), m_Parent);
        m_ActionSaveAllTraces->setShortcuts(QKeySequence::SaveAs);
        m_ActionSaveAllTraces->setStatusTip(tr("Save Trace As..."));
        connect(m_ActionSaveAllTraces, &QAction::triggered, m_Parent, &MainWindow::OnSaveAllTraces);
        m_MenuFile->addAction(m_ActionSaveAllTraces);

        m_MenuFile->addSeparator();

        const QIcon save_project_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionSaveProject = new QAction(save_project_icon, tr("&Save Project"), m_Parent);
        m_ActionSaveProject->setShortcuts(QKeySequence::PreviousChild);
        m_ActionSaveProject->setStatusTip(tr("Save Project"));
        connect(m_ActionSaveProject, &QAction::triggered, m_Parent, &MainWindow::OnSaveProject);
        m_MenuFile->addAction(m_ActionSaveProject);

        const QIcon save_project_as_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionSaveProjectAs = new QAction(save_project_as_icon, tr("&Save Project As"), m_Parent);
        m_ActionSaveProjectAs->setShortcuts(QKeySequence::NextChild);
        m_ActionSaveProjectAs->setStatusTip(tr("Save Project As..."));
        connect(m_ActionSaveProjectAs, &QAction::triggered, m_Parent, &MainWindow::OnSaveProjectAs);
        m_MenuFile->addAction(m_ActionSaveProjectAs);

        const QIcon close_project_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionCloseProject = new QAction(save_project_as_icon, tr("&Close Project"), m_Parent);
        m_ActionCloseProject->setShortcuts(QKeySequence::NextChild);
        m_ActionCloseProject->setStatusTip(tr("Close Project"));
        connect(m_ActionCloseProject, &QAction::triggered, m_Parent, &MainWindow::OnCloseProject);
        m_MenuFile->addAction(m_ActionCloseProject);

        m_MenuFile->addSeparator();

        const QIcon recent_traces_icon = QIcon::fromTheme("document-save-as", QIcon(":/images/save_as.png"));
        m_ActionRecentTraces = new QAction(recent_traces_icon, tr("&Recent Traces"), m_Parent);
        m_ActionRecentTraces->setShortcuts(QKeySequence::NextChild);
        m_ActionRecentTraces->setStatusTip(tr("Recent Traces"));
        connect(m_ActionRecentTraces, &QAction::triggered, m_Parent, &MainWindow::OnCloseProject);
        m_MenuFile->addAction(m_ActionRecentTraces);

        m_MenuRecentTrces = new QMenu("Recent Traces", m_Parent);

        m_ActionRecentTraces->setMenu(m_MenuRecentTrces);

        m_MenuFile->addSeparator();

        const QIcon quit_icon = QIcon::fromTheme("application-exit", QIcon(":/images/quit.png"));
        m_ActionQuit = new QAction(quit_icon, tr("&Quit"), m_Parent);
        m_ActionQuit->setShortcuts(QKeySequence::Quit);
        m_ActionQuit->setStatusTip(tr("Quit the application"));
        m_MenuFile->addAction(m_ActionQuit);
        connect(m_ActionQuit, &QAction::triggered, m_Parent, &MainWindow::OnQuit);

        ////////////////////////////////////////////////////////

        m_MenuEdit = m_Parent->menuBar()->addMenu(tr("&Edit"));
        //    auto action = toolsMenu->addAction(tr("&Settings"), m_Parent, &MainWindow::OnSettings);
        //    action->setShortcuts(QKeySequence::Preferences);
        //    action->setStatusTip(tr("Preferences"));

        m_MenuTools = m_Parent->menuBar()->addMenu(tr("&Tools"));

        const QIcon pref_icon = QIcon::fromTheme("preferences-system", QIcon(":/images/preferences.png"));
        m_ActionPreferences = new QAction(pref_icon, tr("&Preferences"), m_Parent);
        m_ActionPreferences->setShortcuts(QKeySequence::Preferences);
        m_ActionPreferences->setStatusTip(tr("Preferences"));
        m_MenuTools->addAction(m_ActionPreferences);
        connect(m_ActionPreferences, &QAction::triggered, m_Parent, &MainWindow::OnPreferences);

        m_MenuView = m_Parent->menuBar()->addMenu(tr("&View"));

        const QIcon layout_icon = QIcon::fromTheme("preferences-desktop-theme", QIcon(":/images/layout.png"));
        m_ActionResetLayout = new QAction(pref_icon, tr("&Reset Layout"), m_Parent);
        m_ActionResetLayout->setShortcuts(QKeySequence::Replace);
        m_ActionResetLayout->setStatusTip(tr("Reset Layout"));
        m_MenuView->addAction(m_ActionResetLayout);
        connect(m_ActionResetLayout, &QAction::triggered, m_Parent, &MainWindow::OnResetLayout);

        const QIcon save_layout_icon = QIcon::fromTheme("preferences-desktop-theme", QIcon(":/images/layout.png"));
        m_ActionSaveLayout = new QAction(pref_icon, tr("&Save Layout"), m_Parent);
        m_ActionSaveLayout->setShortcuts(QKeySequence::Replace);
        m_ActionSaveLayout->setStatusTip(tr("Save Layout"));
        m_MenuView->addAction(m_ActionSaveLayout);
        connect(m_ActionSaveLayout, &QAction::triggered, m_Parent, &MainWindow::OnSaveLayout);

        m_MenuHelp = m_Parent->menuBar()->addMenu(tr("&Help"));
    }

    void createStatusBar()
    {
        m_Parent->statusBar()->showMessage(tr("Ready"));
    }

    void createDockWindows()
    {
        m_MenuView->addSeparator();

        auto data_view_dock = new QDockWidget(tr("Data View"), m_Parent);
        data_view_dock->setAllowedAreas(Qt::AllDockWidgetAreas);

        data_view_dock->setWidget(m_DataView);
        m_Parent->addDockWidget(Qt::BottomDockWidgetArea, data_view_dock);
        m_MenuView->addAction(data_view_dock->toggleViewAction());

        auto field_view_dock = new QDockWidget(tr("Field View"), m_Parent);
        field_view_dock->setAllowedAreas(Qt::AllDockWidgetAreas);

        field_view_dock->setWidget(m_FieldView);
        m_Parent->addDockWidget(Qt::BottomDockWidgetArea, field_view_dock);
        m_MenuView->addAction(field_view_dock->toggleViewAction());
    }

    void OnOpen(bool checked, QObject* sender)
    {

        QString file_name;
        auto action = qobject_cast<QAction*>(sender);

        if (action) {
            file_name = action->data().toString();
        }
        if (file_name.size() == 0) {
            auto last_filename = m_Settings->RecentAt(Core::FileType::Trace, 0);
            file_name = QFileDialog::getOpenFileName(m_Parent, "Open Trace", QString::fromStdString(last_filename), tr("Traces (*.pet);; Projects(*.h *.cpp)"));
        }
        if (file_name.size() != 0) {
            m_Settings->AddRecent(Core::FileType::Trace, file_name.toStdString());
            auto trace_id = m_Service->open(Core::FileType::Trace, file_name.toStdString());

            if (trace_id == InvalidId)
                return;

            //            auto trace_file = new Core::Business::TraceFile();
            //            trace_file->open(file_name.toStdString());
            //            m_TraceFiles.push_back(trace_file);
            //            auto protocol = m_Settings->CoreSettings()->GetProtocols()->At(trace_file->protocolId());
            auto trace_tab = new SpreadSheet(trace_id, Core::ViewType::Event, m_Parent);
            m_TraceView.addTab(trace_tab, QFileInfo(file_name).fileName());
            m_TraceView.setCurrentWidget(trace_tab);
            auto field_view = new FieldView(false, trace_tab);
            m_FieldView->addWidget(field_view);
            auto data_view = new DataView(trace_tab);
            m_DataView->addWidget(data_view);

            auto column_names = m_Service->columnNames(trace_id, Core::ViewType::Event);
            QStringList headers;
            for (auto name : column_names)
                headers.push_back(QString::fromStdString(name));

            trace_tab->setRowCount(1000);
            trace_tab->setColumnCount(headers.size());
            trace_tab->setHorizontalHeaderLabels(headers);

            m_Service->traverseTrace(trace_id, Core::ViewType::Event, 0, 9999, [](Core::Business::Packet*) { return false; });
            for (size_t i = 0; i < 1000; ++i) {
                trace_tab->fillRow(i, m_Service->rowData(trace_id, Core::ViewType::Event, i));
            }

            //            m_Service->traverseTrace(trace_id, Core::ViewType::Event, 0, 1000, std::bind(&SpreadSheet::fillRow, trace_tab, std::placeholders::_1, 0));
            connect(trace_tab, &SpreadSheet::currentItemChangedSignal, field_view, &FieldView::setPacket);
            connect(trace_tab, &SpreadSheet::currentItemChangedSignal, data_view, &DataView::setPacket);
            //            trace_tab->setCurrentCell(0, 0);
        }
    }

    void OnQuit(bool checked)
    {
        QDataStream ss;
        auto x = m_Parent->saveGeometry();

        QSettings settings(COMPANY_NAME, APP_NAME);
        settings.setValue("m_Name", x);

        QVariant v(x);
        auto u = x.toStdString();

        m_Settings->CoreSettings()->SetGeometry(m_Parent->saveGeometry().toStdString());
        m_Settings->CoreSettings()->SetState(m_Parent->saveState().toStdString());
    }

    void OnCloseTrace(int index)
    {
        auto field_view = m_FieldView->widget(index);
        auto data_view = m_DataView->widget(index);
        m_FieldView->removeWidget(field_view);
        m_DataView->removeWidget(data_view);
        m_TraceView.removeTab(index);
    }

    void OnPreferences(bool checked)
    {
        m_Settings->readSettings();
        m_Settings->reload();
        m_Settings->open();
    }

    void OnResetLayout(bool checked = false)
    {
    }

    void OnSaveLayout(bool checked = false)
    {
    }

    void MenuRecentTracesAboutToShow()
    {
        auto recents = m_Settings->Recent(Core::FileType::Trace);
        m_MenuRecentTrces->clear();
        for (auto recent : recents) {
            auto action = m_MenuRecentTrces->addAction(recent.c_str());
            action->setData(recent.c_str());
            connect(action, &QAction::triggered, m_Parent, &MainWindow::OnOpen);
        }
    }

    void OnCurrentTabChanged(int index)
    {
        m_FieldView->setCurrentIndex(index);
        m_DataView->setCurrentIndex(index);
    }
};

MainWindow::MainWindow(Core::Business::Service* service, QWidget* parent)
    : QMainWindow(parent)
    , Impl(std::make_unique<MainWindowPrivate>(service, this))
{
    //    auto lyMain = new QBoxLayout(QBoxLayout::TopToBottom);
    //    {
    //        lyMain->addWidget(&dataView);
    //    }

    //    setLayout(lyMain);
}

void MainWindow::OnNewProject(bool checked)
{
}

void MainWindow::OnOpen(bool checked)
{
    Impl->OnOpen(checked, sender());
}

void MainWindow::OnSaveTrace(bool checked)
{
}

void MainWindow::OnSaveTraceAs(bool checked)
{
}

void MainWindow::OnSaveAllTraces(bool checked)
{
}

void MainWindow::OnSaveProject(bool checked)
{
}

void MainWindow::OnSaveProjectAs(bool checked)
{
}

void MainWindow::OnCloseProject(bool checked)
{
}

void MainWindow::OnRecentTraces(bool checked)
{
}

void MainWindow::OnRecentProjects(bool checked)
{
}

void MainWindow::OnPreferences(bool checked)
{
    Impl->OnPreferences(checked);
}

void MainWindow::OnQuit(bool checked)
{
    Impl->OnQuit(checked);
    QMainWindow::close();
}

void MainWindow::OnCloseTrace(int index)
{
    Impl->OnCloseTrace(index);
}

void MainWindow::OnPreferencesFinished(int result)
{
    Core::Info("Result is {}", result);
}

void MainWindow::OnResetLayout(bool checked)
{
    Impl->OnResetLayout(checked);
}

void MainWindow::OnSaveLayout(bool checked)
{
    Impl->OnSaveLayout(checked);
}

void MainWindow::MenuRecentTracesAboutToShow()
{
    Impl->MenuRecentTracesAboutToShow();
}

void MainWindow::OnCurrentTabChanged(int index)
{
    Impl->OnCurrentTabChanged(index);
}

MainWindow::~MainWindow() = default;
