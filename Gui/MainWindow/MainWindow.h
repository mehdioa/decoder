#pragma once

namespace Core {
namespace Setting {
    class Settings;
}
namespace Business {
    class Service;
}
}

namespace Gui {
namespace Base {

    class DECODERGUI_API MainWindow : public QMainWindow {
        Q_OBJECT
    public:
        MainWindow(Core::Business::Service* service, QWidget* parent = nullptr);
        ~MainWindow();

    public Q_SLOTS:

    private Q_SLOTS:
        void OnNewProject(bool checked = false);
        void OnOpen(bool checked = false);
        void OnSaveTrace(bool checked = false);
        void OnSaveTraceAs(bool checked = false);
        void OnSaveAllTraces(bool checked = false);
        void OnSaveProject(bool checked = false);
        void OnSaveProjectAs(bool checked = false);
        void OnCloseProject(bool checked = false);
        void OnRecentTraces(bool checked = false);
        void OnRecentProjects(bool checked = false);
        void OnPreferences(bool checked = false);
        void OnQuit(bool checked = false);
        void OnCloseTrace(int index);
        void OnPreferencesFinished(int result);
        void OnResetLayout(bool checked = false);
        void OnSaveLayout(bool checked = false);
        void MenuRecentTracesAboutToShow();
        void OnCurrentTabChanged(int index);

    private:
        struct MainWindowPrivate;
        std::unique_ptr<MainWindowPrivate> Impl;
    };
}
}
