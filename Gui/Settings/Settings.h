#pragma once

#include "Widget.h"

namespace Gui {
namespace Setting {

    class DECODERGUI_API Settings : public Gui::Widget<QDialog> {
        Q_OBJECT

    public:
        Core::StringVec Recent(Core::FileType file_type) const;
        void AddRecent(Core::FileType file_type, const std::string value);
        std::string RecentAt(Core::FileType file_type, size_t index) const;
        Core::Setting::Settings* CoreSettings() const;

    protected Q_SLOTS:
        void onRestoreDefault(bool checked = false);
        void onApply(bool checked = false);
        void onOk(bool checked = false);
        void onCancel(bool checked = false);
        void OnCurrentChanged(const QModelIndex& current, const QModelIndex& previous);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    public:
        Settings(Core::Setting::Settings* core_settings, QWidget* parent = nullptr);
        ~Settings();

        void reload();
    };
}
}
