#pragma once

#include "Node.h"

namespace Gui {
	namespace Setting {

		class DECODERGUI_API Logger : public Node {
			Q_OBJECT

		public:
			Logger();
			~Logger() override;

		public Q_SLOTS:
			void onRestoreDefault() override;
			void onApply() override;
			void onReset() override;

		protected:
			void initLayout() override;
			Node* initChild(Core::Setting::Node* node) override;

			struct Impl;
			std::unique_ptr<Impl> d;
		};
	}
}
