// clang-format off
#include "pch.hpp"
#include "Settings.h"
#include "Base/TreeModel.h"
#include "Base/TreeView.h"
#include "tree.h"
#include "Logger.h"
#include "Layout.h"
#include "Core/Settings/Settings.h"
#include "Node.h"
// clang-format on

namespace Gui {
namespace Setting {

    struct Settings::Impl {
        Settings* m_Parent;
        Core::Setting::Settings* m_CoreSettings;
        Base::TreeView m_NodeTreeView;
        NodeTree m_NodeTree;
        Base::TreeModel m_NodeTreeModel;
        QStackedWidget* m_NodeSettingWidget;
        QPushButton m_OkButton;
        QPushButton m_ApplyButton;
        QPushButton m_CancelButton;
        QPushButton m_DefaultButton;
        Logger m_Logger;
        Layout m_Layout;

        std::vector<QString> m_PathStrings;

        Impl(Core::Setting::Settings* core_settings, Settings* parent)
            : m_Parent(parent)
            , m_CoreSettings(core_settings)
            , m_NodeSettingWidget(new QStackedWidget(parent))
        {
            m_OkButton.setText(tr("Ok"));
            m_ApplyButton.setText(tr("Apply"));
            m_CancelButton.setText(tr("Cancel"));
            m_DefaultButton.setText(tr("Default"));
            //            m_NodeTreeModel.setEditable(true);
            auto core_settings_children = m_CoreSettings->children();
            for (auto core_setting_child : core_settings_children) {
                if (core_setting_child->name() == "Logger") {
                    m_Logger.init(core_setting_child);
                    m_NodeTree.push_back(&m_Logger);
                    m_NodeSettingWidget->addWidget(m_Logger.widget());
                } else if (core_setting_child->name() == "Layout") {
                    m_Layout.init(core_setting_child);
                    m_NodeTree.push_back(&m_Layout);
                    m_NodeSettingWidget->addWidget(m_Layout.widget());
                }
            }

            auto lyMain = new QBoxLayout(QBoxLayout::TopToBottom);
            {
                auto lyRaw = new QBoxLayout(QBoxLayout::LeftToRight);
                {
                    lyRaw->addWidget(&m_NodeTreeView);
                    lyRaw->addWidget(m_NodeSettingWidget);
                }
                auto lyButtons = new QBoxLayout(QBoxLayout::LeftToRight);
                {
                    lyButtons->addWidget(&m_DefaultButton);
                    lyButtons->addStretch();
                    lyButtons->addWidget(&m_OkButton);
                    lyButtons->addWidget(&m_ApplyButton);
                    lyButtons->addWidget(&m_CancelButton);
                }
                lyMain->addLayout(lyRaw);
                lyMain->addLayout(lyButtons);
            }
            m_Parent->setLayout(lyMain);
            connect(&m_NodeTreeView, &Base::TreeView::currentChanged, m_Parent, &Settings::OnCurrentChanged);
            reload();
        }

        ~Impl()
        {
        }

        void OnCurrentChanged(const QModelIndex& current, const QModelIndex& previous)
        {
            auto current_row = current.row();
            auto previus_row = previous.row();
            if (current_row != previus_row)
                m_NodeSettingWidget->setCurrentIndex(current_row);
        }

        void reload()
        {
            m_Logger.onReset();
            m_Layout.onReset();

            m_NodeTreeModel.init(&m_NodeTree);
            m_NodeTreeView.setModel(&m_NodeTreeModel);
        }

        void onRestoreDefault(bool checked)
        {
            auto current_row = m_NodeSettingWidget->currentIndex();
            auto current_node = m_NodeTree.at(current_row);

            if (current_node)
                current_node->value()->onRestoreDefault();
        }

        void onApply(bool checked)
        {
            auto current_row = m_NodeSettingWidget->currentIndex();
            auto current_node = m_NodeTree.at(current_row);

            if (current_node)
                current_node->value()->onApply();
        }
    };

    Core::StringVec Settings::Recent(Core::FileType file_type) const
    {
        return d->m_CoreSettings->Recent(file_type);
    }

    void Settings::AddRecent(Core::FileType file_type, const std::string value)
    {
        return d->m_CoreSettings->AddRecent(file_type, value);
    }

    std::string Settings::RecentAt(Core::FileType file_type, size_t index) const
    {
        return d->m_CoreSettings->RecentAt(file_type, index);
    }

    Core::Setting::Settings* Settings::CoreSettings() const
    {
        return d->m_CoreSettings;
    }

    void Settings::onRestoreDefault(bool checked)
    {
        d->onRestoreDefault(checked);
    }

    void Settings::onApply(bool checked)
    {
        d->onApply(checked);
    }

    void Settings::onOk(bool checked)
    {
        d->onApply(true);
        accept();
    }

    void Settings::onCancel(bool checked)
    {
        reject();
    }

    void Settings::OnCurrentChanged(const QModelIndex& current, const QModelIndex& previous)
    {
        d->OnCurrentChanged(current, previous);
    }

    Settings::Settings(Core::Setting::Settings* core_settings, QWidget* parent)
        : Gui::Widget<QDialog>("Widgets/Settings/geometry", parent)
        , d(std::make_unique<Settings::Impl>(core_settings, this))
    {
        setWindowTitle("Settings");
        QObject::connect(&d->m_DefaultButton, SIGNAL(clicked(bool)), this, SLOT(onRestoreDefault(bool)));
        QObject::connect(&d->m_ApplyButton, SIGNAL(clicked(bool)), this, SLOT(onApply(bool)));
        QObject::connect(&d->m_OkButton, SIGNAL(clicked(bool)), this, SLOT(onOk(bool)));
        QObject::connect(&d->m_CancelButton, SIGNAL(clicked(bool)), this, SLOT(onCancel(bool)));
    }

    void Settings::reload()
    {
        d->reload();
    }

    Settings::~Settings() = default;

}
}
