#pragma once

namespace Gui {
	namespace Setting {
		class DECODERGUI_API Node : public QObject {
			Q_OBJECT

		public:
			Node();
			~Node();

			QGroupBox* widget();
			void init(Core::Setting::Node* node);
			Nodes children() const;
			QString name() const;
			QBoxLayout* layout() const;

		public Q_SLOTS:
			virtual void onRestoreDefault() = 0;
			virtual void onApply() = 0;
			virtual void onReset() = 0;

		protected:
			virtual Node* initChild(Core::Setting::Node* node) = 0;
			virtual void initLayout() = 0;

			Core::Setting::Node* m_CoreNode;
			Nodes m_Children;
			QBoxLayout* m_Layout;
			QGroupBox* m_GroupBox;
		};
	}
}
