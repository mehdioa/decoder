// clang-format off
#include "pch.hpp"
#include "Logger.h"
#include "Core/Settings/Logger.h"
#include "Core/Settings/Parameter.h"
// clang-format on

using namespace Gui::Setting;

struct Logger::Impl {
    Logger* m_Parent;
    Core::Setting::Logger* m_CoreNode;

    QComboBox* m_LevelComboBox;
    QLabel* m_LevelLabel;
    QBoxLayout* m_LevelLayout;

    QComboBox* m_OutputComboBox;
    QLabel* m_OutputLabel;
    QBoxLayout* m_OutputLayout;

    QCheckBox* m_ColoredCheckbox;
    QBoxLayout* m_ColoredLayout;

    QCheckBox* m_InvertedCheckbox;
    QBoxLayout* m_InvertedLayout;

    QCheckBox* m_ForceFlushCheckbox;
    QBoxLayout* m_ForceFlushLayout;

    QSpinBox* m_MaxFileSizeSpinBox;
    QLabel* m_MaxFileSizeLabel;
    QBoxLayout* m_MaxFileSizeLayout;

    QSpinBox* m_MaxFilesSpinBox;
    QLabel* m_MaxFilesLabel;
    QBoxLayout* m_MaxFilesLayout;

    QLineEdit* m_PatternLineEdit;
    QLabel* m_PatternLabel;
    QBoxLayout* m_PatternLayout;

    Impl(Logger* parent)
        : m_Parent(parent)
        , m_CoreNode(nullptr)
        , m_LevelComboBox(new QComboBox)
        , m_LevelLabel(new QLabel("Level:"))
        , m_LevelLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_OutputComboBox(new QComboBox)
        , m_OutputLabel(new QLabel("Output:"))
        , m_OutputLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_ColoredCheckbox(new QCheckBox("Colored"))
        , m_ColoredLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_InvertedCheckbox(new QCheckBox("Inverted"))
        , m_InvertedLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_ForceFlushCheckbox(new QCheckBox("Force Flush"))
        , m_ForceFlushLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_MaxFileSizeSpinBox(new QSpinBox)
        , m_MaxFileSizeLabel(new QLabel("Max File Size:"))
        , m_MaxFileSizeLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_MaxFilesSpinBox(new QSpinBox)
        , m_MaxFilesLabel(new QLabel("Max Files:"))
        , m_MaxFilesLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_PatternLineEdit(new QLineEdit)
        , m_PatternLabel(new QLabel("Pattern:"))
        , m_PatternLayout(new QBoxLayout(QBoxLayout::LeftToRight))
    {
    }

    ~Impl()
    {
        delete m_LevelLabel;
        delete m_LevelLayout;
        delete m_LevelComboBox;
        delete m_OutputLabel;
        delete m_OutputLayout;
        delete m_OutputComboBox;
        delete m_ColoredCheckbox;
        delete m_InvertedCheckbox;
        delete m_InvertedLayout;
        delete m_ForceFlushCheckbox;
        delete m_ForceFlushLayout;
        delete m_MaxFileSizeLabel;
        delete m_MaxFileSizeLayout;
        delete m_MaxFileSizeSpinBox;
        delete m_MaxFilesLabel;
        delete m_MaxFilesLayout;
        delete m_MaxFilesSpinBox;
        delete m_PatternLineEdit;
        delete m_PatternLabel;
        delete m_PatternLayout;
    }

    void initLayout()
    {
        m_CoreNode = static_cast<Core::Setting::Logger*>(m_Parent->m_CoreNode);

        for (int item = static_cast<int>(Core::Log::Level::Begin); item < static_cast<int>(Core::Log::Level::Last); ++item) {
            m_LevelComboBox->addItem(LevelName(static_cast<Core::Log::Level>(item)));
        }

        m_LevelLayout->addWidget(m_LevelLabel);
        m_LevelLayout->addStretch();
        m_LevelLayout->addWidget(m_LevelComboBox);

        for (int item = static_cast<int>(Core::Log::Output::Begin); item < static_cast<int>(Core::Log::Output::Last); ++item) {
            m_OutputComboBox->addItem(OutputName(static_cast<Core::Log::Output>(item)));
        }

        m_OutputLayout->addWidget(m_OutputLabel);
        m_OutputLayout->addStretch();
        m_OutputLayout->addWidget(m_OutputComboBox);

        m_ColoredLayout->addWidget(m_ColoredCheckbox);
        m_ColoredLayout->addStretch();

        m_InvertedLayout->addWidget(m_InvertedCheckbox);
        m_InvertedLayout->addStretch();

        m_ForceFlushLayout->addWidget(m_ForceFlushCheckbox);
        m_ForceFlushLayout->addStretch();

        m_MaxFileSizeSpinBox->setMinimum(0);
        m_MaxFileSizeSpinBox->setMaximum(std::numeric_limits<int>::max());
        m_MaxFileSizeLayout->addWidget(m_MaxFileSizeLabel);
        m_MaxFileSizeLayout->addStretch();
        m_MaxFileSizeLayout->addWidget(m_MaxFileSizeSpinBox);

        m_MaxFilesSpinBox->setMinimum(0);
        m_MaxFilesSpinBox->setMaximum(std::numeric_limits<int>::max());
        m_MaxFilesLayout->addWidget(m_MaxFilesLabel);
        m_MaxFilesLayout->addStretch();
        m_MaxFilesLayout->addWidget(m_MaxFilesSpinBox);

        m_PatternLayout->addWidget(m_PatternLabel);
        m_PatternLayout->addStretch();
        m_PatternLayout->addWidget(m_PatternLineEdit);
        m_PatternLineEdit->setToolTip("Visit https://github.com/gabime/spdlog/wiki/3.-Custom-formatting");

        m_Parent->layout()->setDirection(QBoxLayout::TopToBottom);
        m_Parent->layout()->addLayout(m_LevelLayout);
        m_Parent->layout()->addLayout(m_OutputLayout);
        m_Parent->layout()->addLayout(m_ColoredLayout);
        m_Parent->layout()->addLayout(m_InvertedLayout);
        m_Parent->layout()->addLayout(m_ForceFlushLayout);
        m_Parent->layout()->addLayout(m_MaxFileSizeLayout);
        m_Parent->layout()->addLayout(m_MaxFilesLayout);
        m_Parent->layout()->addLayout(m_PatternLayout);

        onReset();
    }

    void onRestoreDefault()
    {
        m_CoreNode->restoreDefault();
        onReset();
    }

    void onApply()
    {
        auto new_level = static_cast<Core::Log::Level>(m_LevelComboBox->currentIndex());
        if (new_level != m_CoreNode->GetLevel())
            m_CoreNode->SetLevel(new_level);

        auto new_output = static_cast<Core::Log::Output>(m_OutputComboBox->currentIndex());
        if (new_output != m_CoreNode->GetOutput())
            m_CoreNode->SetOutput(new_output);

        auto new_colored = (m_ColoredCheckbox->checkState() == Qt::CheckState::Checked);
        if (new_colored != m_CoreNode->GetColored())
            m_CoreNode->SetColored(new_colored);

        auto new_inverted = (m_InvertedCheckbox->checkState() == Qt::CheckState::Checked);
        if (new_inverted != m_CoreNode->GetInverted())
            m_CoreNode->SetInverted(new_inverted);

        auto new_force_flush = (m_ForceFlushCheckbox->checkState() == Qt::CheckState::Checked);
        if (new_force_flush != m_CoreNode->GetForceFlush())
            m_CoreNode->SetForceFlush(new_force_flush);

        auto new_max_file_size = m_MaxFileSizeSpinBox->value();
        if (new_max_file_size != m_CoreNode->GetMaxFileSize())
            m_CoreNode->SetMaxFileSize(new_max_file_size);

        auto new_max_files = m_MaxFilesSpinBox->value();
        if (new_max_files != m_CoreNode->GetMaxFiles())
            m_CoreNode->SetMaxFiles(new_max_files);

        auto new_pattern = m_PatternLineEdit->text().toStdString();
        if (new_pattern != m_CoreNode->GetPattern())
            m_CoreNode->SetPattern(new_pattern);
    }

    void onReset()
    {
        m_LevelComboBox->setCurrentIndex(static_cast<int>(m_CoreNode->GetLevel()));
        m_OutputComboBox->setCurrentIndex(static_cast<int>(m_CoreNode->GetOutput()));
        m_ColoredCheckbox->setChecked(m_CoreNode->GetColored());
        m_InvertedCheckbox->setChecked(m_CoreNode->GetInverted());
        m_ForceFlushCheckbox->setChecked(m_CoreNode->GetForceFlush());
        m_MaxFileSizeSpinBox->setValue(m_CoreNode->GetMaxFileSize());
        m_MaxFilesSpinBox->setValue(m_CoreNode->GetMaxFiles());
        m_PatternLineEdit->setText(QString::fromStdString(m_CoreNode->GetPattern()));
    }
};

Node* Logger::initChild(Core::Setting::Node* node)
{
    return nullptr;
}

void Logger::initLayout()
{
    d->initLayout();
}

Logger::Logger()
    : d(std::make_unique<Logger::Impl>(this))
{
}

void Logger::onRestoreDefault()
{
    d->onRestoreDefault();
}

void Logger::onApply()
{
    d->onApply();
}

void Logger::onReset()
{
    d->onReset();
}

Logger::~Logger() = default;
