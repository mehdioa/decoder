// clang-format off
#include "pch.hpp"
#include "Layout.h"
#include "Core/Settings/Layout.h"
#include "Core/Settings/Parameter.h"
// clang-format on

using namespace Gui::Setting;

struct Layout::Impl {
    Layout* m_Parent;
    Core::Setting::Layout* m_CoreNode;

    QLineEdit* m_GeometryLineEdit;
    QLabel* m_GeometryLabel;
    QBoxLayout* m_GeometryLayout;

    QLineEdit* m_StateLineEdit;
    QLabel* m_StateLabel;
    QBoxLayout* m_StateLayout;

    Impl(Layout* parent)
        : m_Parent(parent)
        , m_CoreNode(nullptr)
        , m_GeometryLineEdit(new QLineEdit)
        , m_GeometryLabel(new QLabel("Geometry:"))
        , m_GeometryLayout(new QBoxLayout(QBoxLayout::LeftToRight))
        , m_StateLineEdit(new QLineEdit)
        , m_StateLabel(new QLabel("State:"))
        , m_StateLayout(new QBoxLayout(QBoxLayout::LeftToRight))
    {
    }

    ~Impl()
    {
        delete m_GeometryLineEdit;
        delete m_GeometryLabel;
        delete m_GeometryLayout;
        delete m_StateLineEdit;
        delete m_StateLabel;
        delete m_StateLayout;
    }

    void initLayout()
    {
        m_CoreNode = static_cast<Core::Setting::Layout*>(m_Parent->m_CoreNode);

        m_GeometryLayout->addWidget(m_GeometryLabel);
        m_GeometryLayout->addStretch();
        m_GeometryLayout->addWidget(m_GeometryLineEdit);
        m_GeometryLineEdit->setToolTip("Setting Geometry");

        m_StateLayout->addWidget(m_StateLabel);
        m_StateLayout->addStretch();
        m_StateLayout->addWidget(m_StateLineEdit);
        m_StateLineEdit->setToolTip("Setting State");

        m_Parent->layout()->setDirection(QBoxLayout::TopToBottom);
        m_Parent->layout()->addLayout(m_GeometryLayout);
        m_Parent->layout()->addLayout(m_StateLayout);

        onReset();
    }

    void onRestoreDefault()
    {
        m_CoreNode->restoreDefault();
        onReset();
    }

    void onApply()
    {
        auto new_geometry = m_GeometryLineEdit->text().toStdString();
        if (new_geometry != m_CoreNode->GetGeometry()) {
            m_CoreNode->SetGeometry(new_geometry);
        }

        auto new_state = m_StateLineEdit->text().toStdString();
        if (new_state != m_CoreNode->GetState())
            m_CoreNode->SetGeometry(new_state);
    }

    void onReset()
    {
        m_GeometryLineEdit->setText(QString::fromStdString(m_CoreNode->GetGeometry()));
        m_StateLineEdit->setText(QString::fromStdString(m_CoreNode->GetState()));
    }
};

Node* Layout::initChild(Core::Setting::Node* node)
{
    return nullptr;
}

void Layout::initLayout()
{
    d->initLayout();
}

Layout::Layout()
    : d(std::make_unique<Layout::Impl>(this))
{
}

void Layout::onRestoreDefault()
{
    d->onRestoreDefault();
}

void Layout::onApply()
{
    d->onApply();
}

void Layout::onReset()
{
    d->onReset();
}

Layout::~Layout() = default;
