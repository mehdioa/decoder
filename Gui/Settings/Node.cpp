// clang-format off
#include "pch.hpp"
#include "Node.h"
#include "Core/Settings/Node.h"
// clang-format on

namespace Gui {
namespace Setting {

    Node::Node()
        : m_CoreNode(nullptr)
        , m_Layout(new QBoxLayout(QBoxLayout::TopToBottom))
        , m_GroupBox(new QGroupBox)
    {
        m_GroupBox->setLayout(m_Layout);
    }

    Node::~Node()
    {
        delete m_Layout;
        delete m_GroupBox;
    }

    QGroupBox* Node::widget()
    {
        return m_GroupBox;
    }

    void Node::init(Core::Setting::Node* node)
    {
        m_CoreNode = node;

        auto core_node_children = m_CoreNode->children();

        for (auto child : core_node_children) {
            auto child_node = initChild(child);
            if (child_node)
                m_Children.push_back(child_node);
        }
        initLayout();
        m_GroupBox->setTitle(name());
    }

    Nodes Node::children() const
    {
        return m_Children;
    }

    QString Node::name() const
    {
        return QString::fromStdString(m_CoreNode->name());
    }

    QBoxLayout* Node::layout() const
    {
        return m_Layout;
    }

}
}
