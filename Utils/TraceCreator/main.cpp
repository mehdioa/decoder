#if defined(_WIN32) || defined(_WIN64)
#include <winsock.h>
#else
#include <arpa/inet.h>
#endif

#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>
using namespace std;

#define WRITE(F, X) F->write(reinterpret_cast<const char*>(&X), sizeof(X))

struct Header {
    uint32_t SpecId;
    uint32_t NoOfPackets;
    uint64_t AddrPackets;
    uint64_t AddrData;
};

struct Packet {
    uint32_t TimeDelta;
    uint32_t Extra;
    uint16_t DataLen;
};

void write_hex(ofstream* s, uint32_t index, double t, uint64_t& addr, const std::string& str)
{
    s->seekp(addr);
    uint8_t v;
    WRITE(s, t);
    for (auto c : str) {
        v = c - '0';
        WRITE(s, v);
    }
    s->seekp(index * sizeof(addr));
    WRITE(s, addr);
    addr += sizeof(t);
    addr += str.length() * sizeof(v);
}

void create_trace(const char* file_name, uint32_t id)
{
    fstream file(file_name, std::ios_base::out | std::ios::binary);
    file.imbue(std::locale::classic());
    Header header;
    header.NoOfPackets = 1000;

    std::vector<uint32_t> values {
        //        htonl(0x06001122),
        //        htonl(0x00334455),
        //        htonl(0x0000AA03),
        //        htonl(0x00000000),
        //        htonl(0x00010002),
        //        htonl(0x00112233),
        //        htonl(0x00112233),
        //        htonl(0x44556677),
        //        htonl(0xAA000000),
        //        htonl(0x11223344),
        //        htonl(0x55667788),
        //        htonl(0x99aabbcc),
        //        htonl(0xddeeffaa),
        //        htonl(0xbbccdd00),
        //        htonl(0x11223344),
        //        htonl(0x55667788),
        0x22110006,
        0x55443300,
        0x03AA0000,
        0x00000000,
        0x02000100,
        0x33221100,
        0x33221100,
        0x77665544,
        0x000000AA,
        0x44332211,
        0x88776655,
        0xccbbaa99,
        0xaaffeedd,
        0x00ddccbb,
        0x44332211,
        0x88776655,
    };

    std::vector<Packet> packets;
    uint32_t time_delta = 0;
    for (uint32_t i = 0; i < header.NoOfPackets; ++i) {
        time_delta += 10000;
        packets.push_back({ time_delta, 0, static_cast<uint16_t>(values.size() * 4) });
    }
    header.SpecId = id;
    header.AddrPackets = 4 + 4 + 8 + 8;
    header.AddrData = header.AddrPackets + header.NoOfPackets * (4 + 4 + 2);

    WRITE((&file), header.SpecId);
    WRITE((&file), header.NoOfPackets);
    WRITE((&file), header.AddrPackets);
    WRITE((&file), header.AddrData);

    for (auto packet : packets) {
        WRITE((&file), packet.TimeDelta);
        WRITE((&file), packet.Extra);
        WRITE((&file), packet.DataLen);
    }

    for (auto packet : packets) {
        for (auto value : values)
            WRITE((&file), value);
    }
    file.close();
}

int main()
{
    //    auto num = std::stoull("a", nullptr, 16);
    //    std::cout << num << std::endl;
    //    std::cout << std::hex;
    //    fstream temp_file;
    //    temp_file.open("example.pet", std::ios::in | std::ios::binary);
    //    if (errno != 0)
    //        throw std::runtime_error(std::strerror(errno));

    //    uint32_t x;
    //    temp_file.read(reinterpret_cast<char*>(&x), sizeof(x));
    //    std::cout << "X is " << x << std::endl;

    //    uint8_t a;
    //    for (auto i = 0; i < 20; ++i) {
    //        temp_file.read(reinterpret_cast<char*>(&a), sizeof(a));
    //        std::cout << " " << (uint16_t)a;
    //    }
    //    temp_file.close();

    create_trace("pcie.pet", 0);
    create_trace("spl.pet", 1);
    return 0;
}
