import qbs

Product {
    name: "TraceCreator"
    type: "application"
    files: [
        "main.cpp",
    ]
    Depends { name: "cpp" }

    cpp.cxxLanguageVersion: project.cxxLanguageVersion

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }
}
