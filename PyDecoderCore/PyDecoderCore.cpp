// PyDecoderCore.cpp : Defines the exported functions for the DLL.
//
#include "PyDecoderCore.h"
#include "Core/Tree.h"
#include "Core/Base/IBuffer.h"
#include "Core/Base/Protocol.h"
#include "Core/Base/Field.h"
#include "Core/Base/EditableBuffer.h"
#include "Core/Base/DecodedField.h"
#include "Core/Base/Buffer.h"
#include "Core/Base/Option.h"

using namespace pugi;
using namespace Core;

PYBIND11_MODULE(Decoder, m)
{
	// py::module sm = m.def_submodule("Decoder", "Example strategy framework");

	py::class_<xml_document>(m, "XmlDoc")
		.def(py::init<>())
		.def("loadFile", (xml_parse_result(xml_document::*)(const char*, unsigned int, xml_encoding)) & xml_document::load_file)
		.def("child", &xml_document::child);

	py::enum_<xml_encoding>(m, "XmlEncoding")
		.value("Auto", xml_encoding::encoding_auto)
		.value("UTF_8", xml_encoding::encoding_utf8)
		.value("UTF_16_LE", xml_encoding::encoding_utf16_le)
		.value("UTF_16_BE", xml_encoding::encoding_utf16_be)
		.value("UTF_16", xml_encoding::encoding_utf16)
		.value("UTF_32_LE", xml_encoding::encoding_utf32_le)
		.value("UTF_32_BE", xml_encoding::encoding_utf32_be)
		.value("UTF_32", xml_encoding::encoding_utf32)
		.value("WChar", xml_encoding::encoding_wchar)
		.value("Latin_1", xml_encoding::encoding_latin1);

	py::enum_<xml_parse_status>(m, "XmlParseStatus")
		.value("Ok", xml_parse_status::status_ok)
		.value("FileNotFound", xml_parse_status::status_file_not_found)
		.value("IOError", xml_parse_status::status_io_error)
		.value("OutOfMemory", xml_parse_status::status_out_of_memory)
		.value("InternalError", xml_parse_status::status_internal_error)
		.value("UnrecognizedTag", xml_parse_status::status_unrecognized_tag)
		.value("BadPI", xml_parse_status::status_bad_pi)
		.value("BadComment", xml_parse_status::status_bad_comment)
		.value("BadCData", xml_parse_status::status_bad_cdata)
		.value("BadDocType", xml_parse_status::status_bad_doctype)
		.value("BadPCData", xml_parse_status::status_bad_pcdata)
		.value("BadStartComment", xml_parse_status::status_bad_start_element)
		.value("BadAttribute", xml_parse_status::status_bad_attribute)
		.value("BadEndElement", xml_parse_status::status_bad_end_element)
		.value("EndElementMismatch", xml_parse_status::status_end_element_mismatch)
		.value("AppendInvalidRoot", xml_parse_status::status_append_invalid_root)
		.value("NoDocumentElement", xml_parse_status::status_no_document_element);

	py::enum_<ByteOrder>(m, "ByteOrder")
		.value("Big", ByteOrder::Big)
		.value("Little", ByteOrder::Little);

	py::enum_<BitOrder>(m, "BitOrder")
		.value("MSB", BitOrder::MSB)
		.value("LSB", BitOrder::LSB);

	py::enum_<ValueType>(m, "ValueType")
		.value("Bin", ValueType::Bin)
		.value("Hex", ValueType::Hex)
		.value("Oct", ValueType::Oct);

	py::enum_<FieldType>(m, "FieldType")
		.value("Integral", FieldType::Integral)
		.value("String", FieldType::String)
		.value("Block", FieldType::Block);


	py::enum_<Log::Level>(m, "LogLevel")
		.value("Trace", Log::Level::Trace)
		.value("Debug", Log::Level::Debug)
		.value("Info", Log::Level::Info)
		.value("Warn", Log::Level::Warn)
		.value("Error", Log::Level::Error)
		.value("Critical", Log::Level::Critical);

	py::enum_<Log::Output>(m, "LogOutput")
		.value("Console", Log::Output::Console)
		.value("File", Log::Output::File);

	py::class_<Base::Buffer>(m, "Buffer")
		.def(py::init<>())
		.def("append", &Base::Buffer::append)
		.def("size", &Base::Buffer::size)
		.def("clear", &Base::Buffer::clear)
		.def("value", &Base::Buffer::value)
		.def("data", &Base::Buffer::data)
		.def("setData", &Base::Buffer::setData)
		.def("toString", &Base::Buffer::toString)
		.def("dump", &Base::Buffer::dump);

	py::class_<Base::EditableBuffer>(m, "EditableBuffer")
		.def(py::init<>())
		.def("init", &Base::EditableBuffer::init)
		.def("append", &Base::EditableBuffer::append)
		.def("size", &Base::EditableBuffer::size)
		.def("clear", &Base::EditableBuffer::clear)
		.def("value", &Base::EditableBuffer::value)
		.def("data", &Base::EditableBuffer::data)
		.def("setData", &Base::EditableBuffer::setData)
		.def("toString", &Base::EditableBuffer::toString);


	py::class_<Base::Option>(m, "Option")
		.def(py::init<>())
		.def("init", &Base::Option::init)
		.def("minMax", &Base::Option::minMax)
		.def("name", &Base::Option::name)
		.def("decode", &Base::Option::decode)
		.def("refOption", &Base::Option::refOption)
		.def("visit", &Base::Option::visit)
		.def("dump", &Base::Option::dump);

	py::class_<Base::Field>(m, "Field")
		.def(py::init<>())
		//.def("visit", &Base::Field::visit)
		.def("init", &Base::Field::init)
		.def("name", &Base::Field::name)
		.def("decode", &Base::Field::decode)
		.def("refId", &Base::Field::refId)
		.def("fieldId", &Base::Field::fieldId)
		.def("setFieldId", &Base::Field::setFieldId)
		.def("id", &Base::Field::id)
		.def("fieldType", &Base::Field::fieldType)
		.def("bitOrder", &Base::Field::bitOrder)
		//.def("options", &Base::Field::options)
		.def("initFieldRefs", &Base::Field::initFieldRefs)
		.def("dump", &Base::Field::dump);


	py::class_<Base::DecodedField>(m, "DecodedField")
		.def(py::init<>())
		.def("init", &Base::DecodedField::init)
		.def("start", &Base::DecodedField::start)
		.def("length", &Base::DecodedField::length)
		.def("setLength", &Base::DecodedField::setLength)
		.def("setOption", &Base::DecodedField::setOption)
		.def("option", &Base::DecodedField::option)
		.def("field", &Base::DecodedField::field)
		.def("value", &Base::DecodedField::value)
		.def("dump", &Base::DecodedField::dump);

	py::class_<Base::Protocol>(m, "Protocol")
		.def(py::init<>())
		.def("init", &Base::Protocol::init)
		.def("bitOrder", &Base::Protocol::bitOrder)
		.def("byteOrder", &Base::Protocol::byteOrder)
		.def("decode", &Base::Protocol::decode)
		.def("dump", &Base::Protocol::dump);




	//py::enum_<Side>(m, "Side")
	//	.value("BUY", Side::BUY)
	//	.value("SELL", Side::SELL);

	//py::class_<Order>(m, "Order")
	//	.def_readonly("symbol", &Order::symbol)
	//	.def_readonly("side", &Order::side)
	//	.def_readwrite("size", &Order::size)
	//	.def_readwrite("price", &Order::price)
	//	.def_readonly("order_id", &Order::order_id);

	//py::class_<Strategy, PyStrategy>(m, "Strategy")
	//	.def(py::init<>())
	//	.def("sendOrder", &Strategy::sendOrder)
	//	.def("stop", &Strategy::stop);
}
