import qbs

DynamicLibrary {
    name: "PyDecoderCore"
//    qbs.architecture: project.arch
    files: [
        "**/*.cpp",
        "**/*.h",
    ]


    install: true

    Depends {name: "cpp"}

    installImportLib: true
    cpp.defines: ["BOOST_DYNAMIC_BITSET_DONT_USE_FRIENDS"]

    cpp.libraryPaths:
    {
        var res = project.LIB_DIRS;
        res.push(product.buildDirectory+"/../install-root/lib")
        return res;
    }

    cpp.cxxLanguageVersion: project.cxxLanguageVersion

    cpp.dynamicLibraries:  [
        "pugixml",
        "DecoderCore",
    ]

    cpp.includePaths: {
        var res = project.INC_DIRS;
        res.push(".");
        return res;
    }

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }
}

