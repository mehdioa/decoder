// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the PYDECODERCORE_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// PYDECODERCORE_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef PYDECODERCORE_EXPORTS
#define PYDECODERCORE_API __declspec(dllexport)
#else
#define PYDECODERCORE_API __declspec(dllimport)
#endif

#include "Core/pch.hpp"
#include <pybind11/embed.h>
#include <pybind11/eval.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/complex.h>
#include <pybind11/stl_bind.h>
#include <pybind11/cast.h>
#include <pybind11/attr.h>
#include <pybind11/buffer_info.h>
#include <pybind11/chrono.h>
#include <pybind11/functional.h>
#include <pybind11/numpy.h>
#include <pybind11/operators.h>
#include <pybind11/pytypes.h>

namespace py = pybind11;